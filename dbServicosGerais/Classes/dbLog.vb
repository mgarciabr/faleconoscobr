﻿Imports plAppLibrary.Enumerators
Imports plAppLibrary.clsCommonFunctions

Public Class dbLog
    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal strLOGDESCR As String, ByVal strLOGORIGEM As String, ByVal strDTLOGDATA As String, ByVal intLOGTIPO As Integer, Optional ByVal intUSID As Integer = Nothing, Optional ByVal intMODID As Integer = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO LOGEVENTOS (LOGDESCR, LOGORIGEM, LOGDATA, LOGTIPO, USID, MODID) ")
            strAux.Append("VALUES (")
            strAux.Append("'").Append(TrataTextos(strLOGDESCR)).Append("'").Append(", ").Append("'").Append(TrataTextos(strLOGORIGEM)).Append("'").Append(", ").Append("'").Append(FormataData_HoraSQL(strDTLOGDATA)).Append("'").Append(", ").Append(intLOGTIPO).Append(", ").Append(intUSID).Append(", ").Append(intMODID)
            'strAux.Append("'").Append(TrataTextos(strLOGDESCR)).Append("'").Append(", ").Append("'").Append(TrataTextos(strLOGORIGEM)).Append("'").Append(", ").Append("'").Append(strDTLOGDATA).Append("'").Append(", ").Append(intLOGTIPO).Append(", ").Append(intUSID).Append(", ").Append(intMODID)
            strAux.Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intLOGID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM LOGEVENTOS WHERE ")
            strAux.Append("LOGID = ").Append(intLOGID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intLOGID As Integer, ByVal strLOGDESCR As String, ByVal strLOGORIGEM As String, ByVal strDTLOGDATA As String, ByVal intLOGTIPO As Integer, Optional ByVal intUSID As Integer = Nothing, Optional ByVal intMODID As Integer = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE LOGEVENTOS SET ")
            strAux.Append("LOGDESCR = ").Append("'").Append(TrataTextos(strLOGDESCR)).Append("'").Append(", ").Append("LOGORIGEM = ").Append("'").Append(TrataTextos(strLOGORIGEM)).Append("'").Append(", ").Append("LOGDATA = ").Append("'").Append(FormataData_HoraSQL(strDTLOGDATA)).Append("'").Append(", ").Append("LOGTIPO = ").Append(intLOGTIPO).Append(", ").Append("USID = ").Append(intUSID).Append(", ").Append("MODID = ").Append(intMODID)
            'strAux.Append("LOGDESCR = ").Append("'").Append(TrataTextos(strLOGDESCR)).Append("'").Append(", ").Append("LOGORIGEM = ").Append("'").Append(TrataTextos(strLOGORIGEM)).Append("'").Append(", ").Append("LOGDATA = ").Append("'").Append(strDTLOGDATA).Append("'").Append(", ").Append("LOGTIPO = ").Append(intLOGTIPO).Append(", ").Append("USID = ").Append(intUSID).Append(", ").Append("MODID = ").Append(intMODID)
            strAux.Append("WHERE ")
            strAux.Append("LOGID = ").Append(intLOGID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            strAux = New Text.StringBuilder
            strAux.Append("SELECT * FROM LOGEVENTOS WHERE ")
            strAux.Append("LOGDESCR = ").Append("''").Append(" AND ").Append("LOGORIGEM = ").Append("''").Append(" AND ").Append("LOGDATA = ").Append("''").Append(" AND ").Append("LOGTIPO = ").Append("-1").Append(" AND ").Append("USID = ").Append("-1").Append(" AND ").Append("MODID = ").Append("-1")

            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.UpdateBatch(dsDados, _
                                         strAux.ToString, _
                                         objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT * FROM LOGEVENTOS")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function


End Class
