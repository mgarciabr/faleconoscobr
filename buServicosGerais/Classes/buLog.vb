﻿Imports System.Text
Imports plAppLibrary.clsCommonFunctions
Imports plAppLibrary.Enumerators
Imports System.Configuration

Public Class buLog

    Private mdbLOGEVENTOS As dbServicosGerais.dbLog

    ''' <summary>
    ''' Função para Inclusão de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal strLOGDESCR As String, ByVal strLOGORIGEM As String, ByVal strDTLOGDATA As String, ByVal intLOGTIPO As Integer, Optional ByVal intUSID As Integer = Nothing, Optional ByVal intMODID As Integer = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbLOGEVENTOS = New dbServicosGerais.dbLog
            Return mdbLOGEVENTOS.Insert(strLOGDESCR, strLOGORIGEM, strDTLOGDATA, intLOGTIPO, intUSID, intMODID, objTransaction)
            mdbLOGEVENTOS = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOGEVENTOS = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intLOGID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbLOGEVENTOS = New dbServicosGerais.dbLog
            Return mdbLOGEVENTOS.Delete(intLOGID, objTransaction)
            mdbLOGEVENTOS = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOGEVENTOS = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intLOGID As Integer, ByVal strLOGDESCR As String, ByVal strLOGORIGEM As String, ByVal strDTLOGDATA As String, ByVal intLOGTIPO As Integer, Optional ByVal intUSID As Integer = Nothing, Optional ByVal intMODID As Integer = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbLOGEVENTOS = New dbServicosGerais.dbLog
            Return mdbLOGEVENTOS.Update(intLOGID, strLOGDESCR, strLOGORIGEM, strDTLOGDATA, intLOGTIPO, intUSID, intMODID, objTransaction)
            mdbLOGEVENTOS = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOGEVENTOS = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbLOGEVENTOS = New dbServicosGerais.dbLog
            Return mdbLOGEVENTOS.UpdateBatch(dsDados, objTransaction)
            mdbLOGEVENTOS = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOGEVENTOS = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela LOGEVENTOS.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/01/2014 15:50:10 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbLOGEVENTOS = New dbServicosGerais.dbLog
            Return mdbLOGEVENTOS.Query(strWhere, strOrderBy, objTransaction)
            mdbLOGEVENTOS = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOGEVENTOS = Nothing
        End Try
    End Function


End Class
