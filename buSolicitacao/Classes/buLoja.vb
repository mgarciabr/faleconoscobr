﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela LOJA
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 14/04/2014 11:55:40</remarks>
Public Class buLoja

    Private mdbLOJA As dbSolicitacao.dbLoja

    ''' <summary>
    ''' Função para Inclusão de dados na tabela LOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:55:40 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(Optional ByVal strLOJADESCR As String = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbLOJA = New dbSolicitacao.dbLoja
            Return mdbLOJA.Insert(strLOJADESCR, objTransaction)
            mdbLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOJA = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela LOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:55:40 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intLOJAID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbLOJA = New dbSolicitacao.dbLoja
            Return mdbLOJA.Delete(intLOJAID, objTransaction)
            mdbLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOJA = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela LOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:55:40 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intLOJAID As Integer, Optional ByVal strLOJADESCR As String = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbLOJA = New dbSolicitacao.dbLoja
            Return mdbLOJA.Update(intLOJAID, strLOJADESCR, objTransaction)
            mdbLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOJA = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela LOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:55:40 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbLOJA = New dbSolicitacao.dbLoja
            Return mdbLOJA.UpdateBatch(dsDados, objTransaction)
            mdbLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOJA = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela LOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:55:40 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbLOJA = New dbSolicitacao.dbLoja
            Return mdbLOJA.Query(strWhere, strOrderBy, objTransaction)
            mdbLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbLOJA = Nothing
        End Try
    End Function

End Class