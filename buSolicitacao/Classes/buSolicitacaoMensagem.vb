﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela SOLICITACAOMENSAGEM
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 14/04/2014 11:40:05</remarks>
Public Class buSolicitacaoMensagem

    Private mdbSOLICITACAOMENSAGEM As dbSolicitacao.dbSolicitacaoMensagem

    ''' <summary>
    ''' Função para Inclusão de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intSOLID As Integer, ByVal strSOLMSGDESCR As String, ByVal strDTSOLMSGDTCADASTRO As String, Optional ByVal intUSID As Integer = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAOMENSAGEM = New dbSolicitacao.dbSolicitacaoMensagem
            Return mdbSOLICITACAOMENSAGEM.Insert(intSOLID, strSOLMSGDESCR, strDTSOLMSGDTCADASTRO, intUSID, objTransaction)
            mdbSOLICITACAOMENSAGEM = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAOMENSAGEM = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intSOLMSGID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAOMENSAGEM = New dbSolicitacao.dbSolicitacaoMensagem
            Return mdbSOLICITACAOMENSAGEM.Delete(intSOLMSGID, objTransaction)
            mdbSOLICITACAOMENSAGEM = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAOMENSAGEM = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intSOLMSGID As Integer, ByVal intSOLID As Integer, ByVal strSOLMSGDESCR As String, ByVal strDTSOLMSGDTCADASTRO As String, Optional ByVal intUSID As Integer = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAOMENSAGEM = New dbSolicitacao.dbSolicitacaoMensagem
            Return mdbSOLICITACAOMENSAGEM.Update(intSOLMSGID, intSOLID, strSOLMSGDESCR, strDTSOLMSGDTCADASTRO, intUSID, objTransaction)
            mdbSOLICITACAOMENSAGEM = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAOMENSAGEM = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAOMENSAGEM = New dbSolicitacao.dbSolicitacaoMensagem
            Return mdbSOLICITACAOMENSAGEM.UpdateBatch(dsDados, objTransaction)
            mdbSOLICITACAOMENSAGEM = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAOMENSAGEM = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSOLICITACAOMENSAGEM = New dbSolicitacao.dbSolicitacaoMensagem
            Return mdbSOLICITACAOMENSAGEM.Query(strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAOMENSAGEM = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAOMENSAGEM = Nothing
        End Try
    End Function

    ' ''' <summary>
    ' ''' Função para Consulta de dados na tabela SOLICITACAOMENSAGEM com inner join em SOLICITACAOMENSAGEM e em USUARIO
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks>08/05/2014 - Implementada por Maria Beatriz</remarks>
    'Public Function QueryUsuario(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
    '                      Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
    '    Try
    '        mdbSOLICITACAOMENSAGEM = New dbSolicitacao.dbSolicitacaoMensagem
    '        Return mdbSOLICITACAOMENSAGEM.QueryUsuario(strWhere, strOrderBy, objTransaction)
    '        mdbSOLICITACAOMENSAGEM = Nothing
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        mdbSOLICITACAOMENSAGEM = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAOMENSAGEM com inner join em SOLICITACAO e em USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>08/05/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryUsuario(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Try
            mdbSOLICITACAOMENSAGEM = New dbSolicitacao.dbSolicitacaoMensagem
            Return mdbSOLICITACAOMENSAGEM.QueryUsuario(strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAOMENSAGEM = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAOMENSAGEM = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAOMENSAGEM com inner join em SOLICITACAO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>08/05/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryPrimeiraMsg(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Try
            mdbSOLICITACAOMENSAGEM = New dbSolicitacao.dbSolicitacaoMensagem
            Return mdbSOLICITACAOMENSAGEM.QueryPrimeiraMsg(strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAOMENSAGEM = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAOMENSAGEM = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para consulta de quantidade de mensagens
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>13/06/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryCount(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Try
            mdbSOLICITACAOMENSAGEM = New dbSolicitacao.dbSolicitacaoMensagem
            Return mdbSOLICITACAOMENSAGEM.QueryCount(strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAOMENSAGEM = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAOMENSAGEM = Nothing
        End Try
    End Function

End Class
