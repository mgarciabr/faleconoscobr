﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela SUBASSUNTO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 16/05/2014 15:14:57</remarks>
Public Class buSubassunto

    Private mdbSUBASSUNTO As dbSolicitacao.dbSubassunto

    ''' <summary>
    ''' Função para Inclusão de dados na tabela SUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:14:57 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal strSBADESCR As String, ByVal intSBASL As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSUBASSUNTO = New dbSolicitacao.dbSubassunto
            Return mdbSUBASSUNTO.Insert(strSBADESCR, intSBASL, objTransaction)
            mdbSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela SUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:14:57 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intSBAID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSUBASSUNTO = New dbSolicitacao.dbSubassunto
            Return mdbSUBASSUNTO.Delete(intSBAID, objTransaction)
            mdbSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:14:57 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intSBAID As Integer, ByVal strSBADESCR As String, ByVal intSBASL As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSUBASSUNTO = New dbSolicitacao.dbSubassunto
            Return mdbSUBASSUNTO.Update(intSBAID, strSBADESCR, intSBASL, objTransaction)
            mdbSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela SUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:14:57 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSUBASSUNTO = New dbSolicitacao.dbSubassunto
            Return mdbSUBASSUNTO.UpdateBatch(dsDados, objTransaction)
            mdbSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:14:57 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSUBASSUNTO = New dbSolicitacao.dbSubassunto
            Return mdbSUBASSUNTO.Query(strWhere, strOrderBy, objTransaction)
            mdbSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTO = Nothing
        End Try
    End Function

End Class