﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela ASSUNTO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 14/04/2014 11:46:07</remarks>
Public Class buAssunto

    Private mdbASSUNTO As dbSolicitacao.dbAssunto

    ''' <summary>
    ''' Função para Inclusão de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intASSUNTOSTATUS As Integer, Optional ByVal strASSUNTODESCR As String = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbASSUNTO = New dbSolicitacao.dbAssunto
            Return mdbASSUNTO.Insert(intASSUNTOSTATUS, strASSUNTODESCR, objTransaction)
            mdbASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intASSUNTOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbASSUNTO = New dbSolicitacao.dbAssunto
            Return mdbASSUNTO.Delete(intASSUNTOID, objTransaction)
            mdbASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intASSUNTOID As Integer, ByVal intASSUNTOSTATUS As Integer, Optional ByVal strASSUNTODESCR As String = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbASSUNTO = New dbSolicitacao.dbAssunto
            Return mdbASSUNTO.Update(intASSUNTOID, intASSUNTOSTATUS, strASSUNTODESCR, objTransaction)
            mdbASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbASSUNTO = New dbSolicitacao.dbAssunto
            Return mdbASSUNTO.UpdateBatch(dsDados, objTransaction)
            mdbASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbASSUNTO = New dbSolicitacao.dbAssunto
            Return mdbASSUNTO.Query(strWhere, strOrderBy, objTransaction)
            mdbASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTO com inner join em PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryPerfil(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbASSUNTO = New dbSolicitacao.dbAssunto
            Return mdbASSUNTO.QueryPerfil(strWhere, strOrderBy, objTransaction)
            mdbASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTO = Nothing
        End Try
    End Function

End Class