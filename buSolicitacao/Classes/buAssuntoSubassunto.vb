﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela ASSUNTOSUBASSUNTO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 19/05/2014 09:38:26</remarks>
Public Class buAssuntoSubassunto

    Private mdbASSUNTOSUBASSUNTO As dbSolicitacao.dbASSUNTOSUBASSUNTO

    ''' <summary>
    ''' Função para Inclusão de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:26 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intASSUNTOID As Integer, ByVal intSBAID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbASSUNTOSUBASSUNTO = New dbSolicitacao.dbASSUNTOSUBASSUNTO
            Return mdbASSUNTOSUBASSUNTO.Insert(intASSUNTOID, intSBAID, objTransaction)
            mdbASSUNTOSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTOSUBASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intASSUBID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbASSUNTOSUBASSUNTO = New dbSolicitacao.dbASSUNTOSUBASSUNTO
            Return mdbASSUNTOSUBASSUNTO.Delete(intASSUBID, objTransaction)
            mdbASSUNTOSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTOSUBASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intASSUBID As Integer, ByVal intASSUNTOID As Integer, ByVal intSBAID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbASSUNTOSUBASSUNTO = New dbSolicitacao.dbASSUNTOSUBASSUNTO
            Return mdbASSUNTOSUBASSUNTO.Update(intASSUBID, intASSUNTOID, intSBAID, objTransaction)
            mdbASSUNTOSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTOSUBASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbASSUNTOSUBASSUNTO = New dbSolicitacao.dbASSUNTOSUBASSUNTO
            Return mdbASSUNTOSUBASSUNTO.UpdateBatch(dsDados, objTransaction)
            mdbASSUNTOSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTOSUBASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbASSUNTOSUBASSUNTO = New dbSolicitacao.dbASSUNTOSUBASSUNTO
            Return mdbASSUNTOSUBASSUNTO.Query(strWhere, strOrderBy, objTransaction)
            mdbASSUNTOSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTOSUBASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTOSUBASSUNTO com INNER JOIN em ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>27/10/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryAssunto(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbASSUNTOSUBASSUNTO = New dbSolicitacao.dbASSUNTOSUBASSUNTO
            Return mdbASSUNTOSUBASSUNTO.QueryAssunto(strWhere, strOrderBy, objTransaction)
            mdbASSUNTOSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTOSUBASSUNTO = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTOSUBASSUNTO com INNER JOIN em ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>27/10/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QuerySubassunto(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbASSUNTOSUBASSUNTO = New dbSolicitacao.dbASSUNTOSUBASSUNTO
            Return mdbASSUNTOSUBASSUNTO.QuerySubassunto(strWhere, strOrderBy, objTransaction)
            mdbASSUNTOSUBASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbASSUNTOSUBASSUNTO = Nothing
        End Try
    End Function
End Class