﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela SOLICITACAO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 14/04/2014 11:32:58</remarks>
Public Class buSolicitacao

    Private mdbSOLICITACAO As dbSolicitacao.dbSolicitacao

    ''' <summary>
    ''' Função para Inclusão de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal strSOLPROTOCOLO As String, ByVal intASSUNTOID As Integer, ByVal intLOJAID As Integer, ByVal strSOLEMAIL As String,
                           ByVal strDTSOLDTCADASTRO As String, ByVal intSOLSTATUS As Integer, ByVal intASSUBID As Integer, Optional ByVal intUSIDCAPTURA As Integer = Nothing,
                           Optional ByVal intUSIDENCERRA As Integer = Nothing, Optional ByVal strSOLTELEFONE As String = Nothing,
                           Optional ByVal strSOLNUMEROPASSAPORTE As String = Nothing, Optional ByVal strSOLDTCAPTURA As String = "",
                           Optional ByVal strSOLDTENCERRA As String = "", Optional ByVal intUSIDULTIMO As Integer = Nothing, Optional ByVal strSOLEMPRESA As String = "",
                           Optional ByVal strSOLNOME As String = "", Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.Insert(strSOLPROTOCOLO, intASSUNTOID, intLOJAID, strSOLEMAIL, strDTSOLDTCADASTRO, intSOLSTATUS,
                                         intASSUBID, intUSIDCAPTURA, intUSIDENCERRA, strSOLTELEFONE, strSOLNUMEROPASSAPORTE,
                                         strSOLDTCAPTURA, strSOLDTENCERRA, intUSIDULTIMO, strSOLEMPRESA, strSOLNOME, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intSOLID As Integer,
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.Delete(intSOLID, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intSOLID As Integer, ByVal strSOLPROTOCOLO As String, ByVal intASSUNTOID As Integer, ByVal intLOJAID As Integer, ByVal strSOLEMAIL As String, ByVal strDTSOLDTCADASTRO As String, ByVal intSOLSTATUS As Integer, ByVal intASSUBID As Integer, Optional ByVal intUSIDCAPTURA As Integer = Nothing, Optional ByVal intUSIDENCERRA As Integer = Nothing, Optional ByVal strSOLTELEFONE As String = Nothing, Optional ByVal strSOLNUMEROPASSAPORTE As String = Nothing, Optional ByVal intUSIDULTIMO As Integer = Nothing,
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.Update(intSOLID, strSOLPROTOCOLO, intASSUNTOID, intLOJAID, strSOLEMAIL, strDTSOLDTCADASTRO, intSOLSTATUS, intASSUBID, intUSIDCAPTURA, intUSIDENCERRA, strSOLTELEFONE, strSOLNUMEROPASSAPORTE, intUSIDULTIMO, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/04/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateStatus(ByVal intSOLID As Integer, ByVal intSOLSTATUS As Integer, Optional ByVal intUSIDCAPTURA As Integer = Nothing, Optional ByVal intUSIDENCERRA As Integer = Nothing, Optional ByVal strSOLDTCAPTURA As String = "", Optional ByVal strSOLDTENCERRA As String = "",
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.UpdateStatus(intSOLID, intSOLSTATUS, intUSIDCAPTURA, intUSIDENCERRA, strSOLDTCAPTURA, strSOLDTENCERRA, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>23/05/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateProtocolo(ByVal intSOLID As Integer, ByVal strSOLPROTOCOLO As String,
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.UpdateProtocolo(intSOLID, strSOLPROTOCOLO, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>13/06/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateUltimo(ByVal intSOLID As Integer, ByVal intUSIDULTIMO As Integer,
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.UpdateUltimo(intSOLID, intUSIDULTIMO, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.Query(strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO, SOLICITACAOMENSAGEM, LOJA, ASSUNTO e USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function QueryUltimo(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.QueryUltimo(strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO, SOLICITACAOMENSAGEM, LOJA, ASSUNTO e USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function ObterSolicitacoesMaster(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.ObterSolicitacoesMaster(strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO, SOLICITACAOMENSAGEM, LOJA, ASSUNTO e USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function ObterSolicitacoesInternas(intUsuarioId As Int32, Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.ObterSolicitacoesInternas(intUsuarioId, strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO, SOLICITACAOMENSAGEM, LOJA, ASSUNTO e USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function ObterSolicitacoesCliente(intUsuarioId As Int32, Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.ObterSolicitacoesCliente(intUsuarioId, strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO com TOP 1
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function QueryTop1(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSOLICITACAO = New dbSolicitacao.dbSolicitacao
            Return mdbSOLICITACAO.QueryTop1(strWhere, strOrderBy, objTransaction)
            mdbSOLICITACAO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSOLICITACAO = Nothing
        End Try
    End Function
End Class
