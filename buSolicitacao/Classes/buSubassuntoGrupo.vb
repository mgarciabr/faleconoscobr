﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela SUBASSUNTOGRUPO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 16/05/2014 15:45:01</remarks>
Public Class buSubassuntoGrupo

    Private mdbSUBASSUNTOGRUPO As dbSolicitacao.dbSubassuntoGrupo

    ''' <summary>
    ''' Função para Inclusão de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:01 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intSBAID As Integer, ByVal intGRUPOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSUBASSUNTOGRUPO = New dbSolicitacao.dbSubassuntoGrupo
            Return mdbSUBASSUNTOGRUPO.Insert(intSBAID, intGRUPOID, objTransaction)
            mdbSUBASSUNTOGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTOGRUPO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:02 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intSBAID As Integer, ByVal intGRUPOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSUBASSUNTOGRUPO = New dbSolicitacao.dbSubassuntoGrupo
            Return mdbSUBASSUNTOGRUPO.Delete(intSBAID, intGRUPOID, objTransaction)
            mdbSUBASSUNTOGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTOGRUPO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:02 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intSBAID As Integer, ByVal intGRUPOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSUBASSUNTOGRUPO = New dbSolicitacao.dbSubassuntoGrupo
            Return mdbSUBASSUNTOGRUPO.Update(intSBAID, intGRUPOID, objTransaction)
            mdbSUBASSUNTOGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTOGRUPO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:02 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbSUBASSUNTOGRUPO = New dbSolicitacao.dbSubassuntoGrupo
            Return mdbSUBASSUNTOGRUPO.UpdateBatch(dsDados, objTransaction)
            mdbSUBASSUNTOGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTOGRUPO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:02 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSUBASSUNTOGRUPO = New dbSolicitacao.dbSubassuntoGrupo
            Return mdbSUBASSUNTOGRUPO.Query(strWhere, strOrderBy, objTransaction)
            mdbSUBASSUNTOGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTOGRUPO = Nothing
        End Try
    End Function

    Public Function QueryEMAILRespChamado(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbSUBASSUNTOGRUPO = New dbSolicitacao.dbSubassuntoGrupo
            Return mdbSUBASSUNTOGRUPO.QueryEMAILRespChamado(strWhere, strOrderBy, objTransaction)
            mdbSUBASSUNTOGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbSUBASSUNTOGRUPO = Nothing
        End Try
    End Function

End Class