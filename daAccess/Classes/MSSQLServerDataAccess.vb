﻿Imports Microsoft.VisualBasic
Imports System.Web
Imports System.Data.SqlClient
Imports System.Configuration

Public Class MSSQLServerDataAccess

    Public mcnConexao As SqlConnection

    Private Sub Open(ByVal objTransaction As plAppLibrary.Transaction)
        Dim strConexao As String
        Try
            If objTransaction Is Nothing Then
                strConexao = ConfigurationSettings.AppSettings("APPConnectionString").ToString
                mcnConexao = New SqlConnection(strConexao)
                mcnConexao.Open()
            Else
                If Not objTransaction.Opened Then
                    'Este método também é responsável por abrir a conexão
                    objTransaction.IniciaTransacao()
                    mcnConexao = objTransaction.SqlConection
                Else
                    mcnConexao = objTransaction.SqlConection
                End If
            End If

            'Catch ex As Exception
            '    Throw ex
        Finally
            strConexao = ""
        End Try
    End Sub

    Private Sub Close(ByVal objtransaction As plAppLibrary.Transaction)
        Try
            If objtransaction Is Nothing Then
                If Not mcnConexao Is Nothing Then
                    SqlConnection.ClearPool(mcnConexao)
                    mcnConexao.Close()
                End If
            End If
            'Catch ex As Exception
            '    Throw ex
        Finally

            mcnConexao = Nothing
        End Try
    End Sub

    Public Function ReturnQuery(ByVal strSQL As String, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing, _
                                Optional ByVal blnCreateNewConnection As Boolean = True) As Data.DataSet
        Dim cmdExecute As SqlCommand
        Dim dsRetorno As Data.DataSet

        Try
            'Abrindo a conexão.
            If blnCreateNewConnection Then
                Open(objTransaction)
            End If

            Try
                'Comando para retorno dos dados.
                cmdExecute = New SqlCommand
                With cmdExecute
                    '.CommandTimeout = 0
                    .CommandText = strSQL
                    If Not objTransaction Is Nothing Then
                        .Transaction = objTransaction.SqlTransaction
                    End If
                    .Connection = mcnConexao
                End With

                'Criando instância do Dataset.
                dsRetorno = New Data.DataSet

                'Preenchendo o dataset.
                Dim daExecute As New SqlDataAdapter(cmdExecute)
                daExecute.Fill(dsRetorno)

                Return dsRetorno
                'Catch ex As Exception
                '    Throw ex
            Finally
                cmdExecute = Nothing
            End Try
            'Catch ex As Exception
            '    Throw ex
        Finally
            'Fechando a conexão.
            If blnCreateNewConnection Then
                Close(objTransaction)
            End If
        End Try

    End Function

    Public Function UpdateBatch(ByVal dsDados As Data.DataSet, ByVal strSQL As String, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            Open(objTransaction)
            Try
                Dim mydaDados As New SqlDataAdapter
                If objTransaction Is Nothing Then
                    mydaDados.SelectCommand = New SqlCommand(strSQL, mcnConexao)
                Else
                    mydaDados.SelectCommand = New SqlCommand(strSQL, mcnConexao, objTransaction.SqlTransaction)
                End If
                Dim custCB As SqlCommandBuilder = New SqlCommandBuilder(mydaDados)
                Dim custDS As New Data.DataSet

                mydaDados.Fill(custDS)

                custDS = dsDados.GetChanges
                If Not custDS Is Nothing Then
                    mydaDados.Update(custDS)
                End If

                'Grava LOG do comando executado.
                GravaLogComando(strSQL, custDS)

                Return True

                'Catch ex As Exception
                '    Throw ex
            Finally
            End Try
            'Catch ex As Exception
            '    Throw ex
        Finally
            Close(objTransaction)
        End Try
    End Function

    Public Function Execute(ByVal strSQL As String, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim cmdExecute As SqlCommand

        Try
            Open(objTransaction)
            Try
                If objTransaction Is Nothing Then
                    cmdExecute = New SqlCommand(strSQL, mcnConexao)
                Else
                    cmdExecute = New SqlCommand(strSQL, mcnConexao, objTransaction.SqlTransaction)
                End If

                cmdExecute.CommandTimeout = 90
                cmdExecute.ExecuteNonQuery()

                'Grava LOG do comando executado.
                GravaLogComando(strSQL)

                Return True

            Catch ex As Exception
                Throw ex
            End Try
            'Catch ex As Exception
            '    Throw ex
        Finally
            Close(objTransaction)
            cmdExecute = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para executar um comando no SQL
    ''' </summary>
    ''' <param name="cmdExecute">comando a ser executado</param>
    ''' <param name="objTransaction"></param>
    ''' <returns></returns>
    ''' <remarks>Implementada por Juliana Lima em 08/05/2012</remarks>
    Public Function ExecuteCommand(ByVal cmdExecute As SqlCommand, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            Open(objTransaction)

            Try
                cmdExecute.Connection = mcnConexao

                If Not IsNothing(objTransaction) Then
                    cmdExecute.Transaction = objTransaction.SqlTransaction
                End If
                cmdExecute.CommandTimeout = 90
                cmdExecute.ExecuteNonQuery()

                'Grava LOG do comando executado.
                GravaLogComando(cmdExecute.CommandText)

                Return True

            Catch ex As Exception
                Throw ex
            End Try
            'Catch ex As Exception
            '    Throw ex
        Finally
            Close(objTransaction)
            cmdExecute = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Método para Executar instruções no SQL Server, retornando um resultado logo em seguida.
    ''' </summary>
    ''' <param name="cmdExecute">comando a ser executado</param>
    ''' <param name="objTransaction"></param>
    ''' <param name="StrSelectSQL">select pra retorno</param>
    ''' <returns>O valor da consulta caso o comando de certo; nothing, caso contrário
    ''' Ou se não houver select de retorno, retorna true ou false</returns>
    ''' <remarks>Implementado por Juliana Lima em 13/04/2012</remarks>
    Public Function ExecuteScalar(ByVal cmdExecute As SqlCommand, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing, _
                                  Optional ByVal StrSelectSQL As String = "") As Object

        Try
            ExecuteScalar = Nothing
            Open(objTransaction)
            Try
                cmdExecute.Connection = mcnConexao
                cmdExecute.CommandTimeout = 90
                cmdExecute.ExecuteNonQuery()

                If StrSelectSQL <> "" Then
                    cmdExecute.CommandText = StrSelectSQL
                    ExecuteScalar = cmdExecute.ExecuteScalar()
                Else
                    ExecuteScalar = True
                End If

                'Grava LOG do comando executado.
                GravaLogComando(cmdExecute.CommandText)

            Catch ex As Exception
                Throw ex
            End Try
            'Catch ex As Exception
            '    Throw ex
        Finally
            Close(objTransaction)
            cmdExecute = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Método para Executar instruções DML no SQL Server, retornando um resultado logo em seguida.
    ''' </summary>
    '''<param name="strSQL">string com o comando a ser executado</param>
    ''' <param name="objTransaction"></param>
    ''' <param name="StrSelectSQL">select pra retorno</param>
    ''' <returns>O valor da consulta caso o comando de certo; nothing, caso contrário</returns>
    ''' <remarks>Implementado por Juliana Lima em 11/06/2012</remarks>
    Public Function ExecuteScalar(ByVal strSQL As String, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing, _
                                  Optional ByVal StrSelectSQL As String = "") As Object
        Dim cmdExecute As SqlCommand

        Try
            ExecuteScalar = Nothing
            Open(objTransaction)

            Try
                cmdExecute = New SqlClient.SqlCommand
                With cmdExecute
                    .CommandTimeout = 90
                    .CommandText = strSQL
                    .Connection = mcnConexao
                    If Not objTransaction Is Nothing Then
                        If Not objTransaction.SqlTransaction Is Nothing Then
                            .Transaction = objTransaction.SqlTransaction
                        End If
                    End If
                End With

                cmdExecute.ExecuteNonQuery()

                If StrSelectSQL <> "" Then
                    cmdExecute.CommandText = StrSelectSQL
                    ExecuteScalar = cmdExecute.ExecuteScalar()
                Else
                    ExecuteScalar = True
                End If

                'Grava LOG do comando executado.
                GravaLogComando(strSQL)

            Catch ex As Exception
                Throw ex
            End Try

            'Catch ex As Exception
            '    Throw ex
        Finally
            Close(objTransaction)
            cmdExecute = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Função para guardar todos os comandos executados no banco de dados.
    ''' </summary>
    ''' <param name="strComandoExecutado"></param>
    ''' <returns></returns>
    ''' <remarks>Implementado por João Paulo em 23/01/2013.</remarks>
    Private Sub GravaLogComando(ByVal strComandoExecutado As String, Optional ByVal dsUpdateBatch As DataSet = Nothing)
        Dim strAux As Text.StringBuilder
        Dim cmdExecute As SqlCommand

        Dim intLogID As Integer
        Dim strDateAux As String

        Dim myConn As SqlConnection

        Try
            If InStr(strComandoExecutado, " LOGEVENTOS ") Then
                'Trata-se de um LOG do LOG. Não precisa registrar.
                Exit Sub
            End If

            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO LOGEVENTOS (LOGDESCR, LOGDATAHORA, LOGTIPO, USID, LOGORIGEM, MODID) ")


            '
            'Implementado em 10/04/2013 - João Paulo.
            'Registra no LOG a operação realizada considerando o UPDATEBATCH.
            '
            If dsUpdateBatch Is Nothing Then
                strAux.Append("VALUES ('").Append(TrataTextos(strComandoExecutado)).Append("', GETDATE(), 4, ")
            Else
                strDateAux = Format(System.DateTime.Now, "yyyy-MM-dd hh:mm")
                'strAux.Append("VALUES ('").Append("LOG Gravado para Comando UPDATEBATCH: ").Append(TrataTextos(strComandoExecutado)).Append("', '") _
                '    .Append(FormataData_HoraSQL(strDateAux)).Append("', 4, ")
                strAux.Append("VALUES ('").Append("LOG Gravado para Comando UPDATEBATCH: ").Append(TrataTextos(strComandoExecutado)).Append("', '") _
                    .Append(strDateAux).Append("', 4, ")
            End If



            If theApplicationUser = "" Then
                strAux.Append("NULL")
            Else
                strAux.Append(theApplicationUser)
            End If
            strAux.Append(", ")
            If theMachine = "" Then
                strAux.Append("NULL")
            Else
                strAux.Append("'").Append(TrataTextos(theMachine)).Append("'")
            End If
            strAux.Append(", NULL)")

            'Gravando os dados
            If dsUpdateBatch Is Nothing Then
                myConn = NewConnection()

                'Não há UpdateBatch, executa tudo normal.
                cmdExecute = New SqlCommand(strAux.ToString, myConn)
                cmdExecute.CommandTimeout = 90
                cmdExecute.ExecuteNonQuery()

                CloseConnection(myConn)
            Else
                '
                'Implementado em 10/04/2013 - João Paulo.
                'Registra no LOG a operação realizada, considerando o UPDATEBATCH.
                '
                '---------------------------------------------------------------

                myConn = NewConnection()

                'Criar o LOG com Scalar, para obter o ID do último registro.
                cmdExecute = New SqlCommand(strAux.ToString, myConn)
                cmdExecute.CommandTimeout = 90
                cmdExecute.ExecuteNonQuery()
                cmdExecute.CommandText = New Text.StringBuilder("SELECT LOGID FROM LOGEVENTOS WHERE LOGDESCR = 'LOG Gravado para Comando UPDATEBATCH: ") _
                    .Append(TrataTextos(strComandoExecutado)).Append("' AND LOGTIPO = 4 AND LOGDATAHORA = '").Append(FormataData_HoraSQL(strDateAux)) _
                    .Append("' ORDER BY LOGID DESC").ToString

                Try
                    'Obtendo o ID do LOG.
                    intLogID = cmdExecute.ExecuteScalar()

                    'Declarando novas variáveis.
                    Dim dsDetailedCommand As DataSet
                    Dim myRowCommand As DataRow
                    Dim txtMyCommand As Text.StringBuilder
                    Dim cmdExecute2 As SqlCommand

                    'Obtendo a estrutura da tabela LOGDetalhes.

                    cmdExecute2 = New SqlCommand
                    With cmdExecute2
                        .CommandText = "SELECT * FROM LOGEVENTOSDETALHES WHERE LOGDETID = -1"
                        .Connection = myConn
                    End With

                    'Criando instância do Dataset.
                    dsDetailedCommand = New Data.DataSet

                    'Preenchendo o dataset.
                    Dim daExecute As New SqlDataAdapter(cmdExecute)
                    daExecute.Fill(dsDetailedCommand)

                    cmdExecute2 = Nothing

                    'Gravando os detalhes do LOG.
                    For Each myRow As DataRow In dsUpdateBatch.Tables(0).Rows

                        'Inicializando a variável para gravar o comando.
                        txtMyCommand = New Text.StringBuilder
                        txtMyCommand.Append("Atualização de dados para os seguintes valores: ")

                        For intCol As Integer = 0 To dsUpdateBatch.Tables(0).Columns.Count - 1
                            If txtMyCommand.ToString.Length > 0 Then
                                txtMyCommand.Append("; ")
                            End If
                            txtMyCommand.Append(dsUpdateBatch.Tables(0).Columns(intCol).ColumnName)
                            txtMyCommand.Append(" = '")
                            txtMyCommand.Append(myRow(intCol))
                            txtMyCommand.Append("'")
                        Next

                        'Gravando o comando.
                        myRowCommand = dsDetailedCommand.Tables(0).NewRow
                        myRowCommand("LOGID") = intLogID
                        myRowCommand("LOGDESCR") = txtMyCommand.ToString
                        dsDetailedCommand.Tables(0).Rows.Add(myRowCommand)
                    Next

                    'Executando o comando no banco.
                    Dim mydaDados As New SqlDataAdapter
                    mydaDados.SelectCommand = New SqlCommand("SELECT * FROM LOGEVENTOSDETALHES WHERE LOGDETID = -1", myConn)
                    Dim custCB As SqlCommandBuilder = New SqlCommandBuilder(mydaDados)
                    Dim custDS As New Data.DataSet
                    mydaDados.Fill(custDS)
                    custDS = dsDetailedCommand.GetChanges
                    If Not custDS Is Nothing Then
                        mydaDados.Update(custDS)
                    End If

                    'Liberando variáveis.
                    dsDetailedCommand = Nothing
                    myRowCommand = Nothing
                    txtMyCommand = Nothing
                    mydaDados = Nothing
                    custCB = Nothing
                    custDS = Nothing

                Catch ex As Exception
                    'Erro ao gravar o LOG do UpdateBatch.
                End Try
                '-----------------------------------------------------------------

                CloseConnection(myConn)

            End If

        Catch ex As Exception
            'Não faz tratamento de erro para não gerar problemas decorrente apenas de registros de informação.
        Finally
            cmdExecute = Nothing
            strAux = Nothing
        End Try
    End Sub

    Private Function TrataTextos(ByVal strTexto As String) As String
        Return Replace(strTexto, "'", "''")
    End Function

    ''' <summary>
    ''' Função para formatar Data/Hora no padrão do SQL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function FormataData_HoraSQL(ByVal strData_Hora As String) As String
        Dim strAux As System.Text.StringBuilder
        Try
            strAux = New System.Text.StringBuilder
            If IsDate(strData_Hora) Then
                strAux.Append(Year(CDate(strData_Hora)))
                strAux.Append("-")
                strAux.Append(Format(Month(CDate(strData_Hora)), "00"))
                strAux.Append("-")
                strAux.Append(Format(Day(CDate(strData_Hora)), "00"))
                strAux.Append(" ")
                strAux.Append(Format(Hour(CDate(strData_Hora)), "00"))
                strAux.Append(":")
                strAux.Append(Format(Minute(CDate(strData_Hora)), "00"))
                strAux.Append(":")
                strAux.Append(Format(Second(CDate(strData_Hora)), "00"))
            End If
            Return strAux.ToString

        Catch ex As Exception
            Throw ex
        Finally
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para sempre abrir uma nova conexão com o banco de dados.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Implementado em 02/05/2013 - João Paulo.</remarks>
    Private Function NewConnection() As SqlConnection
        Dim strConexao As String
        Dim myConn As SqlConnection

        Try
            strConexao = ConfigurationSettings.AppSettings("APPConnectionString").ToString

            myConn = New SqlConnection(strConexao)
            myConn.Open()

            Return myConn
        Finally
            strConexao = ""
        End Try
    End Function

    ''' <summary>
    ''' Função para fechar uma conexão aberta.
    ''' </summary>
    ''' <remarks>Implementado em 02/05/2013 - João Paulo.</remarks>
    Private Sub CloseConnection(ByRef myConn As SqlConnection)
        Try
            If Not myConn Is Nothing Then
                myConn.Close()
            End If
        Finally
            mcnConexao = Nothing
        End Try
    End Sub

End Class
