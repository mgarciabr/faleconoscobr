﻿Public Module GlobalUserRegister

    Private gstrUser As String
    Private gstrMachine As String

    Public Property theApplicationUser As String
        Get
            'Protegendo a variavel,caso ela seja requisitada,é retornado apenas o valor dela
            Return gstrUser
        End Get
        Set(ByVal value As String)
            'recebe o valor e guarda na variavel nome
            gstrUser = value
        End Set
    End Property

    Public Property theMachine As String
        Get
            'Protegendo a variavel,caso ela seja requisitada,é retornado apenas o valor dela
            Return gstrMachine
        End Get
        Set(ByVal value As String)
            'recebe o valor e guarda na variavel nome
            gstrMachine = value
        End Set
    End Property

End Module
