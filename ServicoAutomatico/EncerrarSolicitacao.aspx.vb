﻿Imports plAppLibrary.Enumerators
Imports System.Data
Imports System.Text

Partial Class EncerrarSolicitacao
    Inherits System.Web.UI.Page

    Private mbuSolicitacao As buSolicitacao.buSolicitacao
    Private mbuSolMensagem As buSolicitacao.buSolicitacaoMensagem

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Encerrar()
        Catch ex As Exception

        End Try
    End Sub


    Public Sub Encerrar()
        Dim dsDados, dsAux As DataSet
        Dim strbAux As New StringBuilder
        Dim dtCadastro As DateTime
        Dim tsSL As TimeSpan
        Try
            mbuSolicitacao = New buSolicitacao.buSolicitacao
            mbuSolMensagem = New buSolicitacao.buSolicitacaoMensagem

            'solicitações abertas
            strbAux.Append("SOLSTATUS = ").Append(enStatusSolicitacao.Aberta)
            dsDados = mbuSolicitacao.Query(strbAux.ToString)

            For Each drRow As DataRow In dsDados.Tables(0).Rows()
                strbAux = New StringBuilder
                strbAux.Append("SOLICITACAO.SOLID = ").Append(drRow.Item("SOLID"))
                dsAux = mbuSolMensagem.QueryUsuario(strbAux.ToString, "SOLMSGDTCADASTRO DESC")
                If dsAux.Tables(0).Rows(0).Item("USID").ToString.Equals("0") Then 'se a ultima mensagem foi do cliente
                    'verificar a quanto tempo está aberta
                    dtCadastro = drRow.Item("SOLDTCADASTRO")
                    tsSL = New TimeSpan(3, 0, 0, 0) '3 dias

                    If DateTime.Today.Subtract(dtCadastro) > tsSL Then
                        mbuSolicitacao.UpdateStatus(drRow.Item("SOLID"), enStatusSolicitacao.Encerrada)
                    End If
                End If
            Next

        Catch ex As Exception
            mbuSolicitacao = Nothing
            mbuSolMensagem = Nothing
        End Try
    End Sub
End Class
