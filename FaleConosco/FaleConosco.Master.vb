﻿Imports plAppLibrary.Enumerators

Public Class FaleConosco
    Inherits System.Web.UI.MasterPage

    Private myProfile As New ProfileCommonMakro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Verifica se o usuario clicou em esqueci minha senha.
            If IsNothing(Session("Esqueci")) Then
                If IsNothing(Session("Logado")) Then
                    Response.Redirect("CadastroSolicitacao.aspx", False) 'redireciono pra qual login? como vou saber o que a pessoa é se ela não tá logada?
                Else
                    Session("Logado") = Session("Logado")
                    If Session("Logado") = 1 Then
                        lblBemVindo.Text = New StringBuilder().Append(", ").Append(myProfile.UserInfo.Email).ToString
                        Select Case myProfile.UserInfo.PerfilID
                            Case enUserPerfil.Cliente
                                liUsuario.Visible = False
                                liRelatorio.Visible = False
                                liSol.Visible = True
                                liSol.Attributes.Add("class", "selecionado")
                                lnkSolicitacao.PostBackUrl = "~/Interface/ConsultaSolicitacao.aspx"
                            Case enUserPerfil.Gerente
                                liUsuario.Visible = True
                                liRelatorio.Visible = True
                                liSol.Visible = True
                                lnkUsuarios.PostBackUrl = "~/Interface/AlteracaoSenha.aspx"
                                lnkSolicitacao.PostBackUrl = "~/Interface/ConsultaSolicitacao.aspx"
                            Case enUserPerfil.Master
                                liUsuario.Visible = True
                                liRelatorio.Visible = True
                                liSol.Visible = True
                                lnkUsuarios.PostBackUrl = "~/Interface/Usuario.aspx"
                                lnkSolicitacao.PostBackUrl = "~/Interface/ConsultaSolicitacao.aspx"
                        End Select
                    Else
                        lblBemVindo.Text = ""
                    End If
                End If
            Else
                lnkSolicitacao.PostBackUrl = "~/Interface/EsquecimentoSenha.aspx"
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class