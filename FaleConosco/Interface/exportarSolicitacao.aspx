﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="exportarSolicitacao.aspx.vb" Inherits="FaleConosco.exportarSolicitacao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="gvSolicitacao" runat="server" AllowPaging="False"
                AutoGenerateColumns="False" EmptyDataText="Nenhuma solicitação cadastrada no momento" DataKeyNames="SOLID" CellPadding="0" class="grid">
                <Columns>
                    <asp:BoundField DataField="SOLPROTOCOLO" HeaderText="Protocolo" />
                    <asp:BoundField DataField="STATUSDESCR" HeaderText="Status" />
                    <asp:BoundField DataField="SOLDTCADASTRO" HeaderText="Cadastro" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="SOLNOME" HeaderText="Mensagem Enviada" />
                    <asp:BoundField DataField="primMensagem" HeaderText="Atendimento" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="SOLMSGDESCR" HeaderText="Resposta" />
                    <%--<asp:BoundField DataField="SOLMSGDTCADASTRO" HeaderText="Última Interação" DataFormatString="{0:dd/MM/yyyy}" />--%>
                    <asp:BoundField DataField="SOLEMAIL" HeaderText="E-mail Cliente" />
                    <asp:BoundField DataField="USEMAIL" HeaderText="E-mail Atendente" />
                    <%--<asp:BoundField DataField="LOJAREGIAO" HeaderText="Região" />--%>
                    <asp:BoundField DataField="LOJAID" HeaderText="Loja" />
                    <asp:BoundField DataField="ASSUNTODESCR" HeaderText="Assunto" />
                    <asp:BoundField DataField="SBADESCR" HeaderText="Subassunto" />
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
