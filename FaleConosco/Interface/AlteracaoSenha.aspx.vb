﻿Imports plAppLibrary.clsCommonFunctions
Imports plAppLibrary.Enumerators

Public Class AlteracaoSenha
    Inherits System.Web.UI.Page

    Private mbuUsuario As buLogin.buUsuario
    Private mbuLog As buServicosGerais.buLog
    Private myProfile As New ProfileCommonMakro

    Protected Sub btnAlterar_Click(sender As Object, e As EventArgs) Handles btnAlterar.Click
        Dim dsDados As New DataSet
        Try
            If txtAntiga.Text.Trim.Equals("") Or txtConfirmacao.Text.Trim.Equals("") Or txtNova.Text.Trim.Equals("") Then
                lblMensagem.Text = "Preencha todos os campos"
            Else
                mbuUsuario = New buLogin.buUsuario
                dsDados = mbuUsuario.Query(New StringBuilder().Append("USID = ").Append(myProfile.UserInfo.ID).ToString)
                If dsDados.Tables(0).Rows().Count > 0 Then
                    If txtAntiga.Text.Trim.Equals(Decrypt(dsDados.Tables(0).Rows(0).Item("USSENHA").ToString)) Then
                        If txtNova.Text.Equals(txtConfirmacao.Text) Then 'não diz nada sobre especificação de tamanho mínimo etc
                            mbuUsuario.UpdateSenha(myProfile.UserInfo.ID, Encrypt(txtNova.Text), False)
                            Session("Logado") = Nothing
                            PopupMensagem.Mensagem = "Senha alterada com sucesso"
                            PopupMensagem.ShowPopup()
                        Else
                            PopupMensagem.Mensagem = "Confirmação de senha incorreta"
                            PopupMensagem.ShowPopup()
                        End If
                    Else
                        PopupMensagem.Mensagem = "A senha antiga está incorreta"
                        PopupMensagem.ShowPopup()
                    End If
                End If
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "btnAlterar_Click de AlteracaoSenha.aspx", enLogStatus.enlsError)
        Finally
            mbuUsuario = Nothing
        End Try
    End Sub

    Public Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub

    Protected Sub PopupMensagem_OK() Handles PopupMensagem.OK
        Try
            Response.Redirect("LoginAdmin.aspx")
        Catch ex As Exception
            GravarLog(ex.Message, "PopupMensagem_OK", enLogStatus.enlsFailed)
        End Try
    End Sub

    Private Sub AlteracaoSenha_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If IsNothing(Session("Logado")) Then
                Response.Redirect("LoginAdmin.aspx", False)
            Else
                Session("Logado") = Session("Logado")
                If Session("Logado") <> 1 Or myProfile.UserInfo.PerfilID = enUserPerfil.Cliente Then
                    Response.Redirect("LoginAdmin.aspx", False)
                End If
            End If
            If Not IsPostBack Then
                CType(Page.Master.FindControl("liSol"), HtmlGenericControl).Attributes.Remove("class")
                CType(Page.Master.FindControl("liUsuario"), HtmlGenericControl).Attributes.Add("class", "selecionado")
                CType(Page.Master.FindControl("liRelatorio"), HtmlGenericControl).Attributes.Remove("class")
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "AlteracaoSenha_Load de AlteracaoSenha.aspx", enLogStatus.enlsError)
        End Try
    End Sub
End Class