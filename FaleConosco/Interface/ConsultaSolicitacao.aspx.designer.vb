﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ConsultaSolicitacao
    
    '''<summary>
    '''ValidationSummary1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ValidationSummary1 As Global.System.Web.UI.WebControls.ValidationSummary
    
    '''<summary>
    '''lblMensagem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMensagem As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlConsulta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlConsulta As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Panel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''txtProtocolo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProtocolo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPassaporte control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPassaporte As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtInicio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtInicio As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtFim control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFim As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlSituacao control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSituacao As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''btnConsultar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnConsultar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''litContador control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litContador As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''gvSolicitacao control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvSolicitacao As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''pnlConversa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlConversa As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''dvAuxMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dvAuxMsg As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''lblAux control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAux As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlNovaMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlNovaMsg As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''txtNovaMensagem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNovaMensagem As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlCaptura control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCaptura As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''btnCapturar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCapturar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnRetornar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRetornar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnEnviar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEnviar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnVoltar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnVoltar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnEncerrar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEncerrar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnCadastrar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCadastrar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''PopupMensagem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PopupMensagem As Global.FaleConosco.wucPopUpMensagem
End Class
