﻿Imports System
Imports plAppLibrary.clsCommonFunctions
Imports plAppLibrary.Enumerators

Public Class Login
    Inherits System.Web.UI.Page

    Private mbuUsuario As buLogin.buUsuario
    Private mbuLog As buServicosGerais.buLog
    Private myProfile As New ProfileCommonMakro
    Private mbuSolicitacao As New buSolicitacao.buSolicitacao

    Protected Sub btnProximo_Click(sender As Object, e As EventArgs) Handles btnProximo.Click
        Dim strbAux As New StringBuilder
        Dim dsDados As DataSet
        Try
            'verificar se é e-mail
            If validaEmail(txtEmail.Text) Then
                pnlSenha.Visible = True
                pnlBaixo.Visible = True
                txtEmail.Attributes.Add("style", "width: 100%;")
                btnProximo.Visible = False
                txtSenha.Focus()

                'procura na tabela de usuarios
                mbuUsuario = New buLogin.buUsuario
                strbAux.Append("USEMAIL = '").Append(txtEmail.Text).Append("'")
                dsDados = mbuUsuario.Query(strbAux.ToString)

                If dsDados.Tables(0).Rows().Count = 0 Then
                    'se for cliente, não pode mudar senha
                    btnEsqueci.Visible = False
                End If
            Else
                PopupMensagem.Mensagem = "Digite um e-mail válido"
                PopupMensagem.ShowPopup()
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "btnProximo_Click de Login.aspx", enLogStatus.enlsError)
        Finally
            mbuSolicitacao = Nothing
            dsDados = Nothing
        End Try
    End Sub

    Public Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles Me.Load
        Session("Logado") = Nothing
        myProfile.UserInfo.LimparPerfil()
    End Sub

    Function validaEmail(ByVal email As String) As Boolean
        Dim emailRegex As New System.Text.RegularExpressions.Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        Dim emailMatch As System.Text.RegularExpressions.Match = emailRegex.Match(email)
        Return emailMatch.Success
    End Function

    Protected Sub btnAcessar_Click(sender As Object, e As EventArgs) Handles btnAcessar.Click
        Dim strbAux As New StringBuilder
        Dim dsDados As DataSet
        Try
            'procura na tabela de usuarios
            mbuUsuario = New buLogin.buUsuario
            strbAux.Append("USEMAIL = '").Append(txtEmail.Text).Append("' AND USSENHA = '").Append(Encrypt(txtSenha.Text)).Append("'")
            dsDados = mbuUsuario.Query(strbAux.ToString)

            If dsDados.Tables(0).Rows().Count > 0 Then
                myProfile.UserInfo.ID = dsDados.Tables(0).Rows(0).Item("USID").ToString
                myProfile.UserInfo.Email = dsDados.Tables(0).Rows(0).Item("USEMAIL").ToString
                myProfile.UserInfo.PerfilID = dsDados.Tables(0).Rows(0).Item("PERFID").ToString
                myProfile.UserInfo.Loja = dsDados.Tables(0).Rows(0).Item("LOJAID").ToString
                Session.Item("Logado") = 1
                If dsDados.Tables(0).Rows(0).Item("USALTERASENHA") Then
                    Response.Redirect("AlteracaoSenha.aspx", False)
                Else
                    Response.Redirect("ConsultaSolicitacao.aspx", False)
                End If
            Else
                'procura na tabela de solicitações
                mbuSolicitacao = New buSolicitacao.buSolicitacao
                strbAux.Clear().Append("SOLPROTOCOLO = '").Append(txtSenha.Text.Trim).Append("' AND SOLEMAIL = '").Append(txtEmail.Text.Trim()).Append("'")
                dsDados = mbuSolicitacao.Query(strbAux.ToString)

                If dsDados.Tables(0).Rows().Count > 0 Then
                    myProfile.UserInfo.ID = txtSenha.Text.Trim 'id é o protocolo; facilita né?
                    myProfile.UserInfo.Email = txtEmail.Text.Trim
                    myProfile.UserInfo.PerfilID = enUserPerfil.Cliente

                    Session.Item("Logado") = 1
                    Response.Redirect("ConsultaSolicitacao.aspx", False)
                Else
                    'não encontrou em nenhuma
                    PopupMensagem.Mensagem = "Login ou protocolo/senha inválidos<br>Tente novamente"
                    PopupMensagem.ShowPopup()
                End If
            End If

        Catch ex As Exception
            GravarLog(ex.Message, "btnAcessar_Click de Login.aspx", enLogStatus.enlsError)
        Finally
            mbuUsuario = Nothing
            dsDados = Nothing
            mbuSolicitacao = Nothing
        End Try
    End Sub

    Protected Sub btnVoltar_Click(sender As Object, e As EventArgs) Handles btnVoltar.Click
        Try
            pnlBaixo.Visible = False
            txtEmail.Attributes.Remove("style")
            pnlSenha.Visible = False
            btnProximo.Visible = True
        Catch ex As Exception
            GravarLog(ex.Message, "btnVoltar_Click de Login.aspx", enLogStatus.enlsError)
        End Try
    End Sub
End Class