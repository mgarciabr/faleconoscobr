﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AlteracaoSenha

    '''<summary>
    '''lblMensagem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMensagem As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtAntiga control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAntiga As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtNova control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNova As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtConfirmacao control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtConfirmacao As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnAlterar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAlterar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnRetornar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRetornar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''PopupMensagem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PopupMensagem As Global.FaleConosco.wucPopUpMensagem
End Class
