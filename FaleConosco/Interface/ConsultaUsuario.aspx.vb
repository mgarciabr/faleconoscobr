﻿Imports plAppLibrary.Enumerators
Imports plAppLibrary.clsCommonFunctions

Public Class ConsultaUsuario
    Inherits System.Web.UI.Page

    Private mbuLog As buServicosGerais.buLog
    Private mbuUsuario As buLogin.buUsuario
    Private mbuPerfil As buLogin.buPerfil
    Private mbuLoja As buSolicitacao.buLoja
    Private myProfile As New ProfileCommonMakro
    Private mbuAssunto As New buSolicitacao.buAssunto

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsNothing(Session("Logado")) Then
                Response.Redirect("LoginAdmin.aspx", False)
            Else
                Session("Logado") = Session("Logado")
                If Session("Logado") = 1 Then
                    If myProfile.UserInfo.PerfilID <> enUserPerfil.Master Then
                        Response.Redirect("LoginAdmin.aspx", False) 'só o master pode consultar
                    End If
                Else
                    Response.Redirect("LoginAdmin.aspx", False)
                End If
            End If
            If Not IsPostBack Then
                configurarPagina()
                preenchergvUsuario("")
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "Page_Load de ConsultaUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Private Sub preenchergvUsuario(ByVal strFiltro As String)
        Dim dsDados As DataSet
        Try
            mbuUsuario = New buLogin.buUsuario
            dsDados = mbuUsuario.QueryLoja(strFiltro, "USEMAIL")
            gvUsuario.DataSource = dsDados
            gvUsuario.DataBind()
        Catch ex As Exception
            Throw ex
        Finally
            mbuUsuario = Nothing
        End Try
    End Sub

    Private Sub configurarPagina()
        Try
            preencherPerfil()
            preencherLoja()
            CType(Page.Master.FindControl("liSol"), HtmlGenericControl).Attributes.Remove("class")
            CType(Page.Master.FindControl("liUsuario"), HtmlGenericControl).Attributes.Add("class", "selecionado")
            CType(Page.Master.FindControl("liRelatorio"), HtmlGenericControl).Attributes.Remove("class")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        Dim dsDados As DataSet
        Try
            mbuUsuario = New buLogin.buUsuario
            dsDados = mbuUsuario.QueryLoja(obterFiltro, "USEMAIL")
            gvUsuario.DataSource = dsDados
            gvUsuario.DataBind()
        Catch ex As Exception
            GravarLog(ex.Message, "btnConsultar_Click de ConsultaUsuario.aspx", enLogStatus.enlsError)
        Finally
            mbuUsuario = Nothing
        End Try
    End Sub

    Protected Function obterFiltro() As String
        Dim strbAux As New StringBuilder

        Try

            If Not txtEmail.Text.Trim.Equals("") Then
                strbAux.Append("USEMAIL LIKE '%").Append(txtEmail.Text).Append("%'")
            End If

            If Not txtData.Text.Trim.Equals("") Then
                If Not strbAux.Length = 0 Then
                    strbAux.Append(" AND USDTCADASTRO = '").Append(FormataData_HoraSQL(txtData.Text)).Append("'")
                    'strbAux.Append(" AND USDTCADASTRO = '").Append(txtData.Text).Append("'")
                Else
                    strbAux.Append("USDTCADASTRO = '").Append(FormataData_HoraSQL(txtData.Text)).Append("'")
                    'strbAux.Append("USDTCADASTRO = '").Append(txtData.Text).Append("'")
                End If
            End If

            If Not ddlPerfil.SelectedValue = -1 Then
                If Not strbAux.Length = 0 Then
                    strbAux.Append(" AND PERFID = ").Append(ddlPerfil.SelectedValue)
                Else
                    strbAux.Append("PERFID = ").Append(ddlPerfil.SelectedValue)
                End If
            End If

            If Not ddlLoja.SelectedValue = -1 Then
                If Not strbAux.Length = 0 Then
                    strbAux.Append(" AND USUARIOLOJA.LOJAID = ").Append(ddlLoja.SelectedValue)
                Else
                    strbAux.Append("USUARIOLOJA.LOJAID = ").Append(ddlLoja.SelectedValue)
                End If
            End If

            Return strbAux.ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub gvUsuario_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvUsuario.RowCommand
        Dim dsDados As DataSet
        Dim strbAux As New StringBuilder
        Dim index As Integer
        Try
            index = Convert.ToInt32(e.CommandArgument)
            If e.CommandName = "editar" Then
                strbAux.Append("EdicaoUsuario.aspx?usid=").Append(Encrypt(gvUsuario.DataKeys(index).Value))
                Response.Redirect(strbAux.ToString, False)
            ElseIf e.CommandName = "excluir" Then
                Session("ID") = gvUsuario.DataKeys(index).Value
                PopupPergunta.ShowPopup()
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "gvBancos_RowCommand de ConsultaUsuario.aspx", enLogStatus.enlsError)
        Finally
            dsDados = Nothing
        End Try
    End Sub

    Protected Sub gvUsuario_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvUsuario.RowDataBound
        Dim dsDados As DataSet
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'substituindo descrição do perfil
                mbuPerfil = New buLogin.buPerfil
                dsDados = mbuPerfil.Query()
                For Each drRow As DataRow In dsDados.Tables(0).Rows()
                    If e.Row.Cells(1).Text.Equals(drRow.Item("PERFID").ToString) Then
                        e.Row.Cells(1).Text = drRow.Item("PERFDESCR")
                    End If
                Next

                '***comentado em 27/10 a pedidos de retirar o conceito de Região do sistema
                'substituindo descrição da região
                'Select Case e.Row.Cells(2).Text
                '    Case enLojaRegiao.Centro
                '        e.Row.Cells(2).Text = "Centro"
                '    Case enLojaRegiao.Nordeste
                '        e.Row.Cells(2).Text = "Nordeste"
                '    Case enLojaRegiao.Norte
                '        e.Row.Cells(2).Text = "Norte"
                '    Case enLojaRegiao.RJES
                '        e.Row.Cells(2).Text = "Rio de Janeiro/Espírito Santo"
                '    Case enLojaRegiao.SaoPaulo
                '        e.Row.Cells(2).Text = "São Paulo"
                '    Case enLojaRegiao.SaoPauloInter
                '        e.Row.Cells(2).Text = "São Paulo-Interior"
                '    Case enLojaRegiao.SaoPauloRM
                '        e.Row.Cells(2).Text = "São Paulo Metropolitana"
                '    Case enLojaRegiao.Sul
                '        e.Row.Cells(2).Text = "Sul"
                '    Case enLojaRegiao.Brasil
                '        e.Row.Cells(2).Text = "Brasil"
                'End Select

                '***comentado pela mudança de associação entre usuario e loja
                'substituindo nome da loja
                'mbuLoja = New buSolicitacao.buLoja
                'dsDados = mbuLoja.Query()
                'For Each drRow As DataRow In dsDados.Tables(0).Rows()
                '    If e.Row.Cells(2).Text.Equals(drRow.Item("LOJAID").ToString) Then
                '        e.Row.Cells(2).Text = drRow.Item("LOJADESCR")
                '        Exit For
                '    End If
                'Next

                '***comentado pela mudança de associação entre usuario e assunto
                'substituindo descrição do assunto
                'mbuAssunto = New buSolicitacao.buAssunto
                'dsDados = mbuAssunto.Query()
                'For Each drRow As DataRow In dsDados.Tables(0).Rows()
                '    If e.Row.Cells(2).Text.Equals(drRow.Item("ASSUNTOID").ToString) Then
                '        e.Row.Cells(2).Text = drRow.Item("ASSUNTODESCR")
                '    End If
                'Next
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "gvUsuario_RowDataBound de ConsultaUsuario.aspx", enLogStatus.enlsError)
        Finally
            mbuPerfil = Nothing
            mbuLoja = Nothing
        End Try
    End Sub

    Protected Sub gvUsuario_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvUsuario.PageIndexChanging
        Try
            gvUsuario.PageIndex = e.NewPageIndex
        Catch ex As Exception
            GravarLog(ex.Message, "gvUsuario_PageIndexChanging de ConsultaUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub gvUsuario_PageIndexChanged(sender As Object, e As EventArgs) Handles gvUsuario.PageIndexChanged
        Try
            preenchergvUsuario(obterFiltro())
        Catch ex As Exception
            GravarLog(ex.Message, "gvUsuario_PageIndexChanged de ConsultaUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub preencherPerfil()
        Dim dsDados As DataSet
        Try
            mbuPerfil = New buLogin.buPerfil
            dsDados = mbuPerfil.Query()
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    ddlPerfil.Items.Clear()
                    ddlPerfil.Items.Add(New ListItem("Todos", -1))
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        ddlPerfil.Items.Add(New ListItem(drRow("PERFDESCR"), drRow("PERFID")))
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuPerfil = Nothing
        End Try
    End Sub

    Protected Sub preencherLoja()
        Dim dsDados As DataSet
        Try
            mbuLoja = New buSolicitacao.buLoja
            dsDados = mbuLoja.Query()
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    ddlLoja.Items.Clear()
                    ddlLoja.Items.Add(New ListItem("Todas", -1))
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        ddlLoja.Items.Add(New ListItem(drRow("LOJADESCR"), drRow("LOJAID")))
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuPerfil = Nothing
        End Try
    End Sub

    Protected Sub PopupPergunta_Excluir() Handles PopupPergunta.Excluir
        Try
            mbuUsuario = New buLogin.buUsuario
            If Not IsNothing(Session("ID")) Then
                If Session("ID") > 0 Then

                    If mbuUsuario.UpdateStatus(Session("ID"), enStatusUsuario.enExcluido) Then
                        Session("ID") = Nothing
                        PopupMensagem.Mensagem = "Registro excluído com sucesso."
                        PopupMensagem.ShowPopup()
                    Else
                        PopupMensagem.Mensagem = "Ocorreu um erro inesperado e não foi possível excluir o registro."
                        PopupMensagem.ShowPopup()
                    End If
                Else
                    PopupMensagem.Mensagem = "Ocorreu um erro inesperado e não foi possível excluir o registro."
                    PopupMensagem.ShowPopup()
                End If
                preenchergvUsuario(obterFiltro())
            Else
                PopupMensagem.Mensagem = "Ocorreu um erro inesperado e não foi possível excluir o registro."
                PopupMensagem.ShowPopup()

            End If
        Catch ex As Exception
            GravarLog(ex.Message, "PopupPergunta_Excluir de CadastroLojas.aspx", enLogStatus.enlsError)

        End Try
    End Sub
End Class