﻿Imports plAppLibrary.clsCommonFunctions
Imports plAppLibrary.Enumerators

Public Class CadastroSolicitacaoMobile
    Inherits System.Web.UI.Page

    Private mbuUsuario As buLogin.buUsuario
    Private mbuLoja As buSolicitacao.buLoja
    Private mbuSolicitacao As buSolicitacao.buSolicitacao
    Private mbuSolMsg As buSolicitacao.buSolicitacaoMensagem
    Private mbuLog As buServicosGerais.buLog
    Private mbuAssunto As buSolicitacao.buAssunto
    Private mbuSub As buSolicitacao.buSubassunto
    Private mbuAssuntoSub As buSolicitacao.buAssuntoSubassunto
    Private mbuSubGrupo As buSolicitacao.buSubassuntoGrupo
    Private myProfile As New ProfileCommonMakro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMensagem.Text = ""

            '***comentado porque nao vai mais estar logado (ou vai deslogar) quando entrar nessa tela - inclusao do login***
            'If Not IsNothing(Session("Logado")) Then
            '    Session("Logado") = Session("Logado")
            '    If (myProfile.UserInfo.PerfilID <> enUserPerfil.Cliente) Then
            '        Response.Redirect("Login.aspx", False)
            '    End If
            'End If

            If Not IsPostBack Then
                preencherLoja()
                preencherAssunto()
                Session("Logado") = Nothing
                myProfile.UserInfo.LimparPerfil()
                configurarPagina()
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "Page_Load de CadastroSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Private Sub configurarPagina()
        Dim dsDados As DataSet
        Dim strbAux As New StringBuilder
        Try
            CType(Page.Master.FindControl("liSol"), HtmlGenericControl).Attributes.Add("class", "selecionado")
            CType(Page.Master.FindControl("liUsuario"), HtmlGenericControl).Visible = False
            CType(Page.Master.FindControl("liRelatorio"), HtmlGenericControl).Visible = False
            CType(Page.Master.FindControl("lnkSair"), LinkButton).Visible = False

            '***comentado porque nao vai estar logado (ou vai deslogar) quando entrar nessa tela - inclusao do login***

            'If IsNothing(Session("Logado")) Then
            '    CType(Page.Master.FindControl("lnkSair"), LinkButton).Visible = False
            'Else
            '    'preencher campos
            '    If myProfile.UserInfo.PerfilID = enUserPerfil.Cliente Then
            '        mbuSolicitacao = New buSolicitacao.buSolicitacao
            '        dsDados = mbuSolicitacao.Query(strbAux.Append("SOLPROTOCOLO = ").Append(myProfile.UserInfo.ID).ToString)
            '        With dsDados.Tables(0).Rows(0)
            '            txtEmail.Text = .Item("SOLEMAIL").ToString
            '            txtEmpresa.Text = .Item("SOLEMPRESA").ToString
            '            txtTelefone.Text = .Item("SOLTELEFONE").ToString
            '            txtEmpresa.Text = .Item("SOLEMPRESA").ToString
            '        End With
            '    End If
            'End If

        Catch ex As Exception
            Throw ex
        Finally
            dsDados = Nothing
            mbuSolicitacao = Nothing
            mbuUsuario = Nothing
        End Try
    End Sub

    Public Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub

    Protected Sub preencherLoja()
        Dim dsDados As DataSet
        Try
            mbuLoja = New buSolicitacao.buLoja
            dsDados = mbuLoja.Query("", "2 ASC")
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    ddlLoja.Items.Clear()
                    ddlLoja.Items.Add(New ListItem("Selecione", -1))
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        If drRow("LOJADESCR") <> "Brasil" Then
                            ddlLoja.Items.Add(New ListItem(drRow("LOJADESCR"), drRow("LOJAID")))
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuLoja = Nothing
        End Try
    End Sub

    Protected Sub preencherAssunto()
        Dim dsDados As DataSet
        Try
            mbuAssunto = New buSolicitacao.buAssunto
            dsDados = mbuAssunto.Query(New StringBuilder().Append("ASSUNTOSTATUS = ").Append(enStatusAssunto.Ativo).ToString)
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    ddlAssunto.Items.Clear()
                    ddlAssunto.Items.Add(New ListItem("Selecione", -1))
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        ddlAssunto.Items.Add(New ListItem(drRow("ASSUNTODESCR"), drRow("ASSUNTOID")))
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuAssunto = Nothing
        End Try
    End Sub

    Protected Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        Dim strProtocolo As String = ""
        Dim intSolid As Integer
        Dim dsDados As DataSet
        Dim strbMsg, strbAux As New StringBuilder()
        Dim strAux As String

        Try
            If txtPassaporte.Text <> "" Then
                strAux = Validar(txtPassaporte.Text)
                If strAux <> "true" Then
                    lblMensagem.Text = strAux
                    Exit Sub
                Else
                    'O passaporte digitado é valido
                End If
            End If

            If txtEmail.Text.Trim().Equals("") Or txtMensagem.Text.Trim().Equals("") Or txtTelefone.Text.Trim().Equals("") Or ddlAssunto.SelectedValue.Equals("-1") Or ddlSubassunto.SelectedValue.Equals("-1") Or ddlLoja.SelectedValue.Equals("-1") Then
                lblMensagem.Text = "Preencha todos os campos obrigatórios"
            Else
                mbuSolicitacao = New buSolicitacao.buSolicitacao
                mbuSolicitacao.Insert(strProtocolo, ddlAssunto.SelectedValue, ddlLoja.SelectedValue, txtEmail.Text, System.DateTime.Now, enStatusSolicitacao.Aberta, _
                                      ddlSubassunto.SelectedValue, , , txtTelefone.Text, txtPassaporte.Text.Trim(), , , , txtEmpresa.Text, txtNome.Text)

                dsDados = mbuSolicitacao.QueryTop1(, "SOLID DESC")
                intSolid = dsDados.Tables(0).Rows(0).Item("SOLID")
                mbuSolMsg = New buSolicitacao.buSolicitacaoMensagem
                mbuSolMsg.Insert(intSolid, txtMensagem.Text, System.DateTime.Now)

                strProtocolo = gerarProtocolo()
                mbuSolicitacao.UpdateProtocolo(intSolid, strProtocolo)

                strbMsg.Append("Agradecemos o seu contato!<br><br>O número de protocolo da mensagem enviada é:<br><br><span style='font-weight: bold; color: red; font-size: 16px;'>")
                strbMsg.Append(strProtocolo)
                strbMsg.Append("</span><br><br>A sua mensagem foi encaminhada aos responsáveis<br>que em breve irão respondê-lo.")
                PopupMensagem.Mensagem = strbMsg.ToString()
                PopupMensagem.ShowPopup()

                'envia e-mail para as lojas
                'enviarEmailMakro()

                'envia e-mail para o cliente
                Try
                    strbMsg.Append("<br><br><b>Dados da solicitação</b><br><br>")
                    strbMsg.Append("<b>Nome:</b> ").Append(txtNome.Text).Append("<br>")
                    strbMsg.Append("<b>E-mail:</b> ").Append(txtEmail.Text).Append("<br>")
                    If Not txtEmpresa.Text.Equals("") Then
                        strbMsg.Append("<b>Empresa:</b> ").Append(txtEmpresa.Text).Append("<br>")
                    End If
                    strbMsg.Append("<b>Telefone:</b> ").Append(txtTelefone.Text).Append("<br>")
                    If txtPassaporte.Text.Equals("") Then
                        strbMsg.Append("<b>Passaporte Makro: -</b><br>")
                    Else
                        strbMsg.Append("<b>Passaporte Makro:</b> ").Append(txtPassaporte.Text).Append("<br>")
                    End If
                    strbMsg.Append("<b>Loja:</b> ").Append(ddlLoja.SelectedItem.Text).Append("<br>")
                    strbMsg.Append("<b>Assunto:</b> ").Append(ddlAssunto.SelectedItem.Text).Append("<br>")
                    strbMsg.Append("<b>Subassunto:</b> ").Append(ddlSubassunto.SelectedItem.Text).Append("<br>")
                    strbMsg.Append("<b>Mensagem:</b> ").Append(txtMensagem.Text).Append("<br><br><br>")

                    strbMsg.Append("Atenciosamente,<br/>")
                    strbMsg.Append("Equipe Makro")

                    'Inserir metodo que faz o envio para todos os responsaveis pelo chamado.

                    plAppLibrary.clsCommonFunctions.EnviarEmail(strbMsg.ToString, txtEmail.Text, "Cliente Makro", "Nova solicitação")
                    enviarEmailMakro()
                Catch ex As Exception
                    GravarLog(ex.Message, "btnEnviar_Click de CadastroSolicitacao.aspx", enLogStatus.enlsError)
                End Try
            End If

        Catch ex As Exception
            GravarLog(ex.Message, "btnEnviar_Click de CadastroSolicitacao.aspx", enLogStatus.enlsError)
        Finally
            mbuSolicitacao = Nothing
        End Try
    End Sub

    Protected Function gerarProtocolo() As String
        Dim dsDados As DataSet
        Dim strbAux As New StringBuilder
        Try
            mbuSolicitacao = New buSolicitacao.buSolicitacao
            dsDados = mbuSolicitacao.QueryTop1(, "SOLID DESC")
            strbAux.Append(System.DateTime.Now.Year.ToString.Substring(2, 2)).Append(dsDados.Tables(0).Rows(0).Item("SOLID") + 1)
            Return strbAux.ToString
        Catch ex As Exception
            Throw ex
        Finally
            dsDados = Nothing
        End Try
    End Function

    Protected Sub enviarEmailMakro()
        Dim dsDados As DataSet
        Dim strbMsg, strbAux As New StringBuilder()
        Try
            'tentar enviar e-mail para todos dos grupos responsáveis makro
            'conteúdo do e-mail
            strbMsg.Clear().Append("Nova solicitação do tipo ").Append(ddlAssunto.SelectedItem.Text).Append(" - ").Append(ddlSubassunto.SelectedItem.Text).Append(".<br/>")
            strbMsg.Append("Cadastrada em ").Append(System.DateTime.Now).Append(".<br/><br/>")
            'Adiciona link para visualizar o chamado para o gerente
            strbMsg.Append(strbMsg.ToString)
            strbMsg.Append("Clique no <a href='").Append(System.Configuration.ConfigurationSettings.AppSettings("Site").ToString).Append("/interface/loginadmin.aspx'").Append(">link</a> para acessar o chamado. ").Append("<br/><br/><br/>")

            strbMsg.Append("Atenciosaente,<br/>")
            strbMsg.Append("Equipe Makro")

            'obtém e-mails
            mbuSubGrupo = New buSolicitacao.buSubassuntoGrupo
            'Alterado por Eduardo Junque em 21/11/2014
            'A nova tabela é ASSUNTOSUBASSUNTO
            'dsDados = mbuSubGrupo.Query(strbAux.Append(" SBAID = ").Append(ddlSubassunto.SelectedValue).ToString)
            dsDados = mbuSubGrupo.QueryEMAILRespChamado(strbAux.Append(" ASSUBID = ").Append(ddlSubassunto.SelectedValue).ToString)

            'strbAux.Clear().Append("GRUPOID = ").Append(dsDados.Tables(0).Rows(0).Item("GRUPOID"))
            'For i As Integer = 1 To dsDados.Tables(0).Rows().Count - 1
            '    strbAux.Append(" OR GRUPOID = ").Append(dsDados.Tables(0).Rows(i).Item("GRUPOID"))
            'Next
            'mbuUsuario = New buLogin.buUsuario
            'dsDados = mbuUsuario.Query(strbAux.ToString)

            'manda e-mails
            For Each drRow As DataRow In dsDados.Tables(0).Rows()
                'não interrompe o loop se der exceção pra algum e-mail
                Try
                    plAppLibrary.clsCommonFunctions.EnviarEmail(strbMsg.ToString, drRow.Item("USEMAIL"), drRow.Item("USEMAIL"), "Nova solicitação")
                Catch ex As Exception

                End Try
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub PopupMensagem_OK() Handles PopupMensagem.OK
        Try
            Response.Redirect("CadastroSolicitacao.aspx")
        Catch ex As Exception
            GravarLog(ex.Message, "PopupMensagem_OK", enLogStatus.enlsFailed)
        End Try
    End Sub

    Protected Sub ddlAssunto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssunto.SelectedIndexChanged
        Dim strbAux As New StringBuilder
        Dim dsDados, dsAux As DataSet
        Try
            If Not ddlAssunto.SelectedValue.Equals("-1") Then
                'preencher ddlSubassunto
                mbuAssuntoSub = New buSolicitacao.buAssuntoSubassunto
                dsDados = mbuAssuntoSub.Query(strbAux.Append("ASSUNTOID = ").Append(ddlAssunto.SelectedValue).ToString)

                If Not IsNothing(dsDados) Then
                    If dsDados.Tables(0).Rows().Count() > 0 Then
                        mbuSub = New buSolicitacao.buSubassunto
                        ddlSubassunto.Items.Clear()
                        ddlSubassunto.Items.Add(New ListItem("Selecione", -1))

                        'obtém a descrição do subassunto relacionado a cada id de subassunto
                        For Each drRow As DataRow In dsDados.Tables(0).Rows()
                            dsAux = mbuSub.Query(strbAux.Clear().Append("SBAID = ").Append(drRow("SBAID")).ToString)
                            ddlSubassunto.Items.Add(New ListItem(dsAux.Tables(0).Rows(0).Item("SBADESCR"), drRow.Item("ASSUBID")))
                        Next

                    End If
                End If
            Else
                ddlSubassunto.Items.Clear()
                ddlSubassunto.Items.Add(New ListItem("Selecione", -1))
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "ddlAssunto_SelectedIndexChanged de CadastroSolicitacao.aspx", enLogStatus.enlsError)
        Finally
            dsDados = Nothing
        End Try
    End Sub

    Protected Sub btnProximo_Click(sender As Object, e As EventArgs) Handles btnProximo.Click
        Dim strbAux As New StringBuilder
        Dim dsDados As DataSet
        Try
            'verificar se é e-mail
            If validaEmail(txtEmailLogin.Text) Then
                pnlSenha.Visible = True
                pnlBaixo.Visible = True
                txtEmailLogin.Attributes.Add("style", "width: 100%;")
                btnProximo.Visible = False
                txtSenha.Focus()

                btnEsqueci.Visible = False
            Else
                PopupMensagem.Mensagem = "Digite um e-mail válido"
                PopupMensagem.ShowPopup()
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "btnProximo_Click de CadastroSolicitacao.aspx", enLogStatus.enlsError)
        Finally
            mbuSolicitacao = Nothing
            dsDados = Nothing
        End Try
    End Sub

    Function validaEmail(ByVal email As String) As Boolean
        Dim emailRegex As New System.Text.RegularExpressions.Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        Dim emailMatch As System.Text.RegularExpressions.Match = emailRegex.Match(email)
        Return emailMatch.Success
    End Function

    Protected Sub btnAcessar_Click(sender As Object, e As EventArgs) Handles btnAcessar.Click
        Dim strbAux As New StringBuilder
        Dim dsDados As DataSet
        Try
            'procura na tabela de solicitações
            mbuSolicitacao = New buSolicitacao.buSolicitacao
            strbAux.Clear().Append("SOLPROTOCOLO = '").Append(txtSenha.Text.Trim).Append("' AND SOLEMAIL = '").Append(txtEmailLogin.Text.Trim()).Append("'")
            dsDados = mbuSolicitacao.Query(strbAux.ToString)

            If dsDados.Tables(0).Rows().Count > 0 Then
                myProfile.UserInfo.ID = txtSenha.Text.Trim 'id é o protocolo; facilita né?
                myProfile.UserInfo.Email = txtEmailLogin.Text.Trim
                myProfile.UserInfo.PerfilID = enUserPerfil.Cliente

                Session.Item("Logado") = 1
                Response.Redirect("ConsultaSolicitacao.aspx", False)
            Else
                'não encontrou em nenhuma
                PopupMensagem.Mensagem = "Login ou protocolo inválidos<br>Tente novamente"
                PopupMensagem.ShowPopup()
            End If

        Catch ex As Exception
            GravarLog(ex.Message, "btnAcessar_Click de CadastroSolicitacao.aspx", enLogStatus.enlsError)
        Finally
            mbuUsuario = Nothing
            dsDados = Nothing
            mbuSolicitacao = Nothing
        End Try
    End Sub

    Protected Sub btnVoltar_Click(sender As Object, e As EventArgs) Handles btnVoltar.Click
        Try
            pnlBaixo.Visible = False
            txtEmailLogin.Attributes.Remove("style")
            pnlSenha.Visible = False
            btnProximo.Visible = True
        Catch ex As Exception
            GravarLog(ex.Message, "btnVoltar_Click de CadastroSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub


    Public Function Validar(ByVal strPassaporte As String) As String
        Dim aux As String = String.Empty
        Dim passaporte As String = String.Empty
        Dim loja As String = String.Empty
        Dim passaportemakro As String = String.Empty
        'Dim dtDados As DataTable
        'Dim list As SPList
        Try


            ' Retira todos os caracteres que não sejam numéricos
            For i As Integer = 0 To strPassaporte.Length - 1
                If Char.IsNumber(strPassaporte(i)) Then
                    aux += strPassaporte(i).ToString()
                End If
            Next

            strPassaporte = aux
            strPassaporte = strPassaporte.PadLeft(13, "0"c).ToString()

            If strPassaporte.Substring(0, 1) = "8"c OrElse strPassaporte.Substring(0, 1) = "9"c Then
                aux = strPassaporte.Substring(4, 7)
            Else
                aux = strPassaporte.Substring(0, 7)
            End If

            'Verifica Código do cliente divisível por 13

            If (Convert.ToInt64(aux) Mod 13) <> 0 Then
                Return "O PASSAPORTE MAKRO digitado não é válido." & vbLf & "Verifique se o número do passaporte Makro foi digitado corretamente."
            End If

            passaporte = aux

            If strPassaporte.Substring(0, 1) = "8"c OrElse strPassaporte.Substring(0, 1) = "9"c Then
                aux = Convert.ToString(Convert.ToInt64(strPassaporte.Substring(1, 3)))
            Else
                aux = Convert.ToString(Convert.ToInt64(strPassaporte.Substring(7, 2)))
            End If

            loja = aux

            'Verifica Loja Makro existe

            ''Using oSiteCollection = New SPSite(SPContext.Current.Web.Url.ToString)
            ''    Using oSiteWeb = oSiteCollection.OpenWeb()
            ''        list = oSiteWeb.Lists("LojaNova")
            ''        dtDados = ConvertToDataTable(list)
            ''    End Using
            ''End Using

            Dim dsLojaAux As DataSet

            mbuLoja = New buSolicitacao.buLoja
            dsLojaAux = New DataSet

            dsLojaAux = mbuLoja.Query(New StringBuilder("LOJAID = ").Append(loja).ToString)


            ' Dim result() As DataRow = dtDados.Select(New Text.StringBuilder("Cidade = '").Append(cboCidade.SelectedValue).Append("'").Append(strFiltroServico).ToString, "Nome").Distinct().ToArray


            '   objPassaporteLojaSQL = New PassaporteLojaSQL(aux)

            '   If Not objPassaporteLojaSQL.ConsultaLojaMakro() Then
            'Return "O PASSAPORTE MAKRO digitado não é válido." & vbLf & "Verifique se o número do passaporte Makro foi digitado corretamente."
            '   End If

            'Verifica Tipo Estabelecimento está entre 001 e 200

            '	  if( codigo.Substring( 0, 1 ) == '8' || codigo.Substring( 0, 1 ) == '9' )
            '        aux = 1;
            '	  else
            '        aux = codigo.Substring(9, 3);
            '
            ' 
            '      if (Convert.ToInt64(aux) < 1 || Convert.ToInt64(aux) > 200)
            '            return "O PASSAPORTE MAKRO digitado não é válido.\nVerifique se o número do passaporte Makro foi digitado corretamente."; 

            If dsLojaAux.Tables(0).Rows.Count > 0 Then


                'Verifica último dígito está entre 0 e 3
                If Not (strPassaporte.Substring(0, 1) = "8"c OrElse strPassaporte.Substring(0, 1) = "9"c) Then
                    aux = strPassaporte.Substring(12, 1)
                    If Convert.ToInt64(aux) < 0 OrElse Convert.ToInt64(aux) > 3 Then
                        Return "O PASSAPORTE MAKRO digitado não é válido." & vbLf & "Verifique se o número do passaporte Makro foi digitado corretamente."
                    End If
                End If

                passaportemakro = loja.PadLeft(3, "0"c).ToString() & passaporte.PadLeft(7, "0"c).ToString()
                Return "true"
            Else
                Return "O PASSAPORTE MAKRO digitado não é válido." & vbLf & "Verifique se o número do passaporte Makro foi digitado corretamente."
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class