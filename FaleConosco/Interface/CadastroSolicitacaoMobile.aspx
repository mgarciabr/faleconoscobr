﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CadastroSolicitacaoMobile.aspx.vb" Inherits="FaleConosco.CadastroSolicitacaoMobile" %>


<%@ Register Src="~/UserControl/wucPopupMensagem.ascx" TagPrefix="uc1" TagName="wucPopupMensagem" %>

<html>
<head>

    <title>Makro - Fale Conosco</title>

    <!-- Fornece compatilidade para o IE-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- css -->
    <link href="../Style/reset.css" rel="stylesheet" />
    <link href="../Style/style.css" rel="stylesheet" />
    <link href="../Style/cadastroSolicitacao.css" rel="stylesheet" />

    <!-- js -->
    <script src="../Scripts/FormataMascara.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="../Scripts/jquery.mask.js"></script>
    <script>
        $(document).ready(function () {

            // Mascara telefone
            $('#txtTelefone').mask("(00) 00009-0000", { placeholder: "(00) 0000-0000" });
            //$('#txtTelefone').mask("(99) 9999-99999");
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="wrapper">
            <div class="main">
                <div class="content cadastro-solicitacao">
                    <div class="baixo" style="width: 50%">
                        <div class="box">
                            <h2>Envie sua mensagem diretamente para o Makro</h2>
                            <h3>Os campos com * são obrigatórios</h3>
                            <div class="validacao">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                                <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                            </div>

                            <div class="email">
                                <h4>E-mail*</h4>
                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="150" TabIndex="1"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Informe um endereço de e-mail válido" Display="None" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </div>

                            <div class="nome">
                                <h4>Empresa</h4>
                                <asp:TextBox ID="txtEmpresa" runat="server" MaxLength="150" TabIndex="2"></asp:TextBox>
                            </div>
                            <div class="nome">
                                <h4>Nome</h4>
                                <asp:TextBox ID="txtNome" runat="server" MaxLength="150" TabIndex="2"></asp:TextBox>
                            </div>
                            <div class="nome">
                                <h4>Telefone*</h4>
                                <%--<asp:TextBox ID="txtTelefone" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}" runat="server" TabIndex="3" ToolTip="Formato: (00)0000-0000"></asp:TextBox>--%>
                                <asp:TextBox ID="txtTelefone" runat="server" TabIndex="3" ToolTip="Formato: (00)0000-0000"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator ID="revTelefone" runat="server" ErrorMessage="Informe um número de telefone válido" Display="None" ControlToValidate="txtTelefone" ToolTip="Modelo: (XX) XXXX-XXXX" ValidationExpression="^(\([0-9]{2})\)\s([0-9]{5}|[0-9]{4})-[0-9]{4}$"></asp:RegularExpressionValidator>--%>
                            </div>
                            <div style="clear: both"></div>
                            <div class="nome">
                                <h4>Passaporte Makro</h4>
                                <asp:TextBox ID="txtPassaporte" runat="server" MaxLength="13" TabIndex="4"></asp:TextBox>
                            </div>

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="assunto">
                                        <h4>Assunto*</h4>
                                        <asp:DropDownList ID="ddlAssunto" runat="server" TabIndex="6" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                     <div style="clear: both"></div>
                                    <div class="loja">
                                        <h4>Subassunto*</h4>
                                        <asp:DropDownList ID="ddlSubassunto" runat="server" TabIndex="7">
                                            <asp:ListItem Value="-1">Selecione</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>


                            <div class="email">
                                <h4>Loja*</h4>
                                <asp:DropDownList ID="ddlLoja" runat="server" TabIndex="8">
                                </asp:DropDownList>
                            </div>

                            <div class="mensagem">
                                <h4>Mensagem*</h4>
                                <asp:TextBox ID="txtMensagem" runat="server" TextMode="MultiLine" Rows="5" CssClass="txtMensagem" MaxLength="8000" TabIndex="9" Style="height: 100px;"></asp:TextBox>
                            </div>

                            <div class="ir-voltar">
                                <asp:LinkButton ID="btnEnviar" runat="server" class="botao btnEsq">Enviar</asp:LinkButton>
                                <asp:LinkButton ID="btnVoltar" runat="server" class="botao btnDir" PostBackUrl="~/Interface/ConsultaSolicitacao.aspx">Retornar</asp:LinkButton>
                            </div>

                            <div class="acompanhe">
                                <h3 style="overflow: hidden; clear: both; padding-top: 30px; margin-bottom: 10px;">Acompanhe sua solicitação</h3>
                                <asp:Panel ID="pnlEmail" runat="server" CssClass="emailLogin" DefaultButton="btnProximo">
                                    <br />
                                    <p>E-mail</p>
                                    <br />
                                    <asp:TextBox ID="txtEmailLogin" runat="server" ToolTip="E-mail"></asp:TextBox>
                                    <asp:LinkButton ID="btnProximo" class="botao btnProximo" runat="server"></asp:LinkButton>
                                </asp:Panel>
                                <asp:Panel ID="pnlAcessar" runat="server" DefaultButton="btnAcessar">
                                    <div class="senha" runat="server" id="pnlSenha" visible="false">
                                        <br />
                                        <p>Protocolo</p>
                                        <br />
                                        <asp:TextBox ID="txtSenha" runat="server" TextMode="Password" ToolTip="Protocolo"></asp:TextBox>
                                        <asp:LinkButton ID="btnEsqueci" class="frase" runat="server" PostBackUrl="~/Interface/EsquecimentoSenha.aspx">Esqueci a senha</asp:LinkButton>
                                    </div>
                                    <div class="baixoLogin" runat="server" id="pnlBaixo" visible="false" style="width: 100%; margin: 10px 0 25px; padding: 0;">
                                        <asp:LinkButton ID="btnAcessar" class="botao btnAcessar" runat="server">Acessar</asp:LinkButton>
                                         
                                        <asp:LinkButton ID="LinkButton1" class="botao btnVoltar" runat="server">Voltar</asp:LinkButton>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <uc1:wucPopupMensagem runat="server" ID="PopupMensagem" />
                </div>
                <!-- content -->
            </div>
            <!-- content -->
        </div>
        <!-- content -->
    </form>
</body>
</html>