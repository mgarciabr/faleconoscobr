﻿Imports plAppLibrary.clsCommonFunctions
Imports plAppLibrary.Enumerators

Public Class EsquecimentoSenha
    Inherits System.Web.UI.Page

    Private mbuUsuario As buLogin.buUsuario
    Private mbuLog As buServicosGerais.buLog
    Private myProfile As New ProfileCommonMakro

    Public Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub

    Protected Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        Dim dsDados As DataSet
        Dim strbAux, strEmail As New StringBuilder
        Dim strNovaSenha As String = GerarSenha(6)
        Try
            mbuUsuario = New buLogin.buUsuario
            dsDados = mbuUsuario.Query(strbAux.Append("USEMAIL='").Append(txtEmail.Text.Trim).Append("'").ToString)
            If dsDados.Tables(0).Rows().Count > 0 Then
                mbuUsuario.UpdateSenha(dsDados.Tables(0).Rows(0).Item("USID"), Encrypt(strNovaSenha), True)
                strEmail.Append("Olá,<br/>Sua senha provisória do Fale Conosco é:<br/><strong>")
                strEmail.Append(strNovaSenha).Append("</strong><br/><br/>Ao logar, você será redirecionado(a) para alterá-la.<br/><br/>")
                strEmail.Append("Atenciosamente,<br/>Equipe Makro")

                'Tentar enviar email
                Try
                    plAppLibrary.clsCommonFunctions.EnviarEmail(strEmail.ToString, txtEmail.Text.Trim, txtEmail.Text.Trim, "Alteração de senha")
                    strEmail.Clear().Append("Senha temporária enviada para o e-mail: ").Append(txtEmail.Text.Trim)
                    PopupMensagem.Mensagem = strEmail.ToString()
                    PopupMensagem.ShowPopup()
                Catch ex As Exception
                    'Não pode travar a aplicação se ocorrer algum erro ao enviar email
                    PopupMensagem.Mensagem = "Ocorreu um problema no envio do seu novo token.\nPor favor, tente novamente."
                    PopupMensagem.ShowPopup()
                End Try
            Else
                PopupMensagem.Mensagem = "E-mail não cadastrado"
                PopupMensagem.ShowPopup()
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "btnEnviar_Click de EsquecimentoSenha.aspx", enLogStatus.enlsError)
        Finally
            mbuUsuario = Nothing
        End Try
    End Sub

    Protected Sub PopupMensagem_OK() Handles PopupMensagem.OK
        Try
            Response.Redirect("LoginAdmin.aspx")
        Catch ex As Exception
            GravarLog(ex.Message, "PopupMensagem_OK", enLogStatus.enlsFailed)
        End Try
    End Sub

    Private Sub EsquecimentoSenha_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                CType(Page.Master.FindControl("liSol"), HtmlGenericControl).Attributes.Remove("class")
                CType(Page.Master.FindControl("liUsuario"), HtmlGenericControl).Attributes.Add("class", "selecionado")
                CType(Page.Master.FindControl("liRelatorio"), HtmlGenericControl).Attributes.Remove("class")
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "EsquecimentoSenha_Load de EsquecimentoSenha.aspx", enLogStatus.enlsError)
        End Try
    End Sub
End Class