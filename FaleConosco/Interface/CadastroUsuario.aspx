﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CadastroUsuario.aspx.vb" Inherits="FaleConosco.CadastroUsuario" MasterPageFile="../FaleConosco.Master" %>

<%@ Register Src="~/UserControl/wucPopupMensagem.ascx" TagPrefix="uc1" TagName="wucPopupMensagem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">

    <!-- css -->
    <link href="../Style/cadastroUser.css" rel="stylesheet" />

    <!-- js -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph2" runat="Server">
    <div class="baixo">
        <div class="box">
            <h2>Cadastro de Usuário</h2>
            <h3>Todos os campos são obrigatórios</h3>
            <div class="validacao">
                <asp:Label ID="lblMensagem" runat="server" Text="" ForeColor="Red"></asp:Label>
            </div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="email">
                        <h4>E-mail</h4>
                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="150"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Informe um e-mail válido" Visible="True" ControlToValidate="txtEmail" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>

                    <div class="perfil-user">
                        <h4>Perfil</h4>
                        <asp:DropDownList ID="ddlPerfil" runat="server" AutoPostBack="True"></asp:DropDownList>
                    </div>

                    <%--            
                    ***comentado em 27/10 a pedidos de retirar o conceito de Região do sistema

                    <div class="regiao">
                        <h4>Região</h4>
                        <asp:DropDownList ID="ddlRegiao" runat="server" AutoPostBack="True">
                            <asp:ListItem Value="-1">Selecione</asp:ListItem>
                            <asp:ListItem Value="5">Centro</asp:ListItem>
                            <asp:ListItem Value="6">Nordeste</asp:ListItem>
                            <asp:ListItem Value="7">Norte</asp:ListItem>
                            <asp:ListItem Value="4">Rio de Janeiro/Espírito Santo</asp:ListItem>
                            <asp:ListItem Value="1">São Paulo</asp:ListItem>
                            <asp:ListItem Value="2">São Paulo-Interior</asp:ListItem>
                            <asp:ListItem Value="3">São Paulo Metropolitana</asp:ListItem>
                            <asp:ListItem Value="8">Sul</asp:ListItem>
                            <asp:ListItem Value="9">Brasil</asp:ListItem>
                        </asp:DropDownList>
                    </div>--%>

                    <fieldset class="check-list" runat="server" id="fsAssunto">
                        <h4>Assunto</h4>
                        <div class="assunto" id="dvAssunto" runat="server">
                            <div class="assunto-lista">                                
                                <asp:CheckBox ID="cbxTodosAssuntos" runat="server" Text="Selecionar todos" ToolTip="Clique para selecionar/limpar todos os assuntos listados" AutoPostBack="True" />
                                <asp:Panel ID="Panel1" runat="server">
                                    <asp:CheckBoxList ID="cblAssunto" runat="server" RepeatColumns="3" AutoPostBack="True"></asp:CheckBoxList>
                                </asp:Panel>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="check-list" runat="server" id="fsSubAssunto">
                        <h4>Subassunto</h4>
                        <div class="subassunto-lista" id="dvSubassunto" runat="server">                            
                            <asp:CheckBox ID="cbxTodosSubassuntos" runat="server" Text="Selecionar todos" ToolTip="Clique para selecionar/limpar todas os subassuntos listados" AutoPostBack="True" />
                            <asp:Panel ID="Panel3" runat="server">
                                <asp:CheckBoxList ID="cblSubassunto" runat="server" RepeatColumns="3"></asp:CheckBoxList>
                            </asp:Panel>
                        </div>
                    </fieldset>

                    <fieldset class="check-list" runat="server" id="fsLoja">
                        <h4>Loja</h4>
                        <div class="linha-2" id="dvLoja" runat="server">
                            <div class="loja-lista">                                 
                                <asp:CheckBox ID="cbxTodasLojas" runat="server" Text="Selecionar todas" ToolTip="Clique para selecionar/limpar todas as lojas listadas" AutoPostBack="True" />
                                <asp:Panel ID="Panel2" runat="server">
                                    <asp:CheckBoxList ID="cblLojas" runat="server" RepeatColumns="3"></asp:CheckBoxList>
                                </asp:Panel>
                            </div>
                        </div>
                    </fieldset>
                </ContentTemplate>
            </asp:UpdatePanel>

            <%--<div class="email">
                <h4>E-mail</h4>
                <asp:TextBox ID="txtEmail" runat="server" MaxLength="150"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Informe um e-mail válido" Visible="True" ControlToValidate="txtEmail" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </div>

            <div class="perfil-user">
                <h4>Perfil</h4>
                <asp:DropDownList ID="ddlPerfil" runat="server"></asp:DropDownList>
            </div>

            <div class="grupo">
                <h4>Loja</h4>
                <asp:DropDownList ID="ddlLoja" runat="server"></asp:DropDownList>
            </div>

            <div class="grupo">
                <h4>Assunto</h4>
                <asp:DropDownList ID="ddlAssunto" runat="server"></asp:DropDownList>
            </div> --%>

            <div class="ir-voltar">
                <asp:LinkButton ID="btnIncluir" runat="server" class="botao btnEsq" Style="width: 100px; margin: 0 15px 0 0;">Incluir</asp:LinkButton>
                <asp:LinkButton ID="btnRetornar" runat="server" class="botao btnDir" Style="width: 110px; margin: 0;" PostBackUrl="~/Interface/ConsultaUsuario.aspx" CausesValidation="False">Retornar</asp:LinkButton>
            </div>
        </div>
    </div>
    <uc1:wucPopupMensagem runat="server" ID="PopupMensagem" />

    <script>
        $(document).ready(function () {
            var tamanhoJanela = $(window).width() - 169;
            $('.content').css('width', tamanhoJanela);
        });
    </script>
</asp:Content>
