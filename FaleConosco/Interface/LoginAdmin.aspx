﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LoginAdmin.aspx.vb" Inherits="FaleConosco.LoginAdmin" %>

<%@ Register Src="~/UserControl/wucPopupMensagem.ascx" TagPrefix="uc1" TagName="wucPopupMensagem" %>

<html>
<head>

    <title>Makro - Fale Conosco</title>

    <!-- Fornece compatilidade para o IE-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- css -->
    <link href="../Style/reset.css" rel="stylesheet" />
    <link href="../Style/style.css" rel="stylesheet" />
    <link href="../Style/login.css" rel="stylesheet" />

    <!-- js -->
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var alturaJanela = $(window).height();
            $('#form1').css('height', alturaJanela);

            // Aplicar altura no form
            $(window).mousemove(function () {
                $('#form1').css('height', alturaJanela);
            });

        });
    </script>

</head>
<body>
    <form runat="server" id="form1" class="form-body">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="wrapper">
            <div class="main">
                <div class="content login">
                    <center>
                    <div class="logo">
                        <img src="../Images/_login/logo.png" alt="" style="width: 232px"/>
                    </div>
                        </center>
                    <div class="box box-login">
                        <h1>Fale Conosco</h1>
                        <asp:Panel ID="pnlEmail" runat="server" CssClass="email" DefaultButton="btnProximo">
                            <br />
                            <p>E-mail</p>
                            <br />
                            <asp:TextBox ID="txtEmail" runat="server" ToolTip="E-mail"></asp:TextBox>
                            <asp:LinkButton ID="btnProximo" class="botao btnProximo" runat="server"></asp:LinkButton>
                        </asp:Panel>
                        <asp:Panel ID="pnlAcessar" runat="server" DefaultButton="btnAcessar">
                            <div class="senha" runat="server" id="pnlSenha" visible="false">
                                <br />
                                <p>Senha</p>
                                <br />
                                <asp:TextBox ID="txtSenha" runat="server" TextMode="Password" ToolTip="Senha"></asp:TextBox>
                                <asp:LinkButton ID="btnEsqueci" class="frase" runat="server">Esqueci a senha</asp:LinkButton>
                            </div>
                            <div class="baixo" runat="server" id="pnlBaixo" visible="false" style="width: 100%; margin: 25px 0; padding: 0;">
                                <asp:LinkButton ID="btnAcessar" class="botao btnAcessar" runat="server">Acessar</asp:LinkButton>
                                <asp:LinkButton ID="btnVoltar" class="botao btnVoltar" runat="server">Voltar</asp:LinkButton>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <!-- div content -->
            </div>
            <!-- div main -->
        </div>
        <!-- div wrapper -->
        <uc1:wucPopupMensagem runat="server" ID="PopupMensagem" />
    </form>
</body>
</html>
