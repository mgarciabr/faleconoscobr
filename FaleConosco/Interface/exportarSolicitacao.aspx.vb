﻿Imports plAppLibrary.Enumerators
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports System.IO

Public Class exportarSolicitacao
    Inherits System.Web.UI.Page

    Private mbuLog As buServicosGerais.buLog
    Private mbuLoja As buSolicitacao.buLoja
    Private myProfile As New ProfileCommonMakro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim attachment As New StringBuilder
        Dim dsAux As Data.DataSet
        Try
            dsAux = Session("dsDados")
            gvSolicitacao.DataSource = dsAux
            gvSolicitacao.DataBind()

            attachment.Append("attachment; filename=Solicitacoes").Append(Year(Date.Now)).Append(Month(Date.Now)).Append(Day(Date.Now)).Append(Hour(Date.Now)).Append(Minute(Date.Now)).Append(Second(Date.Now)).Append(".xls")

            HttpContext.Current.Response.Charset = "utf-8"
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250")

            Response.ClearContent()
            Response.AddHeader("content-disposition", attachment.ToString)
            Response.ContentType = "application/ms-excel"

            Dim sw As New System.IO.StringWriter()
            Dim htw As New HtmlTextWriter(sw)

            gvSolicitacao.RenderControl(htw)
            Response.Write(sw.ToString())
            Response.End()
        Catch ex As Exception
            GravarLog(ex.Message, "Page_Load de exportarSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        Return
    End Sub


    Protected Sub gvSolicitacao_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSolicitacao.RowDataBound
        Dim dsDados As DataSet
        Dim strbAux As New StringBuilder()
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'substituindo loja
                mbuLoja = New buSolicitacao.buLoja
                dsDados = mbuLoja.Query()
                For Each drRow As DataRow In dsDados.Tables(0).Rows()
                    If e.Row.Cells(6).Text.Equals(drRow.Item("LOJAID").ToString) Then
                        e.Row.Cells(6).Text = drRow.Item("LOJADESCR")
                        Exit For
                    End If
                Next

                ''substituindo descrição da região
                'Select Case e.Row.Cells(2).Text
                '    Case enLojaRegiao.Centro
                '        e.Row.Cells(2).Text = "Centro"
                '    Case enLojaRegiao.Nordeste
                '        e.Row.Cells(2).Text = "Nordeste"
                '    Case enLojaRegiao.Norte
                '        e.Row.Cells(2).Text = "Norte"
                '    Case enLojaRegiao.RJES
                '        e.Row.Cells(2).Text = "Rio de Janeiro/Espírito Santo"
                '    Case enLojaRegiao.SaoPaulo
                '        e.Row.Cells(2).Text = "São Paulo"
                '    Case enLojaRegiao.SaoPauloInter
                '        e.Row.Cells(2).Text = "São Paulo-Interior"
                '    Case enLojaRegiao.SaoPauloRM
                '        e.Row.Cells(2).Text = "São Paulo Metropolitana"
                '    Case enLojaRegiao.Sul
                '        e.Row.Cells(2).Text = "Sul"
                '    Case enLojaRegiao.Brasil
                '        e.Row.Cells(2).Text = "Brasil"
                'End Select
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "gvSolicitacao_RowDataBound de Relatorio.aspx", enLogStatus.enlsError)
        Finally
            mbuLoja = Nothing
        End Try
    End Sub

    Private Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub
End Class