﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ConsultaUsuario.aspx.vb" Inherits="FaleConosco.ConsultaUsuario" MasterPageFile="../FaleConosco.Master" %>

<%@ Register Src="~/UserControl/wucPopupMensagem.ascx" TagPrefix="uc1" TagName="wucPopupMensagem" %>
<%@ Register Src="~/UserControl/wucPopUpPergunta.ascx" TagPrefix="uc2" TagName="wucPopupPergunta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">

    <!-- css -->
    <link href="../Style/cadastroUser.css" rel="stylesheet" />

    <!-- js -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="../Scripts/FormataMascara.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph2" runat="Server">
    <div class="baixo">
        <div class="box">
            <h2>Consulta de Usuário</h2>
            <div class="validacao">
                <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
            </div>

            <div class="ir-voltar">
                <asp:LinkButton ID="btnNovo" runat="server" class="botao btnCadastrar" PostBackUrl="~/Interface/CadastroUsuario.aspx" Style="margin: -20px 0 30px; width: 160px; display: table;">Cadastrar Novo</asp:LinkButton>
            </div>

            <asp:Panel ID="Panel1" runat="server" GroupingText="Filtro">
                <div class="email-consulta">
                    <h4>E-mail</h4>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="150" TabIndex="1"></asp:TextBox>
                </div>

                <div class="perfil-consulta">
                    <h4>Perfil</h4>
                    <asp:DropDownList ID="ddlPerfil" runat="server" TabIndex="2"></asp:DropDownList>
                </div>

                <div class="email-consulta">
                    <h4>Última Alteração</h4>
                    <asp:TextBox ID="txtData" runat="server" MaxLength="10" TabIndex="3" onkeydown="formataData(this, event)"></asp:TextBox>
                </div>

                <div class="regiao-consulta">
                    <h4>Loja</h4>
                    <asp:DropDownList ID="ddlLoja" runat="server">
                    </asp:DropDownList>
                </div>

                <div class="ir-voltar" style="margin-top: -20px;">
                    <asp:LinkButton ID="btnConsultar" runat="server" class="botao btnConsultar center" Style="margin: 30px 0 0; width: 120px; float: right;">Consultar</asp:LinkButton>
                </div>
            </asp:Panel>



            <br />
            <h4>Usuários cadastrados:</h4>
<%--            <asp:UpdatePanel ID="updPanel" runat="server">
                <ContentTemplate>--%>
                    <asp:GridView ID="gvUsuario" runat="server" AllowPaging="True"
                        AutoGenerateColumns="False" EmptyDataText="Nenhum usuário cadastrado no momento" DataKeyNames="USID" CellPadding="0" class="grid">
                        <PagerStyle CssClass="pagination" HorizontalAlign="Center"></PagerStyle>
                        <AlternatingRowStyle BackColor="#EEEEEE" />
                        <Columns>
                            <asp:BoundField DataField="USEMAIL" HeaderText="E-mail" />
                            <asp:BoundField DataField="PERFID" HeaderText="Perfil" />
                            <asp:BoundField DataField="USSENHA" HeaderText="Senha" />
                            <%--<asp:BoundField DataField="ASSUNTOID" HeaderText="Assunto" />--%>
                            <%--<asp:BoundField DataField="LOJAID" HeaderText="Loja" />--%>
                            <%--<asp:BoundField DataField="LOJADESCR" HeaderText="Loja" />--%>
                            <asp:BoundField DataField="USDTCADASTRO" HeaderText="Última Alteração" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="113px" >
                            <ItemStyle Width="113px" />
                            </asp:BoundField>
                            <asp:ButtonField CommandName="editar" ImageUrl="../Images/_comum/Edit.png" ItemStyle-CssClass="comando" ButtonType="Image">
                                <HeaderStyle Width="60px" />
                                <ItemStyle CssClass="comando" />
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="excluir" ImageUrl="../Images/_comum/Delete.png" ItemStyle-CssClass="comando" ButtonType="Image">
                                <HeaderStyle Width="50px" />
                                <ItemStyle CssClass="comando" />
                            </asp:ButtonField>
                        </Columns>
                        <HeaderStyle CssClass="header" />
                    </asp:GridView>
<%--                </ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
    </div>
    <uc1:wucPopupMensagem runat="server" ID="PopupMensagem" />
    <uc2:wucPopupPergunta runat="server" ID="PopupPergunta" />
    <script>
        $(document).ready(function () {
            var tamanhoJanela = $(window).width() - 169;
            $('.content').css('width', tamanhoJanela);
        });
    </script>
</asp:Content>
