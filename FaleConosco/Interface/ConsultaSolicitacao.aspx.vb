﻿Imports plAppLibrary.Enumerators
Imports plAppLibrary.clsCommonFunctions

Public Class ConsultaSolicitacao
    Inherits System.Web.UI.Page

    Private mbuLog As buServicosGerais.buLog
    Private mbuSolicitacao As buSolicitacao.buSolicitacao
    Private mbuUsuario As buLogin.buUsuario
    Private mbuSolicitacaoMsg As buSolicitacao.buSolicitacaoMensagem
    Private mbuAssunto As buSolicitacao.buAssunto
    Private mbuSub As buSolicitacao.buSubassunto
    Private mbuLoja As buSolicitacao.buLoja
    Private mbuSubGrupo As buSolicitacao.buSubassuntoGrupo
    Private myProfile As New ProfileCommonMakro
    Private mbuUsLoja As New buLogin.buUsuarioLoja
    Private mbuUsAssunto As New buLogin.buUsuarioAssunto
    Private mbuAssuntoSub As buSolicitacao.buAssuntoSubassunto


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsNothing(Session("Logado")) Then
                Response.Redirect("CadastroSolicitacao.aspx", False)
            Else
                Session("Logado") = Session("Logado")
                If Session("Logado") <> 1 Then
                    Response.Redirect("CadastroSolicitacao.aspx", False)
                End If
                If myProfile.UserInfo.PerfilID = enUserPerfil.Cliente Then
                    Session("idSelecionado") = obterSolid(myProfile.UserInfo.ID)
                End If
            End If

            If Not IsPostBack Then
                ConfigurarPagina()
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "Page_Load de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Function obterSolid(ByVal intSolProtocolo As String) As Integer
        Dim dsDados As DataSet
        Try
            mbuSolicitacao = New buSolicitacao.buSolicitacao
            dsDados = mbuSolicitacao.Query(New StringBuilder().Append("SOLPROTOCOLO = '").Append(intSolProtocolo).Append("'").ToString)
            Return dsDados.Tables(0).Rows(0).Item("SOLID")
        Catch ex As Exception
            Throw ex
        Finally
            mbuSolicitacao = Nothing
        End Try
    End Function

    Protected Function obterProtocolo(ByVal intSolId As Integer) As String
        Dim dsDados As DataSet
        Try
            mbuSolicitacao = New buSolicitacao.buSolicitacao
            dsDados = mbuSolicitacao.Query(New StringBuilder().Append("SOLID = ").Append(intSolId).ToString)
            Return dsDados.Tables(0).Rows(0).Item("SOLPROTOCOLO")
        Catch ex As Exception
            Throw ex
        Finally
            mbuSolicitacao = Nothing
        End Try
    End Function

    Private Sub ConfigurarPagina()
        Dim dsDados As DataSet
        Dim lnkSair As LinkButton
        Try
            pnlNovaMsg.Visible = False
            btnEnviar.Visible = False
            btnVoltar.Visible = False
            btnEncerrar.Visible = False
            pnlCaptura.Visible = False
            CType(Page.Master.FindControl("liSol"), HtmlGenericControl).Attributes.Add("class", "selecionado")
            CType(Page.Master.FindControl("liUsuario"), HtmlGenericControl).Attributes.Remove("class")
            CType(Page.Master.FindControl("liRelatorio"), HtmlGenericControl).Attributes.Remove("class")

            If myProfile.UserInfo.PerfilID = enUserPerfil.Cliente Then
                btnCadastrar.Visible = True
                txtProtocolo.Enabled = False
                txtProtocolo.Text = myProfile.UserInfo.ID
                txtPassaporte.Enabled = False
                pnlConsulta.Visible = False
                gvSolicitacao.Visible = False
                ExibirMensagens(obterSolid(myProfile.UserInfo.ID))
                btnVoltar.Visible = False
                lnkSair = Page.Master.FindControl("lnkSair")
                lnkSair.PostBackUrl = "~/Interface/CadastroSolicitacao.aspx"
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dsDados = Nothing
            mbuSolicitacao = Nothing
        End Try
    End Sub

    Private Sub CarregarGrid()
        Dim dsDados, dsAux As New DataSet
        Dim strbAux, strFim As New StringBuilder
        Dim dtCadastro As DateTime
        Dim tsSL As TimeSpan
        Dim filtros As String = ""
        Try
            mbuSolicitacao = New buSolicitacao.buSolicitacao

            'FILTROS
            If (txtProtocolo.Text <> String.Empty) Then
                filtros &= " AND SOLICITACAO.SOLPROTOCOLO = '" & txtProtocolo.Text & "'"
            End If

            If (txtPassaporte.Text <> String.Empty) Then
                filtros &= " AND SOLICITACAO.SOLNUMEROPASSAPORTE = '" & txtPassaporte.Text & "'"
            End If

            If (txtInicio.Text <> String.Empty) Then
                Dim dataDe As DateTime = DateTime.MinValue
                If (DateTime.TryParse(txtInicio.Text, dataDe)) Then
                    filtros &= " AND SOLICITACAO.SOLDTCADASTRO >= '" & dataDe.ToString("yyyy-MM-dd HH:mm") & "'"
                End If
            End If

            If (txtFim.Text <> String.Empty) Then
                Dim dataAte As DateTime = DateTime.MinValue
                If (DateTime.TryParse(txtInicio.Text, dataAte)) Then
                    filtros &= " AND SOLICITACAO.SOLDTCADASTRO <= '" & dataAte.ToString("yyyy-MM-dd HH:mm") & "'"
                End If
            End If

            If (ddlSituacao.SelectedValue <> String.Empty) Then
                filtros &= " AND SOLICITACAO.SOLSTATUS = '" & ddlSituacao.SelectedValue & "'"
            End If

            'PERFIL
            If myProfile.UserInfo.PerfilID = enUserPerfil.Cliente Then
                dsAux = mbuSolicitacao.ObterSolicitacoesCliente(myProfile.UserInfo.ID, filtros)
            ElseIf myProfile.UserInfo.PerfilID = enUserPerfil.Gerente Then
                dsAux = mbuSolicitacao.ObterSolicitacoesInternas(myProfile.UserInfo.ID, filtros)
            ElseIf myProfile.UserInfo.PerfilID = enUserPerfil.Master Then
                dsAux = mbuSolicitacao.ObterSolicitacoesMaster(filtros)
            End If

            'STATUS
            dsAux.Tables(0).Columns.Add("STATUSDESCR", Type.GetType("System.String"))
            For Each drRow As DataRow In dsAux.Tables(0).Rows()
                'se hoje - item("dtcadastro") > item("sbasl")
                dtCadastro = drRow.Item("SOLDTCADASTRO")
                tsSL = New TimeSpan(drRow.Item("SBASL"), 0, 0, 0)

                Select Case drRow.Item("SOLSTATUS")
                    Case enStatusSolicitacao.Capturada
                        drRow.Item("STATUSDESCR") = "Respondida"
                    Case enStatusSolicitacao.Encerrada
                        drRow.Item("STATUSDESCR") = "Encerrada"
                    Case enStatusSolicitacao.Aberta
                        drRow.Item("STATUSDESCR") = "Em aberto"
                End Select

                If Not drRow.Item("STATUSDESCR").ToString.Equals("Encerrada") Then
                    strbAux.Clear().Append(drRow.Item("STATUSDESCR"))

                    If DateTime.Today.Subtract(dtCadastro) > tsSL Then
                        drRow.Item("STATUSDESCR") = strbAux.Append(" (fora de prazo)").ToString
                    Else
                        drRow.Item("STATUSDESCR") = strbAux.Append(" (no prazo)").ToString
                    End If
                End If

            Next

            litContador.Text = " (" & dsAux.Tables(0).Rows.Count() & ")"
            gvSolicitacao.DataSource = dsAux
            gvSolicitacao.DataBind()
            pnlConversa.Visible = False
            dvAuxMsg.Visible = False
            btnVoltar.Visible = False
            btnEncerrar.Visible = False
        Catch ex As Exception
            GravarLog(ex.Message, "btnConsultar_Click de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        Finally
            mbuSolicitacao = Nothing
        End Try
    End Sub

    Protected Sub ExibirMensagens(ByVal intSolId As Integer)
        Dim strbAux As New StringBuilder
        Dim dsDados As DataSet
        Try
            'fazer consulta e fazer código abaixo num for pra cada linha
            mbuSolicitacaoMsg = New buSolicitacao.buSolicitacaoMensagem
            dsDados = mbuSolicitacaoMsg.QueryUsuario(New StringBuilder().Append("SOLICITACAOMENSAGEM.SOLID = ").Append(intSolId).ToString)

            If dsDados.Tables(0).Rows.Count > 0 Then
                strbAux.Append("<div class='mensagem' style='height: 20px;'><span>Histórico de Mensagens</span>")
                strbAux.Append("<p style='font-weight: bold'>Protocolo ").Append(dsDados.Tables(0).Rows(0).Item("SOLPROTOCOLO").ToString).Append("</p></div>")
                For Each drRow As DataRow In dsDados.Tables(0).Rows
                    strbAux.Append("<div class='mensagem'>")

                    strbAux.Append("<div class='dir'>")
                    If drRow.Item("USID").ToString.Equals("0") Then 'quer dizer que não foi o gerente que mandou
                        strbAux.Append("<span class='remetente'>").Append(drRow.Item("SOLEMAIL").ToString).Append("</span>")
                    Else 'gerente
                        strbAux.Append("<span class='remetente'>").Append(drRow.Item("USEMAIL").ToString).Append("</span>")
                    End If
                    strbAux.Append("<span class='data' style='margin-bottom: 10px;'>").Append(drRow.Item("SOLDTCADASTRO").ToString).Append("</span>")

                    'Informações extras
                    strbAux.Append("<div>")
                    strbAux.Append("<span class='maisInformacoes'><b>Empresa:</b> ").Append(IIf(String.IsNullOrEmpty(drRow.Item("SOLEMPRESA").ToString), "-", drRow.Item("SOLEMPRESA").ToString)).Append("</span>")
                    strbAux.Append("<span class='maisInformacoes'><b>Telefone:</b> ").Append(IIf(String.IsNullOrEmpty(drRow.Item("SOLTELEFONE").ToString), "-", drRow.Item("SOLTELEFONE").ToString)).Append("</span>")
                    strbAux.Append("<span class='maisInformacoes'><b>Passaporte Makro:</b> ").Append(IIf(String.IsNullOrEmpty(drRow.Item("SOLNUMEROPASSAPORTE").ToString), "-", drRow.Item("SOLNUMEROPASSAPORTE").ToString)).Append("</span>")
                    strbAux.Append("<span class='maisInformacoes'><b>Loja:</b> ").Append(IIf(String.IsNullOrEmpty(drRow.Item("LOJADESCR").ToString), "-", drRow.Item("LOJADESCR").ToString)).Append("</span>")
                    strbAux.Append("<span class='maisInformacoes'><b>Assunto:</b> ").Append(IIf(String.IsNullOrEmpty(drRow.Item("ASSUNTODESCR").ToString), "-", drRow.Item("ASSUNTODESCR").ToString)).Append("</span>")
                    strbAux.Append("<span class='maisInformacoes'><b>Subassunto:</b> ").Append(IIf(String.IsNullOrEmpty(drRow.Item("SBADESCR").ToString), "-", drRow.Item("SBADESCR").ToString)).Append("</span>")
                    strbAux.Append("</div>")

                    strbAux.Append("</div>")

                    strbAux.Append("<textarea class='textoMsg' disabled='disabled' style='height: 100px; resize:none;'>").Append(drRow.Item("SOLMSGDESCR")).Append("</textarea>")

                    strbAux.Append("</div>")
                Next
            End If
            pnlConversa.InnerHtml = strbAux.ToString
            pnlConversa.Visible = True
            btnVoltar.Visible = True
            'pnlCaptura.Visible = False

            If myProfile.UserInfo.PerfilID = enUserPerfil.Cliente Then 'se for cliente
                btnEncerrar.Visible = True
                'If qtsMensagensCliente(intSolId) < 4 Then 'se ainda tiver solicitações por fazer e não tiver encerrada
                If dsDados.Tables(0).Rows(0).Item("SOLSTATUS") <> enStatusSolicitacao.Encerrada Then
                    'btnEnviar.Visible = True 'deixa enviar
                    'pnlNovaMsg.Visible = True
                    'btnEnviar.Focus()
                    btnEncerrar.Visible = False
                    btnEnviar.Visible = False
                    pnlNovaMsg.Visible = False
                    btnVoltar.Focus()
                    lblAux.Text = New StringBuilder().Append("Solicitação ainda não respondida").ToString
                    dvAuxMsg.Visible = True
                Else
                    btnEncerrar.Visible = False
                    btnEnviar.Visible = False
                    pnlNovaMsg.Visible = False
                    btnVoltar.Focus()
                    lblAux.Text = New StringBuilder().Append("Solicitacao encerrada em ").Append(dsDados.Tables(0).Rows(0).Item("SOLDTENCERRA")).ToString
                    dvAuxMsg.Visible = True
                End If
                'Else
                '    btnEnviar.Visible = False
                '    pnlNovaMsg.Visible = False
                '    lblMensagem.Text = "Você atingiu o número limite de interações por solicitação"
                'End If
            Else
                If dsDados.Tables(0).Rows(0).Item("SOLSTATUS") = enStatusSolicitacao.Encerrada Then
                    pnlNovaMsg.Visible = False
                    btnEnviar.Visible = False
                    btnVoltar.Focus()
                    lblAux.Text = New StringBuilder().Append("Solicitacao encerrada em ").Append(dsDados.Tables(0).Rows(0).Item("SOLDTENCERRA")).ToString
                    dvAuxMsg.Visible = True
                Else
                    pnlNovaMsg.Visible = True
                    btnEnviar.Visible = True
                    btnEnviar.Focus()
                    lblAux.Text = ""
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
            mbuSolicitacaoMsg = Nothing
            dsDados = Nothing
        End Try
    End Sub



    Private Function lojasRelacioandas(ByVal intUsId) As String
        Try
            Dim strbResultado, strbAux As New StringBuilder
            Dim dsDados As New DataSet
            mbuUsLoja = New buLogin.buUsuarioLoja
            strbAux.Append("USID = ").Append(intUsId)
            dsDados = mbuUsLoja.Query(strbAux.ToString)

            For Each drRow As DataRow In dsDados.Tables(0).Rows()
                strbResultado.Append("LOJA.LOJAID = ").Append(drRow("LOJAID")).Append(" OR ")
            Next

            If (strbResultado.Length >= 4) Then
                strbResultado.Remove(strbResultado.Length - 4, 4) 'tirando o último 'OR'
            End If
            Return strbResultado.ToString
        Catch ex As Exception
            Throw ex
        Finally
            mbuUsLoja = Nothing
        End Try
    End Function

    Private Function assuntosRelacionados(ByVal intUsId) As String
        Try
            Dim strbResultado, strbAux As New StringBuilder
            Dim dsDados As New DataSet
            mbuUsAssunto = New buLogin.buUsuarioAssunto
            strbAux.Append("USID = ").Append(intUsId)
            dsDados = mbuUsAssunto.Query(strbAux.ToString)

            For Each drRow As DataRow In dsDados.Tables(0).Rows()
                strbResultado.Append("ASSUNTO.ASSUNTOID = ").Append(drRow("ASSUNTOID")).Append(" OR ")
            Next

            If (strbResultado.Length >= 4) Then
                strbResultado.Remove(strbResultado.Length - 4, 4) 'tirando o último 'OR'
            End If
            Return strbResultado.ToString
        Catch ex As Exception
            Throw ex
        Finally
            mbuUsLoja = Nothing
        End Try
    End Function

    Protected Sub gvSolicitacao_PageIndexChanged(sender As Object, e As EventArgs) Handles gvSolicitacao.PageIndexChanged
        Try
            CarregarGrid()
        Catch ex As Exception
            GravarLog(ex.Message, "gvSolicitacao_PageIndexChanged de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub gvSolicitacao_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvSolicitacao.PageIndexChanging
        Try
            gvSolicitacao.PageIndex = e.NewPageIndex
        Catch ex As Exception
            GravarLog(ex.Message, "gvSolicitacao_PageIndexChanging de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub gvSolicitacao_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSolicitacao.RowDataBound
        Dim dsDados As DataSet
        Dim strbAux As New StringBuilder()
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'substituindo nome da loja
                mbuLoja = New buSolicitacao.buLoja
                dsDados = mbuLoja.Query()
                For Each drRow As DataRow In dsDados.Tables(0).Rows()
                    If e.Row.Cells(4).Text.Equals(drRow.Item("LOJAID").ToString) Then
                        e.Row.Cells(4).Text = drRow.Item("LOJADESCR")
                        Exit For
                    End If
                Next

                'substituindo assunto
                mbuAssunto = New buSolicitacao.buAssunto
                dsDados = mbuAssunto.Query()
                For Each drRow As DataRow In dsDados.Tables(0).Rows()
                    If e.Row.Cells(5).Text.Equals(drRow.Item("ASSUNTOID").ToString) Then
                        e.Row.Cells(5).Text = drRow.Item("ASSUNTODESCR")
                        Exit For
                    End If
                Next

                'substituindo subassunto
                mbuAssuntoSub = New buSolicitacao.buAssuntoSubassunto
                dsDados = mbuAssuntoSub.QuerySubassunto()
                For Each drRow As DataRow In dsDados.Tables(0).Rows()
                    If e.Row.Cells(6).Text.Equals(drRow.Item("ASSUBID").ToString) Then
                        e.Row.Cells(6).Text = drRow.Item("SBADESCR")
                        Exit For
                    End If
                Next

            End If
        Catch ex As Exception
            GravarLog(ex.Message, "gvSolicitacao_RowDataBound de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        Finally
            mbuSolicitacao = Nothing
            mbuAssunto = Nothing
            mbuLoja = Nothing
        End Try
    End Sub

    Protected Sub gvSolicitacao_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvSolicitacao.SelectedIndexChanged
        Try
            Session("idSelecionado") = gvSolicitacao.SelectedValue
            'If gvSolicitacao.SelectedRow.Cells(9).Text.Equals("Aberta") Then
            '    If myProfile.UserInfo.PerfilID = enUserPerfil.Gerente Or myProfile.UserInfo.PerfilID = enUserPerfil.Master Then
            '        'mostrar mensagens (sem poder mandar nova) antes de capturar?
            '        pnlCaptura.Visible = True
            '        btnRetornar.Focus()
            '        pnlConversa.Visible = False
            '        pnlNovaMsg.Visible = False
            '        btnEnviar.Visible = False
            '        btnVoltar.Visible = False
            '        btnEncerrar.Visible = False
            '    Else
            '        exibirMensagens(Session("idSelecionado"))
            '    End If
            'Else
            '    exibirMensagens(Session("idSelecionado"))
            'End If
            If myProfile.UserInfo.PerfilID = enUserPerfil.Gerente Or myProfile.UserInfo.PerfilID = enUserPerfil.Master Then
                pnlConversa.Visible = True
                pnlNovaMsg.Visible = True
                btnEnviar.Visible = True
                btnVoltar.Visible = True
                btnEncerrar.Visible = False
            End If
            ExibirMensagens(Session("idSelecionado"))
        Catch ex As Exception
            GravarLog(ex.Message, "gvSolicitacao_SelectedIndexChanged de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Private Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        CarregarGrid()
    End Sub

    Protected Sub btnCapturar_Click(sender As Object, e As EventArgs) Handles btnCapturar.Click
        Dim strbAux As New StringBuilder
        Try
            If Not IsNothing(Session("idSelecionado")) Then
                mbuSolicitacao = New buSolicitacao.buSolicitacao
                mbuSolicitacao.UpdateStatus(Session("idSelecionado"), enStatusSolicitacao.Capturada, myProfile.UserInfo.ID, , System.DateTime.Now)
                PopupMensagem.Mensagem = strbAux.Clear.Append("Solicitação ").Append(gvSolicitacao.SelectedRow.Cells(0).Text).Append(" capturada com sucesso").ToString
                PopupMensagem.ShowPopup()
                CarregarGrid()
                ExibirMensagens(Session("idSelecionado"))  'exibir conversa
                pnlCaptura.Visible = False
            Else
                'como ta guardando o id selecionado numa session, caso a session expire:
                PopupMensagem.Mensagem = "Tempo limite atingido <br/>Selecione uma solicitação novamente"
                PopupMensagem.ShowPopup()
                pnlConversa.Visible = False
                pnlNovaMsg.Visible = False
                btnEnviar.Visible = False
                btnVoltar.Visible = False
                btnEncerrar.Visible = False
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "btnCapturar_Click de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        Finally
            mbuSolicitacao = Nothing
        End Try
    End Sub

    Protected Sub btnRetornar_Click(sender As Object, e As EventArgs) Handles btnRetornar.Click
        Try
            pnlCaptura.Visible = False
        Catch ex As Exception
            GravarLog(ex.Message, "btnCapturar_Click de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub



    Protected Function qtsMensagensCliente(ByVal intSolId As Integer) As Integer
        Dim dsDados As DataSet
        Try
            mbuSolicitacaoMsg = New buSolicitacao.buSolicitacaoMensagem
            dsDados = mbuSolicitacaoMsg.QueryCount(New StringBuilder().Append("SOLICITACAOMENSAGEM.SOLID = ").Append(intSolId).Append(" AND USID = 0").ToString)
            Return dsDados.Tables(0).Rows(0).Item("qtdMsg")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Function qtsMensagensGerente(ByVal intSolId As Integer) As Integer
        Dim dsDados As DataSet
        Try
            mbuSolicitacaoMsg = New buSolicitacao.buSolicitacaoMensagem
            dsDados = mbuSolicitacaoMsg.QueryCount(New StringBuilder().Append("SOLICITACAOMENSAGEM.SOLID = ").Append(intSolId).Append(" AND USID <> 0").ToString)
            Return dsDados.Tables(0).Rows(0).Item("qtdMsg")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        Dim intUsId As Integer
        Dim strbMsg As New StringBuilder
        Try
            If Not IsNothing(Session("idSelecionado")) Then
                If Not txtNovaMensagem.Text.Trim.Equals("") Then

                    mbuSolicitacao = New buSolicitacao.buSolicitacao
                    mbuSolicitacaoMsg = New buSolicitacao.buSolicitacaoMensagem

                    If myProfile.UserInfo.PerfilID = enUserPerfil.Cliente Then
                        intUsId = 0 'quer dizer que é do cliente
                    Else
                        intUsId = myProfile.UserInfo.ID
                    End If

                    mbuSolicitacaoMsg.Insert(Session("idSelecionado"), txtNovaMensagem.Text, System.DateTime.Now, intUsId)

                    If intUsId <> 0 Then 'atualizar ultimo 
                        mbuSolicitacao.UpdateUltimo(Session("idSelecionado"), intUsId)
                    End If

                    'se for do gerente, altera pra respondida
                    If intUsId <> 0 Then
                        mbuSolicitacao.UpdateStatus(Session("idSelecionado"), enStatusSolicitacao.Encerrada, , , , System.DateTime.Now)
                    Else
                        'altera pra aberta
                        mbuSolicitacao.UpdateStatus(Session("idSelecionado"), enStatusSolicitacao.Aberta, , , )
                    End If

                    ExibirMensagens(Session("idSelecionado"))
                    txtNovaMensagem.Text = ""

                    ' e-mail
                    If myProfile.UserInfo.PerfilID <> enUserPerfil.Cliente Then
                        CarregarGrid()
                        'para cliente avisando nova mensagem
                        strbMsg.Clear().Append("Há uma nova mensagem em sua solicitação no Fale Conosco. <br/>")
                        strbMsg.Append("Para visualizá-la, vá a Consulta de Solicitações e procure pelo protocolo <bold>").Append(obterProtocolo(Session("idSelecionado"))).Append("</bold>.<br/><br/>")
                        strbMsg.Append("Atenciosamente,<br/>")
                        strbMsg.Append("Equipe Makro")
                        'enviarEmail(1, strbMsg.ToString, "Nova mensagem - Makro") 
                    Else
                        'para responsável makro
                        strbMsg.Clear().Append("Nova mensagem na solicitação do tipo ").Append("").Append(" - ").Append("").Append(".<br/>")
                        strbMsg.Append("Protocolo: ").Append(myProfile.UserInfo.ID).Append(".<br/><br/>")
                        strbMsg.Append("Atenciosamente,<br/>")
                        strbMsg.Append("Equipe Makro")
                        'enviarEmail(2, strbMsg.ToString, strbMsg.Clear().Append("Nova mensagem - ").Append("").ToString) 
                    End If
                End If
            Else
                'como ta guardando o id selecionado numa session, caso a session expire:
                If myProfile.UserInfo.PerfilID <> enUserPerfil.Cliente Then
                    PopupMensagem.Mensagem = "Tempo limite atingido <br/>Selecione uma solicitação novamente"
                Else
                    PopupMensagem.Mensagem = "Tempo limite atingido <br/>Faça login novamente"
                End If
                PopupMensagem.ShowPopup()
                pnlConversa.Visible = False
                pnlNovaMsg.Visible = False
                btnEnviar.Visible = False
                btnVoltar.Visible = False
                btnEncerrar.Visible = False
                dvAuxMsg.Visible = False
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "btnEnviar_Click de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        Finally
            mbuSolicitacaoMsg = Nothing
            mbuSolicitacao = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="intPara">1 = envia para cliente; 2 = envia para grupo</param>
    ''' <param name="strMsg">conteúdo do e-mail</param>
    ''' <param name="strAssunto">assunto do e-mail</param>
    ''' <remarks></remarks>
    Private Sub enviarEmail(ByVal intPara As Integer, ByVal strMsg As String, ByVal strAssunto As String)
        Dim dsDados As DataSet
        Dim strbAux As New StringBuilder()
        Try
            mbuSolicitacao = New buSolicitacao.buSolicitacao
            dsDados = mbuSolicitacao.Query(strbAux.Append("SOLID = ").Append(Session("idSelecionado")("idSelecionado")).ToString)
            If intPara = 1 Then
                plAppLibrary.clsCommonFunctions.EnviarEmail(strMsg, dsDados.Tables(0).Rows(0).Item("SOLEMAIL"), dsDados.Tables(0).Rows(0).Item("SOLEMAIL"), strAssunto)
            Else
                If dsDados.Tables(0).Rows(0).Item("SOLSTATUS") = enStatusSolicitacao.Aberta Then
                    'se estiver aberta, manda para o grupo todo

                    'obtém e-mails
                    mbuSubGrupo = New buSolicitacao.buSubassuntoGrupo
                    dsDados = mbuSubGrupo.Query(strbAux.Clear.Append(" SBAID = ").Append(dsDados.Tables(0).Rows(0).Item("SBAID")).ToString)

                    strbAux.Clear().Append("GRUPOID = ").Append(dsDados.Tables(0).Rows(0).Item("GRUPOID"))
                    For i As Integer = 1 To dsDados.Tables(0).Rows().Count - 1
                        strbAux.Append(" OR GRUPOID = ").Append(dsDados.Tables(0).Rows(i).Item("GRUPOID"))
                    Next
                    mbuUsuario = New buLogin.buUsuario
                    dsDados = mbuUsuario.Query(strbAux.ToString)

                    'manda e-mails
                    For Each drRow As DataRow In dsDados.Tables(0).Rows()
                        'não interrompe o loop se der exceção pra algum e-mail
                        Try
                            plAppLibrary.clsCommonFunctions.EnviarEmail(strMsg, drRow.Item("USEMAIL"), drRow.Item("USEMAIL"), strAssunto)
                        Catch ex As Exception

                        End Try
                    Next

                Else
                    'se não, manda só pro usuário que capturou
                    mbuUsuario = New buLogin.buUsuario
                    dsDados = mbuUsuario.Query(strbAux.Clear().Append("USID = ").Append(dsDados.Tables(0).Rows(0).Item("USIDCAPTURA")).ToString)
                    plAppLibrary.clsCommonFunctions.EnviarEmail(strMsg, dsDados.Tables(0).Rows(0).Item("USEMAIL"), dsDados.Tables(0).Rows(0).Item("USEMAIL"), strAssunto)
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
            mbuSubGrupo = Nothing
            mbuUsuario = Nothing
        End Try
    End Sub

    Protected Sub btnVoltar_Click(sender As Object, e As EventArgs) Handles btnVoltar.Click
        Try
            pnlConversa.Visible = False
            pnlNovaMsg.Visible = False
            btnEnviar.Visible = False
            btnVoltar.Visible = False
            btnEncerrar.Visible = False
            dvAuxMsg.Visible = False
        Catch ex As Exception
            GravarLog(ex.Message, "btnVoltar_Click de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub btnEncerrar_Click(sender As Object, e As EventArgs) Handles btnEncerrar.Click
        Dim strbAux As New StringBuilder
        Try
            mbuSolicitacao = New buSolicitacao.buSolicitacao
            mbuSolicitacao.UpdateStatus(obterSolid(myProfile.UserInfo.ID), enStatusSolicitacao.Encerrada, , , , System.DateTime.Now)
            PopupMensagem.Mensagem = "Solicitação encerrada com sucesso"
            PopupMensagem.ShowPopup()
            pnlConversa.Visible = True
            pnlNovaMsg.Visible = False
            btnEnviar.Visible = False
            btnVoltar.Visible = False
            btnEncerrar.Visible = False
            lblAux.Text = New StringBuilder().Append("Solicitacao encerrada em ").Append(System.DateTime.Now).ToString
            dvAuxMsg.Visible = True
        Catch ex As Exception
            GravarLog(ex.Message, "btnEncerrar_Click de ConsultaSolicitacao.aspx", enLogStatus.enlsError)
        End Try
    End Sub
End Class