﻿Imports plAppLibrary.Enumerators
Imports plAppLibrary.clsCommonFunctions

Public Class Relatorio
    Inherits System.Web.UI.Page

    Private mbuLog As buServicosGerais.buLog
    Private mbuSolicitacao As buSolicitacao.buSolicitacao
    Private mbuSolicitacaoMsg As buSolicitacao.buSolicitacaoMensagem
    Private mbuAssunto As buSolicitacao.buAssunto
    Private mbuLoja As buSolicitacao.buLoja
    Private myProfile As New ProfileCommonMakro
    Private mbuSub As buSolicitacao.buSubassunto
    Private mbuAssuntoSub As buSolicitacao.buAssuntoSubassunto
    Private mbuUsLoja As New buLogin.buUsuarioLoja
    Private mbuUsAssunto As New buLogin.buUsuarioAssunto
    Private mbuUsuario As New buLogin.buUsuario
    Private mbuUsSub As buLogin.buUsuarioSub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMensagem.Text = ""
            If IsNothing(Session("Logado")) Then
                Response.Redirect("LoginAdmin.aspx", False)
            Else
                Session("Logado") = Session("Logado")
                If Session("Logado") = 1 Then
                    If (myProfile.UserInfo.PerfilID <> enUserPerfil.Master) And (myProfile.UserInfo.PerfilID <> enUserPerfil.Gerente) Then
                        Response.Redirect("LoginAdmin.aspx", False) 'só o master e gerente podem gerar relatorio
                    End If
                Else
                    Response.Redirect("LoginAdmin.aspx", False)
                End If
            End If
            If Not IsPostBack Then
                preenchergvSolicitacao("")
                configuraPagina()
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "Page_Load de Relatorio.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Private Sub configuraPagina()
        Dim strbAux As New StringBuilder
        Try
            CType(Page.Master.FindControl("liSol"), HtmlGenericControl).Attributes.Remove("class")
            CType(Page.Master.FindControl("liUsuario"), HtmlGenericControl).Attributes.Remove("class")
            CType(Page.Master.FindControl("liRelatorio"), HtmlGenericControl).Attributes.Add("class", "selecionado")

            preencherAssunto()
            preencherSubassuntos()
            dvSubassunto.Visible = False

            If myProfile.UserInfo.PerfilID <> enUserPerfil.Master Then
                preencherLoja(lojasRelacioandas(myProfile.UserInfo.ID))
            Else
                preencherLoja(New StringBuilder().Append("LOJAREGIAO <> ").Append(enLojaRegiao.Brasil).ToString)
            End If

        Catch ex As Exception
            Throw ex
        Finally
            mbuLoja = Nothing
            mbuUsLoja = Nothing
        End Try
    End Sub

    Private Sub preenchergvSolicitacao(ByVal strFiltro As String)
        Dim dsDados, dsAux As New DataSet
        Dim strMsg As String
        Dim strbAux As New StringBuilder()
        Dim dtCadastro As DateTime
        Dim tsSL As TimeSpan
        Try
            mbuSolicitacao = New buSolicitacao.buSolicitacao

            strbAux.Append(obterFiltro())

            'filtro muda de acordo com o perfil
            If myProfile.UserInfo.PerfilID = enUserPerfil.Gerente Then 'atendente vê todas de suas lojas e subassuntos relacionados
                strbAux.Append("AND (").Append(lojasRelacioandas(myProfile.UserInfo.ID)).Append(") ")
                strbAux.Append("AND (").Append(subassuntosRelacionados(myProfile.UserInfo.ID)).Append(")")
            End If

            'eliminando as solicitações repetidas
            dsDados = mbuSolicitacao.QueryUltimo(strbAux.ToString, "SOLDTCADASTRO DESC, SOLSTATUS ASC")

            dsAux = mbuSolicitacao.QueryUltimo("SOLSTATUS = -1") 'dataset vazio, apenas com a estrutura das colunas
            For Each drRow As DataRow In dsDados.Tables(0).Rows
                strbAux.Clear.Append("SOLID = ").Append(drRow.Item("SOLID"))
                'If dsAux.Tables(0).Select(strbAux.ToString).Count = 0 Then 'se a chave não estiver no ds auxiliar, insere
                strMsg = drRow("SOLMSGDESCR").ToString

                'Alterado por Eduardo Junque em 22/02/2018
                'Motivo: No relatório devem aparecer as descrições das mensagens.
                'If strMsg.Length > 20 Then
                '    drRow("SOLMSGDESCR") = strbAux.Clear().Append(strMsg.Substring(0, 20)).Append("...")
                'End If
                dsAux.Tables(0).ImportRow(drRow)
                'End If
            Next

            dsAux.Tables(0).Columns.Add("primMensagem", Type.GetType("System.String"))

            'inserindo dados na nova coluna
            mbuSolicitacaoMsg = New buSolicitacao.buSolicitacaoMensagem
            dsDados = mbuSolicitacaoMsg.QueryPrimeiraMsg(" SOLSTATUS <> 0 AND USID <> 0 ", "SOLDTCADASTRO ASC")
            For Each drDados As DataRow In dsDados.Tables(0).Rows
                For Each drAux As DataRow In dsAux.Tables(0).Rows
                    If drAux.Item("SOLID") = drDados.Item("SOLID") Then
                        If drAux.Item("primMensagem").ToString.Equals("") Then
                            drAux.Item("primMensagem") = drDados.Item("primMensagem")
                        End If
                    End If
                Next
            Next

            'substituindo o status
            dsAux.Tables(0).Columns.Add("STATUSDESCR", Type.GetType("System.String"))
            For Each drRow As DataRow In dsAux.Tables(0).Rows()
                'se hoje - item("dtcadastro") > item("sbasl")
                dtCadastro = drRow.Item("SOLDTCADASTRO").ToString
                tsSL = New TimeSpan(drRow.Item("SBASL"), 0, 0, 0)

                Select Case drRow.Item("SOLSTATUS")
                    Case enStatusSolicitacao.Capturada
                        drRow.Item("STATUSDESCR") = "Respondida"
                    Case enStatusSolicitacao.Encerrada
                        drRow.Item("STATUSDESCR") = "Encerrada"
                    Case enStatusSolicitacao.Aberta
                        drRow.Item("STATUSDESCR") = "Em aberto"
                End Select

                If Not drRow.Item("STATUSDESCR").ToString.Equals("Encerrada") Then
                    strbAux.Clear().Append(drRow.Item("STATUSDESCR"))

                    If DateTime.Today.Subtract(dtCadastro) > tsSL Then
                        drRow.Item("STATUSDESCR") = strbAux.Append(" (fora de prazo)").ToString
                    Else
                        drRow.Item("STATUSDESCR") = strbAux.Append(" (no prazo)").ToString
                    End If
                End If

            Next

            gvSolicitacao.DataSource = dsAux
            Session("dsDados") = dsAux
            gvSolicitacao.DataBind()
        Catch ex As Exception
            Throw ex
        Finally
            mbuSolicitacao = Nothing
            mbuSolicitacaoMsg = Nothing
        End Try
    End Sub

    Protected Function obterFiltro() As String
        Dim strbAux, strFim, strIni As New StringBuilder
        Dim strDados() As String
        Dim selecionada As Boolean = False
        Try

            If Not txtInicio.Text.Trim.Equals("") And IsDate(txtInicio.Text.Trim) Then
                'Incluido hora no txtInicio para considerar hora no filtro
                strDados = txtInicio.Text.Split("/")
                strIni = strIni.Append(strDados(2)).Append("-").Append(strDados(1)).Append("-").Append(strDados(0)).Append(" 00:00:00")
                strbAux.Append("SOLDTCADASTRO >= '").Append(strIni.ToString()).Append("'")
                'strbAux.Append("SOLDTCADASTRO >= '").Append(txtInicio.Text).Append("'")
            End If

            If Not txtFim.Text.Trim.Equals("") And IsDate(txtFim.Text.Trim) Then
                strDados = txtFim.Text.Split("/")
                'strFim.Append(CInt(strDados(0)) + 1).Append("/").Append(strDados(1)).Append("/").Append(strDados(2))
                strFim = strFim.Append(strDados(2)).Append("-").Append(strDados(1)).Append("-").Append(strDados(0)).Append(" 23:59:59")
                If Not strbAux.Length = 0 Then
                    strbAux.Append(" AND SOLDTCADASTRO <= '").Append(strFim.ToString()).Append("'")
                    'strbAux.Append(" AND SOLDTCADASTRO <= '").Append(strFim.ToString).Append("'")
                Else
                    strbAux.Append("SOLDTCADASTRO <= '").Append(strFim.ToString()).Append("'")
                    'strbAux.Append("SOLDTCADASTRO <= '").Append(strFim.ToString).Append("'")
                End If
            End If

            If Not strbAux.Length = 0 Then
                strbAux.Append(" AND SOLSTATUS <> ").Append(enStatusSolicitacao.Excluida)
            Else
                strbAux.Append("SOLSTATUS <> ").Append(enStatusSolicitacao.Excluida)
            End If

            'vê apenas os subassuntos que selecionou
            If subassuntoSelecionado() Then
                selecionada = False
                For Each item As ListItem In cblSubassunto.Items
                    If item.Selected Then
                        selecionada = True
                        If strbAux.ToString.Contains("SOLICITACAO.ASSUBID") Then
                            strbAux.Append(" OR ")
                        Else
                            strbAux.Append(" AND (")
                        End If
                        strbAux.Append("SOLICITACAO.ASSUBID = ").Append(item.Value)
                    End If
                Next
                If selecionada Then
                    strbAux.Append(")") 'fechando o AND de subassunto
                End If
            Else
                If myProfile.UserInfo.PerfilID <> enUserPerfil.Master Then 'master nao tem assuntos ou subassuntos
                    strbAux.Append(" AND (").Append(subassuntosRelacionados(myProfile.UserInfo.ID)).Append(")")
                End If
            End If

            'vê apenas os assuntos que selecionou
            If assuntoSelecionado() Then
                selecionada = False
                For Each item As ListItem In cblAssunto.Items
                    If item.Selected Then
                        selecionada = True
                        If strbAux.ToString.Contains("ASSUNTO.ASSUNTOID") Then
                            strbAux.Append(" OR ")
                        Else
                            strbAux.Append(" AND (")
                        End If
                        strbAux.Append(" ASSUNTO.ASSUNTOID = ").Append(item.Value)
                    End If
                Next
                If selecionada Then
                    strbAux.Append(")") 'fechando o AND de assunto
                End If
            Else
                If myProfile.UserInfo.PerfilID <> enUserPerfil.Master Then 'master nao tem assuntos
                    strbAux.Append(" AND (").Append(assuntosRelacionados(myProfile.UserInfo.ID)).Append(")")
                End If
            End If

            'vê apenas as lojas selecionadas
            If lojaSelecionada() Then
                selecionada = False
                For Each item As ListItem In cblLojas.Items
                    If item.Selected Then
                        selecionada = True
                        If strbAux.ToString.Contains("LOJA.LOJAID") Then
                            strbAux.Append(" OR ")
                        Else
                            strbAux.Append(" AND (")
                        End If
                        strbAux.Append("LOJA.LOJAID = ").Append(item.Value)
                    End If
                Next
                If selecionada Then
                    strbAux.Append(")") 'fechando o AND de loja
                End If
            Else
                If myProfile.UserInfo.PerfilID <> enUserPerfil.Master Then 'master nao tem lojas
                    strbAux.Append(" AND (").Append(lojasRelacioandas(myProfile.UserInfo.ID)).Append(")")
                End If
            End If

            Return strbAux.ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub gvSolicitacao_PageIndexChanged(sender As Object, e As EventArgs) Handles gvSolicitacao.PageIndexChanged
        Try
            preenchergvSolicitacao(obterFiltro())
        Catch ex As Exception
            GravarLog(ex.Message, "gvSolicitacao_PageIndexChanged de Relatorio.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub gvSolicitacao_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvSolicitacao.PageIndexChanging
        Try
            gvSolicitacao.PageIndex = e.NewPageIndex
        Catch ex As Exception
            GravarLog(ex.Message, "gvSolicitacao_PageIndexChanging de Relatorio.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub gvSolicitacao_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSolicitacao.RowDataBound
        Dim dsDados As DataSet
        Dim strbAux As New StringBuilder()
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'substituindo o nome da loja
                mbuLoja = New buSolicitacao.buLoja
                dsDados = mbuLoja.Query()
                For Each drRow As DataRow In dsDados.Tables(0).Rows()
                    If e.Row.Cells(6).Text.Equals(drRow.Item("LOJAID").ToString) Then
                        e.Row.Cells(6).Text = drRow.Item("LOJADESCR")
                        Exit For
                    End If
                Next

                'substituindo descrição da região
                'Select Case e.Row.Cells(6).Text
                '    Case enLojaRegiao.Centro
                '        e.Row.Cells(6).Text = "Centro"
                '    Case enLojaRegiao.Nordeste
                '        e.Row.Cells(6).Text = "Nordeste"
                '    Case enLojaRegiao.Norte
                '        e.Row.Cells(6).Text = "Norte"
                '    Case enLojaRegiao.RJES
                '        e.Row.Cells(6).Text = "Rio de Janeiro/Espírito Santo"
                '    Case enLojaRegiao.SaoPaulo
                '        e.Row.Cells(6).Text = "São Paulo"
                '    Case enLojaRegiao.SaoPauloInter
                '        e.Row.Cells(6).Text = "São Paulo-Interior"
                '    Case enLojaRegiao.SaoPauloRM
                '        e.Row.Cells(6).Text = "São Paulo Metropolitana"
                '    Case enLojaRegiao.Sul
                '        e.Row.Cells(6).Text = "Sul"
                '    Case enLojaRegiao.Brasil
                '        e.Row.Cells(6).Text = "Brasil"
                'End Select

            End If
        Catch ex As Exception
            GravarLog(ex.Message, "gvSolicitacao_RowDataBound de Relatorio.aspx", enLogStatus.enlsError)
        Finally
            mbuLoja = Nothing
        End Try
    End Sub

    Private Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        Dim strbAux, strFim As New StringBuilder
        Dim dsDados, dsAux As DataSet
        Dim dtCadastro As DateTime
        Dim tsSL As TimeSpan
        Dim selecionada As Boolean = False
        Try
            mbuSolicitacao = New buSolicitacao.buSolicitacao

            strbAux.Append(obterFiltro())

            mbuSolicitacao = New buSolicitacao.buSolicitacao
            dsDados = mbuSolicitacao.QueryUltimo(strbAux.ToString, "SOLDTCADASTRO DESC, SOLSTATUS ASC")

            'eliminando as solicitações repetidas
            'Alterado por André Calza em 31/07/2018
            'Motivo: As linhas repetidas não devem ser removidas do relatório
            dsAux = mbuSolicitacao.QueryUltimo("SOLSTATUS = -1")
            For Each drRow As DataRow In dsDados.Tables(0).Rows
                'strbAux.Clear.Append("SOLID = ").Append(drRow.Item("SOLID"))
                'If dsAux.Tables(0).Select(strbAux.ToString).Count = 0 Then
                dsAux.Tables(0).ImportRow(drRow)
                ' End If
            Next

            dsAux.Tables(0).Columns.Add("primMensagem", Type.GetType("System.String"))

            'inserindo dados na nova coluna
            mbuSolicitacaoMsg = New buSolicitacao.buSolicitacaoMensagem
            dsDados = mbuSolicitacaoMsg.QueryPrimeiraMsg(" SOLSTATUS <> 0 AND USID <> 0 ", "SOLDTCADASTRO ASC")
            For Each drDados As DataRow In dsDados.Tables(0).Rows
                For Each drAux As DataRow In dsAux.Tables(0).Rows
                    If drAux.Item("SOLID") = drDados.Item("SOLID") Then
                        If drAux.Item("primMensagem").ToString.Equals("") Then
                            drAux.Item("primMensagem") = drDados.Item("primMensagem")
                        End If
                    End If
                Next
            Next

            'substituindo o status
            dsAux.Tables(0).Columns.Add("STATUSDESCR", Type.GetType("System.String"))
            For Each drRow As DataRow In dsAux.Tables(0).Rows()
                'se hoje - item("dtcadastro") > item("sbasl")
                dtCadastro = drRow.Item("SOLDTCADASTRO")
                tsSL = New TimeSpan(drRow.Item("SBASL"), 0, 0, 0)

                Select Case drRow.Item("SOLSTATUS")
                    Case enStatusSolicitacao.Capturada
                        drRow.Item("STATUSDESCR") = "Respondida"
                    Case enStatusSolicitacao.Encerrada
                        drRow.Item("STATUSDESCR") = "Encerrada"
                    Case enStatusSolicitacao.Aberta
                        drRow.Item("STATUSDESCR") = "Em aberto"
                End Select

                If Not drRow.Item("STATUSDESCR").ToString.Equals("Encerrada") Then
                    strbAux.Clear().Append(drRow.Item("STATUSDESCR"))

                    If DateTime.Today.Subtract(dtCadastro) > tsSL Then
                        drRow.Item("STATUSDESCR") = strbAux.Append(" (fora de prazo)").ToString
                    Else
                        drRow.Item("STATUSDESCR") = strbAux.Append(" (no prazo)").ToString
                    End If
                End If

            Next

            gvSolicitacao.DataSource = dsAux
            Session("dsDados") = dsAux
            gvSolicitacao.DataBind()

            'vê apenas as lojas que selecionou ou brasil todo
            'If Not ddlRegiao.SelectedValue = enLojaRegiao.Brasil Then 'se for brasil, não tem filtro de loja
            '    For Each item As ListItem In cblLojas.Items
            '        If item.Selected Then
            '            selecionada = True
            '            If strbAux.ToString.Contains("LOJA.LOJAID") Then
            '                strbAux.Append(" OR ")
            '            Else
            '                strbAux.Append(" AND (")
            '            End If
            '            strbAux.Append("LOJA.LOJAID = ").Append(item.Value)
            '        End If
            '    Next
            '    If selecionada Then
            '        strbAux.Append(")") 'fechando o AND de loja
            '    End If

            'End If
        Catch ex As Exception
            GravarLog(ex.Message, "btnConsultar_Click de Relatorio.aspx", enLogStatus.enlsError)
        Finally
            mbuSolicitacao = Nothing
            dsDados = Nothing
            dsAux = Nothing
        End Try
    End Sub

    Protected Sub preencherLoja(ByVal strQuery As String)
        Dim dsDados As DataSet
        Try
            mbuLoja = New buSolicitacao.buLoja
            dsDados = mbuLoja.Query(strQuery, "LOJADESCR")

            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    cblLojas.Items.Clear()
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        cblLojas.Items.Add(New ListItem(drRow("LOJADESCR"), drRow("LOJAID")))
                    Next
                    dvLoja.Visible = True
                Else
                    cblLojas.Items.Clear()
                    dvLoja.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuLoja = Nothing
        End Try
    End Sub

    Protected Sub preencherAssunto()
        Dim dsDados As DataSet
        Try
            mbuAssunto = New buSolicitacao.buAssunto
            If myProfile.UserInfo.PerfilID = enUserPerfil.Gerente Then 'vê assuntos relacionados
                dsDados = mbuAssunto.Query(assuntosRelacionados(myProfile.UserInfo.ID), "ASSUNTODESCR")
            Else
                dsDados = mbuAssunto.Query(, "ASSUNTODESCR")
            End If

            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    cblAssunto.Items.Clear()
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        cblAssunto.Items.Add(New ListItem(drRow("ASSUNTODESCR"), drRow("ASSUNTOID")))
                    Next
                    dvAssunto.Visible = True
                Else
                    cblAssunto.Items.Clear()
                    dvAssunto.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuAssunto = Nothing
        End Try
    End Sub

    Protected Sub preencherSubassuntos()
        Dim dsDados As New DataSet
        Dim strbAux As New StringBuilder

        Try
            mbuAssuntoSub = New buSolicitacao.buAssuntoSubassunto

            'cada vez que muda, carrega todos os subassuntos novamente
            If myProfile.UserInfo.PerfilID = enUserPerfil.Gerente Then 'vê assuntos relacionados
                dsDados = mbuAssuntoSub.QueryAssunto(strbAux.Clear.Append(subassuntosRelacionados(myProfile.UserInfo.ID)).ToString)
            Else
                dsDados = mbuAssuntoSub.QueryAssunto()
            End If

            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    cblSubassunto.Items.Clear()
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        cblSubassunto.Items.Add(New ListItem(strbAux.Clear.Append(drRow("ASSUNTODESCR")).Append(" - ").Append(drRow("SBADESCR")).ToString, drRow("ASSUBID")))
                    Next
                End If
            Else
                cblSubassunto.Items.Clear()
                dvSubassunto.Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnGerar_Click(sender As Object, e As EventArgs) Handles btnGerar.Click
        Try
            Response.Redirect("exportarSolicitacao.aspx", False)
        Catch ex As Exception
            GravarLog(ex.Message, "btnConsultar_Click de Relatorio.aspx", enLogStatus.enlsError)
        Finally
        End Try
    End Sub

    Protected Sub cbxTodasLojas_CheckedChanged(sender As Object, e As EventArgs) Handles cbxTodasLojas.CheckedChanged
        Try
            If cbxTodasLojas.Checked Then
                For Each item As ListItem In cblLojas.Items
                    item.Selected = True
                Next
                cbxTodasLojas.Text = "Limpar todas"
            Else
                For Each item As ListItem In cblLojas.Items
                    item.Selected = False
                Next
                cbxTodasLojas.Text = "Selecionar todas"
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "cbxTodasLojas_CheckedChanged de Relatorio.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub cbxTodosSubassuntos_CheckedChanged(sender As Object, e As EventArgs) Handles cbxTodosSubassuntos.CheckedChanged
        Try
            If cbxTodosSubassuntos.Checked Then
                For Each item As ListItem In cblSubassunto.Items
                    item.Selected = True
                Next
                cbxTodosSubassuntos.Text = "Limpar todos"
            Else
                For Each item As ListItem In cblSubassunto.Items
                    item.Selected = False
                Next
                cbxTodosSubassuntos.Text = "Selecionar todos"
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "cbxTodosSubAssuntos de Relatorio.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub cbxTodosAssuntos_CheckedChanged(sender As Object, e As EventArgs) Handles cbxTodosAssuntos.CheckedChanged
        Try
            If cbxTodosAssuntos.Checked Then
                For Each item As ListItem In cblAssunto.Items
                    item.Selected = True
                Next
                cbxTodosAssuntos.Text = "Limpar todos"
                dvSubassunto.Visible = True
                exibirSubassuntos()
            Else
                For Each item As ListItem In cblAssunto.Items
                    item.Selected = False
                Next
                cbxTodosAssuntos.Text = "Selecionar todos"
                dvSubassunto.Visible = False
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "cbxTodosAssuntos_CheckedChanged de Relatorio.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Private Function lojasRelacioandas(ByVal intUsId) As String
        Try
            Dim strbResultado, strbAux As New StringBuilder
            Dim dsDados As New DataSet
            mbuUsLoja = New buLogin.buUsuarioLoja
            strbAux.Append("USID = ").Append(intUsId)
            dsDados = mbuUsLoja.Query(strbAux.ToString)

            For Each drRow As DataRow In dsDados.Tables(0).Rows()
                strbResultado.Append("LOJA.LOJAID = ").Append(drRow("LOJAID")).Append(" OR ")
            Next

            strbResultado.Remove(strbResultado.Length - 4, 4) 'tirando o último 'OR'
            Return strbResultado.ToString
        Catch ex As Exception
            Throw ex
        Finally
            mbuUsLoja = Nothing
        End Try
    End Function

    Private Function subassuntosRelacionados(ByVal intUsId) As String
        Try
            Dim strbResultado, strbAux As New StringBuilder
            Dim dsDados As New DataSet
            mbuUsSub = New buLogin.buUsuarioSub
            strbAux.Append("USID = ").Append(intUsId)
            dsDados = mbuUsSub.Query(strbAux.ToString)

            For Each drRow As DataRow In dsDados.Tables(0).Rows()
                strbResultado.Append("ASSUNTOSUBASSUNTO.ASSUBID  = ").Append(drRow("ASSUBID")).Append(" OR ")
            Next

            strbResultado.Remove(strbResultado.Length - 4, 4) 'tirando o último 'OR'
            Return strbResultado.ToString
        Catch ex As Exception
            Throw ex
        Finally
            mbuUsLoja = Nothing
        End Try
    End Function

    Protected Function subassuntoSelecionado() As Boolean
        Try
            If dvSubassunto.Visible Then
                For Each item As ListItem In cblSubassunto.Items
                    If item.Selected Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Function lojaSelecionada() As Boolean
        Try
            If dvLoja.Visible Then
                For Each item As ListItem In cblLojas.Items
                    If item.Selected Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub cblAssunto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cblAssunto.SelectedIndexChanged
        Try
            exibirSubassuntos()
        Catch ex As Exception
            GravarLog(ex.Message, "cblAssunto_SelectedIndexChanged de Relatorio.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub exibirSubassuntos()
        Dim dsDados As New DataSet
        Dim visivel As Boolean = False
        Dim strbAux As New StringBuilder

        Try
            mbuAssuntoSub = New buSolicitacao.buAssuntoSubassunto

            'cada vez que muda, carrega todos os subassuntos novamente
            cblSubassunto.Items.Clear()

            For Each item As ListItem In cblAssunto.Items
                If item.Selected Then
                    If myProfile.UserInfo.PerfilID = enUserPerfil.Gerente Then
                        dsDados = mbuAssuntoSub.QueryAssunto(strbAux.Clear.Append("ASSUNTOSUBASSUNTO.ASSUNTOID = ").Append(item.Value).Append(" AND ").Append(subassuntosRelacionados(myProfile.UserInfo.ID)).ToString)
                    Else
                        dsDados = mbuAssuntoSub.QueryAssunto(strbAux.Clear.Append("ASSUNTOSUBASSUNTO.ASSUNTOID = ").Append(item.Value).ToString)
                    End If
                    If dsDados.Tables(0).Rows.Count > 0 Then
                        For Each drRow As DataRow In dsDados.Tables(0).Rows
                            cblSubassunto.Items.Add(New ListItem(strbAux.Clear.Append(drRow("ASSUNTODESCR")).Append(" - ").Append(drRow("SBADESCR")).ToString, drRow("ASSUBID")))
                            visivel = True
                        Next
                    End If
                End If
            Next
            dvSubassunto.Visible = visivel
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function assuntosRelacionados(ByVal intUsId) As String
        Try
            Dim strbResultado, strbAux As New StringBuilder
            Dim dsDados As New DataSet
            mbuUsAssunto = New buLogin.buUsuarioAssunto
            strbAux.Append("USID = ").Append(intUsId)
            dsDados = mbuUsAssunto.Query(strbAux.ToString)

            For Each drRow As DataRow In dsDados.Tables(0).Rows()
                strbResultado.Append("ASSUNTO.ASSUNTOID = ").Append(drRow("ASSUNTOID")).Append(" OR ")
            Next

            strbResultado.Remove(strbResultado.Length - 4, 4) 'tirando o último 'OR'
            Return strbResultado.ToString
        Catch ex As Exception
            Throw ex
        Finally
            mbuUsLoja = Nothing
        End Try
    End Function

    Protected Function assuntoSelecionado() As Boolean
        Try
            If dvAssunto.Visible Then
                For Each item As ListItem In cblAssunto.Items
                    If item.Selected Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Protected Sub ddlAssunto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssunto.SelectedIndexChanged
    '    Dim strbAux As New StringBuilder
    '    Dim dsDados, dsAux As DataSet
    '    Try
    '        If Not ddlAssunto.SelectedValue.Equals("-1") Then
    '            'preencher ddlSubassunto
    '            mbuAssuntoSub = New buSolicitacao.buAssuntoSubassunto
    '            dsDados = mbuAssuntoSub.Query(strbAux.Append("ASSUNTOID = ").Append(ddlAssunto.SelectedValue).ToString)

    '            If Not IsNothing(dsDados) Then
    '                If dsDados.Tables(0).Rows().Count() > 0 Then
    '                    mbuSub = New buSolicitacao.buSubassunto
    '                    ddlSubassunto.Items.Clear()
    '                    ddlSubassunto.Items.Add(New ListItem("Todos", -1))

    '                    'obtém a descrição do subassunto relacionado a cada id de subassunto
    '                    For Each drRow As DataRow In dsDados.Tables(0).Rows()
    '                        dsAux = mbuSub.Query(strbAux.Clear().Append("SBAID = ").Append(drRow("SBAID")).ToString)
    '                        ddlSubassunto.Items.Add(New ListItem(dsAux.Tables(0).Rows(0).Item("SBADESCR"), dsAux.Tables(0).Rows(0).Item("SBAID")))
    '                    Next

    '                End If
    '            End If
    '        Else
    '            ddlSubassunto.Items.Clear()
    '            ddlSubassunto.Items.Add(New ListItem("Selecione", -1))
    '        End If
    '    Catch ex As Exception
    '        GravarLog(ex.Message, "ddlAssunto_SelectedIndexChanged de Relatorio.aspx", enLogStatus.enlsError)
    '    Finally
    '        dsDados = Nothing
    '    End Try
    'End Sub

    'Protected Sub ddlRegiao_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRegiao.SelectedIndexChanged
    '    Dim strbAux As New StringBuilder
    '    Dim dsDados As DataSet
    '    Try
    '        If Not ddlRegiao.SelectedValue.Equals("-1") And Not ddlRegiao.SelectedValue = enLojaRegiao.Brasil Then
    '            If myProfile.UserInfo.PerfilID = enUserPerfil.Gerente Then
    '                preencherLoja(strbAux.Append("LOJAREGIAO = ").Append(ddlRegiao.SelectedValue).Append(" AND (").Append(lojasRelacioandas(myProfile.UserInfo.ID)).Append(")").ToString)
    '            Else
    '                preencherLoja(strbAux.Append("LOJAREGIAO = ").Append(ddlRegiao.SelectedValue).ToString)
    '            End If
    '        Else
    '            cblLojas.Items.Clear()
    '            dvLoja.Visible = False
    '        End If
    '    Catch ex As Exception
    '        GravarLog(ex.Message, "ddlAssunto_SelectedIndexChanged de CadastroUsuario.aspx", enLogStatus.enlsError)
    '    Finally
    '        dsDados = Nothing
    '    End Try
    'End Sub

End Class