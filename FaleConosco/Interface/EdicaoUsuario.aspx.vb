﻿Imports plAppLibrary.Enumerators
Imports plAppLibrary.clsCommonFunctions

Public Class EdicaoUsuario
    Inherits System.Web.UI.Page

    Private mbuLog As buServicosGerais.buLog
    Private mbuUsuario As buLogin.buUsuario
    Private mbuPerfil As buLogin.buPerfil
    Private mbuGrupo As buLogin.buGrupo
    Private mbuLoja As buSolicitacao.buLoja
    Private myProfile As New ProfileCommonMakro
    Private mbuUsLoja As New buLogin.buUsuarioLoja
    Private mbuAssunto As New buSolicitacao.buAssunto
    Private mbuUsAssunto As New buLogin.buUsuarioAssunto
    Private mbuAssuntoSub As buSolicitacao.buAssuntoSubassunto
    Private mbuUsSub As buLogin.buUsuarioSub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Request.QueryString("usid") Is Nothing Then
                If IsNothing(Session("Logado")) Then
                    Response.Redirect("LoginAdmin.aspx", False)
                Else
                    Session("Logado") = Session("Logado")
                    If Session("Logado") = 1 Then
                        If myProfile.UserInfo.PerfilID <> enUserPerfil.Master Then
                            Response.Redirect("LoginAdmin.aspx", False) 'só o master pode editar
                        End If
                    Else
                        Response.Redirect("LoginAdmin.aspx", False)
                    End If
                End If
                lblMensagem.Text = ""
                If Not IsPostBack Then
                    preencherPerfil()
                    preencherAssunto()
                    preencherCampos()
                    CType(Page.Master.FindControl("liSol"), HtmlGenericControl).Attributes.Remove("class")
                    CType(Page.Master.FindControl("liUsuario"), HtmlGenericControl).Attributes.Add("class", "selecionado")
                    CType(Page.Master.FindControl("liRelatorio"), HtmlGenericControl).Attributes.Remove("class")
                End If
            Else
                Response.Redirect("ConsultaUsuario.aspx", False)
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "Page_Load de EdicaoUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub preencherCampos()
        Dim dsDados, dsAux As New DataSet
        Dim strbAux As New StringBuilder
        Try
            mbuUsuario = New buLogin.buUsuario
            dsDados = mbuUsuario.Query(strbAux.Append("USID = ").Append(Decrypt(Request.QueryString("usid"))).Append(" AND USSTATUS <> 0").ToString)
            If dsDados.Tables(0).Rows().Count > 0 Then
                With dsDados.Tables(0).Rows(0)
                    txtEmail.Text = .Item("USEMAIL")
                    ddlPerfil.SelectedValue = .Item("PERFID")
                    txtSenha.Text = Decrypt(.Item("USSENHA"))
                    If ddlPerfil.SelectedValue = enUserPerfil.Master Then
                        'ddlRegiao.SelectedValue = enLojaRegiao.Brasil
                        'ddlRegiao.Enabled = False
                        dvAssunto.Visible = False
                        dvLoja.Visible = False
                        fsAssunto.Visible = False
                        fsSubAssunto.Visible = False
                        fsLoja.Visible = False
                    End If
                End With
            End If

            'preenchendo lojas

            'mbuUsuario = New buLogin.buUsuario
            'dsDados = mbuUsuario.QueryRegiao(strbAux.Clear.Append("USID = ").Append(myProfile.UserInfo.ID).ToString, "USEMAIL")
            mbuUsLoja = New buLogin.buUsuarioLoja
            dsDados = mbuUsLoja.Query(strbAux.Clear().Append("USID = ").Append(Decrypt(Request.QueryString("usid"))).ToString)
            preencherLoja("")
            If dsDados.Tables(0).Rows().Count > 0 Then

                'ddlRegiao.SelectedValue = dsDados.Tables(0).Rows(0).Item("LOJAREGIAO")

                'If Not ddlRegiao.SelectedValue = enLojaRegiao.Brasil Then 'se não for brasil, exibe uma a uma
                'preencherLoja(strbAux.Clear.Append("LOJAREGIAO = ").Append(ddlRegiao.SelectedValue).ToString)

                'Alterado por Eduardo Junque em 22/02/2018
                'Motivo: preencher loja deve ficar fora da validação se já existe loja vinculada a esse usuario, dessa forma mesmo quando não há essa relação o sistema lista as lojas
                'preencherLoja("")

                For Each drRow As DataRow In dsDados.Tables(0).Rows()
                    'pra cada relação loja-usuario encontrada, marcar em cblLojas
                    cblLojas.Items.FindByValue(drRow.Item("LOJAID")).Selected = True
                Next
                'Else
                '   dvLoja.Visible = False
                ' End If
            End If

            'mbuUsLoja = New buLogin.buUsuarioLoja

            ''se for do brasil todo, vai ser o primeiro registro desse usuario
            'dsDados = mbuUsLoja.Query(strbAux.Clear.Append("USID = ").Append(Decrypt(Request.QueryString("usid"))).ToString, "ULID")

            'If dsDados.Tables(0).Rows().Count > 0 Then

            '    'exibe as lojas da Regiao do usuário → query de lojas para descobrir Regiao
            '    mbuLoja = New buSolicitacao.buLoja
            '    dsAux = mbuLoja.Query(strbAux.Clear.Append("LOJAID = ").Append(dsDados.Tables(0).Rows(0).Item("LOJAID")).ToString)
            '    ddlRegiao.SelectedValue = dsAux.Tables(0).Rows(0).Item("LOJAREGIAO")

            '    If Not dsAux.Tables(0).Rows(0).Item("LOJAREGIAO") = enLojaRegiao.Brasil Then 'se não for brasil, exibe uma a uma
            '        preencherLoja(strbAux.Clear.Append("LOJAREGIAO = ").Append(dsAux.Tables(0).Rows(0).Item("LOJAREGIAO")).ToString)

            '        For Each drRow As DataRow In dsDados.Tables(0).Rows()
            '            'pra cada relação loja-usuario encontrada, marcar em cblLojas
            '            cblLojas.Items.FindByValue(drRow.Item("LOJAID")).Selected = True
            '        Next
            '    Else
            '        dvLoja.Visible = False
            '    End If

            'End If

            'marcando assuntos relacionados
            mbuUsAssunto = New buLogin.buUsuarioAssunto
            dsDados = mbuUsAssunto.Query(strbAux.Clear.Append("USID = ").Append(Decrypt(Request.QueryString("usid"))).ToString)
            For Each drRow As DataRow In dsDados.Tables(0).Rows()
                cblAssunto.Items.FindByValue(drRow.Item("ASSUNTOID")).Selected = True
            Next

            'marcando subassuntos relacionados
            exibirSubassuntos()
            mbuUsSub = New buLogin.buUsuarioSub
            dsDados = mbuUsSub.Query(strbAux.Clear.Append("USID = ").Append(Decrypt(Request.QueryString("usid"))).ToString)
            For Each drRow As DataRow In dsDados.Tables(0).Rows()
                cblSubassunto.Items.FindByValue(drRow.Item("ASSUBID")).Selected = True
            Next

        Catch ex As Exception
            dsDados = Nothing
            mbuUsuario = Nothing
            mbuUsLoja = Nothing
            mbuUsAssunto = Nothing
        End Try
    End Sub

    Protected Sub preencherAssunto()
        Try
            mbuAssunto = New buSolicitacao.buAssunto
            Dim dsDados As DataSet = mbuAssunto.Query(, "ASSUNTODESCR")
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    cblAssunto.Items.Clear()
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        cblAssunto.Items.Add(New ListItem(drRow("ASSUNTODESCR"), drRow("ASSUNTOID")))
                    Next
                    dvAssunto.Visible = True
                Else
                    cblAssunto.Items.Clear()
                    dvAssunto.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuAssunto = Nothing
        End Try
    End Sub

    Protected Sub preencherLoja(ByVal strQuery As String)
        Try
            mbuLoja = New buSolicitacao.buLoja
            Dim dsDados As DataSet = mbuLoja.Query(strQuery, "LOJADESCR")
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    cblLojas.Items.Clear()
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        cblLojas.Items.Add(New ListItem(drRow("LOJADESCR"), drRow("LOJAID")))
                    Next
                    dvLoja.Visible = True
                Else
                    cblLojas.Items.Clear()
                    dvLoja.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuLoja = Nothing
        End Try
    End Sub

    Protected Sub preencherPerfil()
        Dim dsDados As DataSet
        Try
            mbuPerfil = New buLogin.buPerfil
            dsDados = mbuPerfil.Query()
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    ddlPerfil.Items.Clear()
                    ddlPerfil.Items.Add(New ListItem("Selecione", -1))
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        ddlPerfil.Items.Add(New ListItem(drRow("PERFDESCR"), drRow("PERFID")))
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuPerfil = Nothing
            dsDados = Nothing
        End Try
    End Sub

    Private Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub

    Protected Sub btnAlterar_Click(sender As Object, e As EventArgs) Handles btnAlterar.Click
        Dim dsDados, dsIncluir, dsDadosAuxBatch As New DataSet
        Dim drRowAuxBatch As DataRow
        Dim intUsId As Integer = Decrypt(Request.QueryString("usid"))
        Dim myTrans As plAppLibrary.Transaction
        Dim blnSucesso As Boolean

        Try
            If txtEmail.Text.Trim.Equals("") Or ddlPerfil.SelectedValue = -1 Or (Not lojaSelecionada() And ddlPerfil.SelectedValue = enUserPerfil.Gerente) Then
                lblMensagem.Text = "Preencha todos os campos"
            Else
                If validaEmail(txtEmail.Text) Then
                    mbuUsuario = New buLogin.buUsuario
                    mbuUsLoja = New buLogin.buUsuarioLoja
                    mbuLoja = New buSolicitacao.buLoja
                    mbuUsSub = New buLogin.buUsuarioSub

                    myTrans = New plAppLibrary.Transaction
                    myTrans.IniciaTransacao()

                    dsDados = mbuUsuario.Query(New StringBuilder().Append("USID = ").Append(intUsId).ToString, , myTrans)
                    'blnSucesso = mbuUsuario.Update(intUsId, ddlPerfil.SelectedValue, txtEmail.Text, dsDados.Tables(0).Rows(0).Item("USSENHA"), dsDados.Tables(0).Rows(0).Item("USALTERASENHA"), System.DateTime.Now.Date, dsDados.Tables(0).Rows(0).Item("USSTATUS"), myTrans)

                    'limpar todas as relações de loja e salvar as novas
                    blnSucesso = mbuUsLoja.DeleteUs(intUsId, myTrans)

                    'If ddlRegiao.SelectedValue = enLojaRegiao.Brasil Then 'brasil todo

                    '    'antes de relacionar com todas as lojas, relacionar com a loja "brasil"
                    '    dsDados = mbuLoja.Query(New StringBuilder().Append("LOJAREGIAO = ").Append(enLojaRegiao.Brasil).ToString, , myTrans)
                    '    blnSucesso = mbuUsLoja.Insert(dsDados.Tables(0).Rows(0).Item("LOJAID"), intUsId, myTrans)

                    '    dsDados = mbuLoja.Query(, "LOJAID", myTrans) 'todas as lojas
                    '    dsIncluir = mbuUsLoja.Query("USID = -1", , myTrans) 'apenas criando o dataset vazio

                    '    For Each drRow As DataRow In dsDados.Tables(0).Rows 'para cada linha de loja, incluir relação loja-usuario
                    '        Dim novaLinha As DataRow = dsIncluir.Tables(0).NewRow()
                    '        novaLinha("LOJAID") = drRow("LOJAID")
                    '        novaLinha("USID") = intUsId
                    '        dsIncluir.Tables(0).Rows.Add(novaLinha)
                    '    Next

                    '    blnSucesso = mbuUsLoja.UpdateBatch(dsIncluir, myTrans)

                    'Else

                    For Each item As ListItem In cblLojas.Items
                        'pra cada loja selecionada, inserir em USUARIOLOJA
                        If item.Selected Then
                            blnSucesso = mbuUsLoja.Insert(item.Value, intUsId, myTrans)
                        End If
                    Next
                    'End If

                    'incluindo relações de assunto
                    If ddlPerfil.SelectedValue = enUserPerfil.Gerente Then
                        If assuntoSelecionado() Then
                            'limpar todas as relações de assunto e salvar as novas
                            blnSucesso = mbuUsAssunto.DeleteUs(intUsId)

                            'obtem estrutura da tabela
                            dsDadosAuxBatch = mbuUsAssunto.Query("UAID = -1")

                            For Each item As ListItem In cblAssunto.Items
                                'pra cada assunto selecionado, inserir em USUARIOASSUNTO
                                If item.Selected Then
                                    'Retirada inserção de linha a linha e substituido por UpdateBatch
                                    drRowAuxBatch = dsDadosAuxBatch.Tables(0).NewRow()
                                    drRowAuxBatch("USID") = intUsId
                                    drRowAuxBatch("ASSUNTOID") = item.Value
                                    dsDadosAuxBatch.Tables(0).Rows().Add(drRowAuxBatch)
                                    'blnSucesso = mbuUsAssunto.Insert(intUsId, item.Value, myTrans)
                                End If
                            Next
                            blnSucesso = mbuUsAssunto.UpdateBatch(dsDadosAuxBatch, myTrans)

                        Else
                            myTrans.FinalizaTransacao(enTransactionStatus.entsFailed)
                            lblMensagem.Text = "Preencha todos os campos"
                        End If

                    End If

                    'incluindo relações de subassunto
                    'Deleta subassuntos atuais para incluir a nova seleção.
                    blnSucesso = mbuUsSub.DeleteUs(intUsId)
                    dsDadosAuxBatch = mbuUsSub.Query("USSBAID = -1")
                    mbuUsSub = New buLogin.buUsuarioSub
                    If ddlPerfil.SelectedValue = enUserPerfil.Gerente Then
                        If subassuntoSelecionado() Then
                            For Each item As ListItem In cblSubassunto.Items
                                'pra cada assunto selecionado, inserir em USUARIOASSUNTO
                                If item.Selected Then

                                    'Retirada inserção de linha a linha e substituido por UpdateBatch
                                    'blnSucesso = mbuUsSub.Insert(intUsId, item.Value, myTrans)
                                    drRowAuxBatch = dsDadosAuxBatch.Tables(0).NewRow()
                                    drRowAuxBatch("USID") = intUsId
                                    drRowAuxBatch("ASSUBID") = item.Value
                                    dsDadosAuxBatch.Tables(0).Rows().Add(drRowAuxBatch)
                                End If
                            Next
                        Else
                            myTrans.FinalizaTransacao(enTransactionStatus.entsFailed)
                            lblMensagem.Text = "Preencha todos os campos"
                        End If
                        blnSucesso = mbuUsSub.UpdateBatch(dsDadosAuxBatch, myTrans)
                        myTrans.FinalizaTransacao(enTransactionStatus.entsSuccess)

                    Else 'master não tem relação com subassunto

                        myTrans.FinalizaTransacao(enTransactionStatus.entsSuccess)

                    End If


                    If blnSucesso Then
                        PopupMensagem.Mensagem = "Dados cadastrais alterados com sucesso"
                        PopupMensagem.ShowPopup()
                    Else
                        PopupMensagem.Mensagem = "Ocorreu um erro inesperado.\nTente novamente."
                        PopupMensagem.ShowPopup()
                    End If

                Else
                    lblMensagem.Text = "Informe um e-mail válido"
                End If
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "btnAlterar_Click de EdicaoUsuario.aspx", enLogStatus.enlsError)
            PopupMensagem.Mensagem = "Ocorreu um erro inesperado.\nTente novamente."
            PopupMensagem.ShowPopup()
        Finally
            mbuUsuario = Nothing
            mbuUsLoja = Nothing
            mbuLoja = Nothing
            myTrans = Nothing
        End Try
    End Sub

    Protected Function subassuntoSelecionado() As Boolean
        Try
            If dvSubassunto.Visible Then
                For Each item As ListItem In cblSubassunto.Items
                    If item.Selected Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function validaEmail(ByVal email As String) As Boolean
        Dim emailRegex As New System.Text.RegularExpressions.Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        Dim emailMatch As System.Text.RegularExpressions.Match = emailRegex.Match(email)
        Return emailMatch.Success
    End Function

    Protected Sub PopupMensagem_OK() Handles PopupMensagem.OK
        Try
            Response.Redirect("ConsultaUsuario.aspx")
        Catch ex As Exception
            GravarLog(ex.Message, "PopupMensagem_OK", enLogStatus.enlsFailed)
        End Try
    End Sub

    'Protected Sub ddlRegiao_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRegiao.SelectedIndexChanged
    '    Dim strbAux As New StringBuilder
    '    Dim dsDados As DataSet
    '    Try
    '        If Not ddlRegiao.SelectedValue.Equals("-1") And Not ddlRegiao.SelectedValue = enLojaRegiao.Brasil Then
    '            preencherLoja(strbAux.Append("LOJAREGIAO = ").Append(ddlRegiao.SelectedValue).ToString)
    '        Else
    '            cblLojas.Items.Clear()
    '            dvLoja.Visible = False
    '        End If
    '    Catch ex As Exception
    '        GravarLog(ex.Message, "ddlAssunto_SelectedIndexChanged de EdicaoUsuario.aspx", enLogStatus.enlsError)
    '    Finally
    '        dsDados = Nothing
    '    End Try
    'End Sub

    Protected Function lojaSelecionada() As Boolean
        Try
            If dvLoja.Visible Then
                For Each item As ListItem In cblAssunto.Items
                    If item.Selected Then
                        Return True
                    End If
                Next
            Else
                If cbxTodosSubassuntos.Checked Then 'brasil todo
                    Return True
                End If
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub cbxTodasLojas_CheckedChanged(sender As Object, e As EventArgs) Handles cbxTodasLojas.CheckedChanged
        Try
            If cbxTodasLojas.Checked Then
                For Each item As ListItem In cblLojas.Items
                    item.Selected = True
                Next
                cbxTodasLojas.Text = "Limpar todas"
            Else
                For Each item As ListItem In cblLojas.Items
                    item.Selected = False
                Next
                cbxTodasLojas.Text = "Selecionar todas"
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "cbxTodasLojas_CheckedChanged de EdicaoUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub


    Protected Sub cbxTodosSubassuntos_CheckedChanged(sender As Object, e As EventArgs) Handles cbxTodosSubassuntos.CheckedChanged
        Try
            If cbxTodosSubassuntos.Checked Then
                For Each item As ListItem In cblSubassunto.Items
                    item.Selected = True
                Next
                cbxTodosSubassuntos.Text = "Limpar todos"
            Else
                For Each item As ListItem In cblSubassunto.Items
                    item.Selected = False
                Next
                cbxTodosSubassuntos.Text = "Selecionar todos"
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "cbxTodosSubassuntos_CheckedChanged de EdicaoUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub cbxTodosAssuntos_CheckedChanged(sender As Object, e As EventArgs) Handles cbxTodosAssuntos.CheckedChanged
        Try
            If cbxTodosAssuntos.Checked Then
                For Each item As ListItem In cblAssunto.Items
                    item.Selected = True
                Next
                cbxTodosAssuntos.Text = "Limpar todos"
                exibirSubassuntos()
                dvSubassunto.Visible = True
            Else
                For Each item As ListItem In cblAssunto.Items
                    item.Selected = False
                Next
                cbxTodosAssuntos.Text = "Selecionar todos"
                dvSubassunto.Visible = False
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "cbxTodasLojas_CheckedChanged de EdicaoUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Function assuntoSelecionado() As Boolean
        Try
            If dvAssunto.Visible Then
                For Each item As ListItem In cblAssunto.Items
                    If item.Selected Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub ddlPerfil_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPerfil.SelectedIndexChanged
        Try
            If ddlPerfil.SelectedValue = enUserPerfil.Master Then
                'ddlRegiao.SelectedValue = enLojaRegiao.Brasil
                'ddlRegiao.Enabled = False
                dvAssunto.Visible = False
                dvLoja.Visible = False
                dvSubassunto.Visible = False
                fsAssunto.Visible = False
                fsSubAssunto.Visible = False
                fsLoja.Visible = False
            Else
                'ddlRegiao.SelectedValue = -1
                'ddlRegiao.Enabled = True
                dvAssunto.Visible = True
                dvLoja.Visible = True
                fsAssunto.Visible = True
                fsSubAssunto.Visible = True
                fsLoja.Visible = True
                If cblLojas.Items.Count = 0 Then
                    preencherLoja("")
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cblAssunto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cblAssunto.SelectedIndexChanged
        Try
            exibirSubassuntos()
        Catch ex As Exception
            GravarLog(ex.Message, "cblAssunto_SelectedIndexChanged de EdicaoUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub exibirSubassuntos()
        Dim dsDados As New DataSet
        Dim visivel As Boolean = False
        Dim strbAux As New StringBuilder

        Try
            mbuAssuntoSub = New buSolicitacao.buAssuntoSubassunto

            'cada vez que muda, carrega todos os subassuntos novamente
            cblSubassunto.Items.Clear()

            For Each item As ListItem In cblAssunto.Items
                If item.Selected Then
                    dsDados = mbuAssuntoSub.QueryAssunto(strbAux.Clear.Append("ASSUNTOSUBASSUNTO.ASSUNTOID = ").Append(item.Value).ToString)
                    If dsDados.Tables(0).Rows.Count > 0 Then
                        For Each drRow As DataRow In dsDados.Tables(0).Rows
                            cblSubassunto.Items.Add(New ListItem(strbAux.Clear.Append(drRow("ASSUNTODESCR")).Append(" - ").Append(drRow("SBADESCR")).ToString, drRow("ASSUBID")))
                            visivel = True
                        Next
                    End If
                End If
            Next
            dvSubassunto.Visible = visivel
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class