﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Relatorio.aspx.vb" Inherits="FaleConosco.Relatorio" MasterPageFile="../FaleConosco.Master" %>

<%@ Register Src="~/UserControl/wucPopupMensagem.ascx" TagPrefix="uc1" TagName="wucPopupMensagem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">

    <!-- css -->
    <link href="../Style/consultaSolicitacao.css" rel="stylesheet" />

    <!-- js -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="../Scripts/FormataMascara.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph2" runat="Server">
    <div class="baixo" style="width: 90%">
        <div class="box">
            <h2>Relatório das Solicitações</h2>
            <div class="validacao">
                <asp:Label ID="lblMensagem" runat="server" Text="" ForeColor="Red"></asp:Label>
            </div>
            <asp:Panel ID="Panel1" runat="server" GroupingText="Filtro">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="linha-1">
                            <div class="inicio-rel">
                                <h4>De</h4>
                                <asp:TextBox ID="txtInicio" runat="server" MaxLength="10" TabIndex="1" onkeydown="formataData(this, event)"></asp:TextBox>
                            </div>

                            <div class="fim-rel">
                                <h4>Até</h4>
                                <asp:TextBox ID="txtFim" runat="server" MaxLength="10" TabIndex="2" onkeydown="formataData(this, event)"></asp:TextBox>
                            </div>

                            <%--<div class="assunto">
                                <h4>Assunto</h4>
                                <asp:DropDownList ID="ddlAssunto" runat="server" TabIndex="3" AutoPostBack="True"></asp:DropDownList>
                            </div>--%>

                            <%--                            
                            ****comentado em 27/10 a pedidos de retirar o conceito de Região do sistema

                            <div class="assunto">
                                <h4>Região</h4>
                                <asp:DropDownList ID="ddlRegiao" runat="server" TabIndex="4" Style="width: 100%" AutoPostBack="True">
                                    <asp:ListItem Value="-1">Selecione</asp:ListItem>
                                    <asp:ListItem Value="5">Centro</asp:ListItem>
                                    <asp:ListItem Value="6">Nordeste</asp:ListItem>
                                    <asp:ListItem Value="7">Norte</asp:ListItem>
                                    <asp:ListItem Value="4">Rio de Janeiro/Espírito Santo</asp:ListItem>
                                    <asp:ListItem Value="1">São Paulo</asp:ListItem>
                                    <asp:ListItem Value="2">São Paulo-Interior</asp:ListItem>
                                    <asp:ListItem Value="3">São Paulo Metropolitana</asp:ListItem>
                                    <asp:ListItem Value="8">Sul</asp:ListItem>
                                    <asp:ListItem Value="9">Brasil</asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                        </div>

                        <fieldset class="check-list" runat="server" id="fsAssunto">
                            <h4>Assunto</h4>
                            <div class="assunto-lista" id="dvAssunto" runat="server">
                                <asp:CheckBox ID="cbxTodosAssuntos" runat="server" Text="Selecionar todos" ToolTip="Clique para selecionar/limpar todos os assuntos listados" AutoPostBack="True" />
                                <asp:Panel ID="Panel2" runat="server">
                                    <asp:CheckBoxList ID="cblAssunto" runat="server" RepeatColumns="3" AutoPostBack="True"></asp:CheckBoxList>
                                </asp:Panel>
                            </div>
                        </fieldset>

                        <fieldset class="check-list" runat="server" id="Fieldset1">
                            <h4>Subassunto</h4>
                            <div class="subassunto-lista" id="dvSubassunto" runat="server">
                                <asp:CheckBox ID="cbxTodosSubassuntos" runat="server" Text="Selecionar todos" ToolTip="Clique para selecionar/limpar todas os subassuntos listados" AutoPostBack="True" />
                                <asp:Panel ID="Panel4" runat="server">
                                    <asp:CheckBoxList ID="cblSubassunto" runat="server" RepeatColumns="3" AutoPostBack="True"></asp:CheckBoxList>
                                </asp:Panel>
                            </div>
                        </fieldset>

                        <fieldset class="check-list" runat="server" id="Fieldset2">
                            <h4>Lojas</h4>
                            <div class="loja-lista" id="dvLoja" runat="server">
                                <asp:CheckBox ID="cbxTodasLojas" runat="server" Text="Selecionar todas" ToolTip="Clique para selecionar/limpar todas as lojas listadas" AutoPostBack="True" />
                                <asp:Panel ID="Panel3" runat="server">
                                    <asp:CheckBoxList ID="cblLojas" runat="server" RepeatColumns="3"></asp:CheckBoxList>
                                </asp:Panel>
                            </div>
                        </fieldset>
                        <div class="ir-voltar">
                            <asp:LinkButton ID="btnConsultar" runat="server" class="botao btnConsultar" Style="width: 120px; float: right; margin-bottom: -10px;">Consultar</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </asp:Panel>
            <h4 style="margin-top: 15px;">Solicitações encontradas:</h4>
            <asp:UpdatePanel ID="updPanel" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvSolicitacao" runat="server" AllowPaging="True"
                        AutoGenerateColumns="False" EmptyDataText="Nenhuma solicitação cadastrada no momento" DataKeyNames="SOLID" CellPadding="0" class="grid">
                        <AlternatingRowStyle BackColor="#EEEEEE" />
                        <PagerStyle CssClass="pagination" HorizontalAlign="Center"></PagerStyle>
                        <Columns>
                            <asp:BoundField DataField="SOLPROTOCOLO" HeaderText="Protocolo" />
                            <asp:BoundField DataField="STATUSDESCR" HeaderText="Status" />
                            <asp:BoundField DataField="SOLDTCADASTRO" HeaderText="Cadastro" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="primMensagem" HeaderText="Atendimento" DataFormatString="{0:dd/MM/yyyy}" />
                            <%--<asp:BoundField DataField="SOLMSGDTCADASTRO" HeaderText="Última Interação" DataFormatString="{0:dd/MM/yyyy}" />--%>
                            <asp:BoundField DataField="SOLEMAIL" HeaderText="E-mail Cliente" />
                            <asp:BoundField DataField="USEMAIL" HeaderText="E-mail Atendente" />
                            <%--<asp:BoundField DataField="LOJAREGIAO" HeaderText="Região" />--%>
                            <asp:BoundField DataField="LOJAID" HeaderText="Loja" />
                            <asp:BoundField DataField="ASSUNTODESCR" HeaderText="Assunto" />
                            <asp:BoundField DataField="SBADESCR" HeaderText="Subassunto" />
                        </Columns>
                    </asp:GridView>
                    <div class="ir-voltar">
                        <asp:LinkButton ID="btnGerar" runat="server" class="botao btnRelatorio" Style="width: 160px; float: right; margin: 10px 0 -20px;">Gerar relatório</asp:LinkButton>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var tamanhoJanela = $(window).width() - 169;
            $('.content').css('width', tamanhoJanela);
        });
    </script>
</asp:Content>
