﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AlteracaoSenha.aspx.vb" Inherits="FaleConosco.AlteracaoSenha" MasterPageFile="~/FaleConosco.Master" %>

<%@ Register Src="~/UserControl/wucPopupMensagem.ascx" TagPrefix="uc1" TagName="wucPopupMensagem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">

    <!-- css -->
    <link href="../Style/alteracaoSenha.css" rel="stylesheet" />

    <!-- js -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph2" runat="Server">
    <div class="baixo">
        <div class="box" style="width: 50%; margin: 0 auto;">
            <h2>Alteração de Senha</h2>
            <h3>Todos os campos são obrigatórios</h3>
            <div class="validacao">
                <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label></div>
            <div class="antiga">
                <h4>Senha antiga</h4>
                <asp:TextBox ID="txtAntiga" runat="server" MaxLength="200" TextMode="Password"></asp:TextBox>
            </div>

            <div class="nova">
                <h4>Nova senha</h4>
                <asp:TextBox ID="txtNova" runat="server" MaxLength="200" TextMode="Password"></asp:TextBox>
            </div>

            <div class="confirmacao">
                <h4>Confirmação de senha</h4>
                <asp:TextBox ID="txtConfirmacao" runat="server" TextMode="Password" MaxLength="200"></asp:TextBox>
            </div>

            <div class="ir-voltar">
                <asp:LinkButton ID="btnAlterar" runat="server" class="botao btnEsq" Style="width:100px">Alterar</asp:LinkButton>
                <asp:LinkButton ID="btnRetornar" runat="server" class="botao btnDir" Style="width:110px" PostBackUrl="~/Interface/ConsultaUsuario.aspx">Retornar</asp:LinkButton>
            </div>
        </div>
    </div>
    <uc1:wucPopupMensagem runat="server" ID="PopupMensagem" />

    <script>
        $(document).ready(function () {
            var tamanhoJanela = $(window).width() - 169;
            $('.content').css('width', tamanhoJanela);
        });
</script>
</asp:Content>
