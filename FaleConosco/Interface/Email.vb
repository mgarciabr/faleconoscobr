﻿Imports System.Net
Imports System.Net.Mail
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates

Public Class Email

    Public Sub New()
        Me.Sender_ = String.Empty
    End Sub

    Public Sub New(ByVal pFrom As String, ByVal pTo As String, ByVal pTitulo As String, ByVal pTexto As String)
    End Sub

    Public Sub New(ByVal pFrom As String, ByVal pTo As String, ByVal pTitulo As String, ByVal pTexto As String, ByVal pSender As String)
        Me.From_ = pFrom
        Me.To_ = System.Configuration.ConfigurationManager.AppSettings("EMAIL_TO")
        Me.Titulo_ = pTitulo
        Me.Texto_ = pTexto
        Me.Sender_ = pSender
    End Sub

    Private Sender_ As String

    Private From_ As String

    Private FromName_ As String

    Private To_ As String

    Private Cc_ As MailAddressCollection

    Private Titulo_ As String

    Private Texto_ As String

    Public Property EmailFrom As String
        Get
            Return Me.From_
        End Get

        Set(ByVal value As String)
            Me.From_ = value
        End Set
    End Property

    Public Property EmailFromName As String
        Get
            Return Me.FromName_
        End Get

        Set(ByVal value As String)
            Me.FromName_ = value
        End Set
    End Property

    Public Property EmailTo As String
        Get
            Return Me.To_
        End Get

        Set(ByVal value As String)
            Me.To_ = value
        End Set
    End Property

    Public Property EmailCc As MailAddressCollection
        Get
            Return Me.Cc_
        End Get

        Set(ByVal value As MailAddressCollection)
            Me.Cc_ = value
        End Set
    End Property

    Public Property EmailTitulo As String
        Get
            Return Me.Titulo_
        End Get

        Set(ByVal value As String)
            Me.Titulo_ = value
        End Set
    End Property

    Public Property EmailTexto As String
        Get
            Return Me.Texto_
        End Get

        Set(ByVal value As String)
            Me.Texto_ = value
        End Set
    End Property

    Public Property EmailSender As String
        Get
            Return Me.Sender_
        End Get

        Set(ByVal value As String)
            Me.Sender_ = value
        End Set
    End Property

    Protected Function customCertValidation(ByVal sender As Object, ByVal cert As X509Certificate, ByVal chain As X509Chain, ByVal errors As SslPolicyErrors) As Boolean
        Return True
    End Function

    Public Sub Enviar()
        Dim oSmtp As SmtpClient = New SmtpClient()
        oSmtp.EnableSsl = True
        oSmtp.Port = 587
        Dim oEmail As MailMessage = New MailMessage()
        If Not Me.Sender_.Equals(String.Empty) Then
            oEmail.Sender = New MailAddress(Me.Sender_)
        End If

        oEmail.From = New MailAddress(Me.From_, System.Configuration.ConfigurationManager.AppSettings("EMAIL_SEND"))
        oEmail.[To].Add(Me.To_)
        If Me.EmailCc IsNot Nothing Then
            For i As Integer = 0 To Me.EmailCc.Count - 1
                oEmail.CC.Add(Me.EmailCc(i))
            Next
        End If

        oEmail.Priority = MailPriority.Normal
        oEmail.IsBodyHtml = True
        oEmail.Subject = Me.Titulo_
        oEmail.Body = Me.Texto_
        oEmail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1")
        oEmail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1")
        Try
            oSmtp.UseDefaultCredentials = False
            oSmtp.Credentials = New NetworkCredential(System.Configuration.ConfigurationManager.AppSettings("EMAIL_USERNAME"), System.Configuration.ConfigurationManager.AppSettings("EMAIL_PASSWORD"))
            ServicePointManager.ServerCertificateValidationCallback = Function(ByVal s As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) True
            oSmtp.Send(oEmail)
        Catch ex As Exception
            Throw ex
        Finally
            oEmail.Dispose()
        End Try
    End Sub
End Class

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by Refactoring Essentials.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================

