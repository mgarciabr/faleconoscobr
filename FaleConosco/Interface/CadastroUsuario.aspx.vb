﻿Imports plAppLibrary.clsCommonFunctions
Imports plAppLibrary.Enumerators

Public Class CadastroUsuario
    Inherits System.Web.UI.Page

    Private mbuUsuario As buLogin.buUsuario
    Private mbuPerfil As buLogin.buPerfil
    Private mbuLog As buServicosGerais.buLog
    Private mbuLoja As buSolicitacao.buLoja
    Private myProfile As New ProfileCommonMakro
    Private mbuAssunto As New buSolicitacao.buAssunto
    Private mbuUsLoja As New buLogin.buUsuarioLoja
    Private mbuUsAssunto As New buLogin.buUsuarioAssunto
    Private mbuAssuntoSub As buSolicitacao.buAssuntoSubassunto
    Private mbuUsSub As buLogin.buUsuarioSub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMensagem.Text = ""
            If IsNothing(Session("Logado")) Then
                Response.Redirect("LoginAdmin.aspx", False)
            Else
                Session("Logado") = Session("Logado")
                If Session("Logado") = 1 Then
                    If myProfile.UserInfo.PerfilID <> enUserPerfil.Master Then
                        Response.Redirect("LoginAdmin.aspx", False) 'só o master pode cadastrar
                    End If
                Else
                    Response.Redirect("LoginAdmin.aspx", False)
                End If
            End If

            If Not IsPostBack Then
                preencherPerfil()
                preencherLoja(New StringBuilder().Append("LOJAREGIAO <> ").Append(enLojaRegiao.Brasil).ToString)
                preencherAssunto()
                'dvLoja.Visible = False
                CType(Page.Master.FindControl("liSol"), HtmlGenericControl).Attributes.Remove("class")
                CType(Page.Master.FindControl("liUsuario"), HtmlGenericControl).Attributes.Add("class", "selecionado")
                CType(Page.Master.FindControl("liRelatorio"), HtmlGenericControl).Attributes.Remove("class")
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "Page_Load de CadastroUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub preencherLoja(ByVal strQuery As String)
        Try
            mbuLoja = New buSolicitacao.buLoja
            Dim dsDados As DataSet = mbuLoja.Query(strQuery, "LOJADESCR")
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    cblLojas.Items.Clear()
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        cblLojas.Items.Add(New ListItem(drRow("LOJADESCR"), drRow("LOJAID")))
                    Next
                    dvLoja.Visible = True
                Else
                    cblLojas.Items.Clear()
                    dvLoja.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuLoja = Nothing
        End Try
    End Sub

    Protected Sub preencherPerfil()
        Dim dsDados As DataSet
        Try
            mbuPerfil = New buLogin.buPerfil
            dsDados = mbuPerfil.Query()
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    ddlPerfil.Items.Clear()
                    ddlPerfil.Items.Add(New ListItem("Selecione", -1))
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        ddlPerfil.Items.Add(New ListItem(drRow("PERFDESCR"), drRow("PERFID")))
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuPerfil = Nothing
        End Try
    End Sub

    Protected Sub preencherAssunto()
        Dim dsDados As DataSet
        Try
            mbuAssunto = New buSolicitacao.buAssunto
            dsDados = mbuAssunto.Query(, "ASSUNTODESCR")
            If Not IsNothing(dsDados) Then
                If dsDados.Tables(0).Rows().Count() > 0 Then
                    cblAssunto.Items.Clear()
                    For Each drRow As DataRow In dsDados.Tables(0).Rows
                        cblAssunto.Items.Add(New ListItem(drRow("ASSUNTODESCR"), drRow("ASSUNTOID")))
                    Next
                    dvAssunto.Visible = True
                Else
                    cblAssunto.Items.Clear()
                    dvAssunto.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbuAssunto = Nothing
        End Try
    End Sub

    Public Sub GravarLog(ByVal strMensagem As String, ByVal strOrigem As String, ByVal enTipoLog As plAppLibrary.Enumerators.enLogStatus)
        Try
            mbuLog = New buServicosGerais.buLog
            mbuLog.Insert(strMensagem, strOrigem, System.DateTime.Now, enTipoLog, 1, enModulo.SolicitarEmprestimo)
        Catch ex As Exception
            'Não travar a aplicação
        Finally
            mbuLog = Nothing
        End Try
    End Sub

    Protected Sub btnIncluir_Click(sender As Object, e As EventArgs) Handles btnIncluir.Click
        Dim strEmail As New StringBuilder
        Dim strNovaSenha = GerarSenha(6)
        Dim intUltimoId As Integer
        Dim dsDados, dsIncluir As DataSet
        Dim myTrans As New plAppLibrary.Transaction
        Dim blnSucesso As Boolean
        Try
            If txtEmail.Text.Trim().Equals("") Or ddlPerfil.SelectedValue = -1 Or (Not lojaSelecionada() And ddlPerfil.SelectedValue = enUserPerfil.Gerente) Then
                lblMensagem.Text = "Preencha todos os campos"
            Else
                If naoExiste(txtEmail.Text.Trim()) Then 'não deixa repetir e-mails
                    mbuUsuario = New buLogin.buUsuario
                    mbuUsLoja = New buLogin.buUsuarioLoja
                    mbuLoja = New buSolicitacao.buLoja
                    mbuUsAssunto = New buLogin.buUsuarioAssunto

                    myTrans.IniciaTransacao()

                    blnSucesso = mbuUsuario.Insert(ddlPerfil.SelectedValue, txtEmail.Text, Encrypt(strNovaSenha), True, System.DateTime.Now.Date, enStatusUsuario.enAtivo, myTrans)

                    intUltimoId = mbuUsuario.UltimoId(, "USID DESC", myTrans)

                    'incluindo relações de loja

                    'If ddlRegiao.SelectedValue = enLojaRegiao.Brasil Then 'brasil todo

                    '    'antes de relacionar com todas as lojas, relacionar com a loja "brasil"
                    '    dsDados = mbuLoja.Query(New StringBuilder().Append("LOJAREGIAO = ").Append(enLojaRegiao.Brasil).ToString, , myTrans)
                    '    blnSucesso = mbuUsLoja.Insert(dsDados.Tables(0).Rows(0).Item("LOJAREGIAO"), intUltimoId, myTrans)

                    '    dsDados = mbuLoja.Query(, "LOJAID", myTrans) 'todas as lojas
                    '    dsIncluir = mbuUsLoja.Query("USID = -1", , myTrans) 'apenas criando o dataset vazio

                    '    For Each drRow As DataRow In dsDados.Tables(0).Rows 'para cada linha de loja, incluir relação loja-usuario
                    '        Dim novaLinha As DataRow = dsIncluir.Tables(0).NewRow()
                    '        novaLinha("LOJAID") = drRow("LOJAID")
                    '        novaLinha("USID") = intUltimoId
                    '        dsIncluir.Tables(0).Rows.Add(novaLinha)
                    '    Next

                    '    blnSucesso = mbuUsLoja.UpdateBatch(dsIncluir, myTrans)

                    'Else
                    '    For Each item As ListItem In cblLojas.Items
                    '        'pra cada loja selecionada, inserir em USUARIOLOJA
                    '        If item.Selected Then
                    '            blnSucesso = mbuUsLoja.Insert(item.Value, intUltimoId, myTrans)
                    '        End If
                    '    Next
                    'End If

                    For Each item As ListItem In cblLojas.Items
                        'pra cada loja selecionada, inserir em USUARIOLOJA
                        If item.Selected Then
                            blnSucesso = mbuUsLoja.Insert(item.Value, intUltimoId, myTrans)
                        End If
                    Next

                    'incluindo relações de assunto
                    If ddlPerfil.SelectedValue = enUserPerfil.Gerente Then
                        If assuntoSelecionado() Then
                            For Each item As ListItem In cblAssunto.Items
                                'pra cada assunto selecionado, inserir em USUARIOASSUNTO
                                If item.Selected Then
                                    blnSucesso = mbuUsAssunto.Insert(intUltimoId, item.Value, myTrans)
                                End If
                            Next
                        Else
                            myTrans.FinalizaTransacao(enTransactionStatus.entsFailed)
                            lblMensagem.Text = "Preencha todos os campos"
                        End If

                        'myTrans.FinalizaTransacao(enTransactionStatus.entsSuccess)

                        'Else 'master não tem relação com assunto

                        'myTrans.FinalizaTransacao(enTransactionStatus.entsSuccess)

                    End If

                    'incluindo relações de subassunto
                    mbuUsSub = New buLogin.buUsuarioSub
                    If ddlPerfil.SelectedValue = enUserPerfil.Gerente Then
                        If subassuntoSelecionado() Then
                            For Each item As ListItem In cblSubassunto.Items
                                'pra cada assunto selecionado, inserir em USUARIOASSUNTO
                                If item.Selected Then
                                    blnSucesso = mbuUsSub.Insert(intUltimoId, item.Value, myTrans)
                                End If
                            Next
                        Else
                            myTrans.FinalizaTransacao(enTransactionStatus.entsFailed)
                            lblMensagem.Text = "Preencha todos os campos"
                        End If

                        myTrans.FinalizaTransacao(enTransactionStatus.entsSuccess)

                    Else 'master não tem relação com subassunto

                        myTrans.FinalizaTransacao(enTransactionStatus.entsSuccess)

                    End If

                    If blnSucesso Then
                        'e-mail para o novo usuário informando a senha
                        strEmail.Append("Olá,<br/>Sua senha provisória do Fale Conosco é:<br/><strong>")
                        strEmail.Append(strNovaSenha).Append("</strong><br/><br/>Ao logar, você será redirecionado(a) para alterá-la.<br/><br/>")
                        strEmail.Append("Atenciosamente,<br/>Equipe Makro")

                        'Tentar enviar email
                        Try
                            plAppLibrary.clsCommonFunctions.EnviarEmail(strEmail.ToString, txtEmail.Text.Trim, txtEmail.Text.Trim, "Senha de usuário - Makro")
                        Catch ex As Exception
                            'Não pode travar a aplicação se ocorrer algum erro ao enviar email
                        End Try

                        PopupMensagem.Mensagem = "Usuário cadastrado com sucesso"
                        PopupMensagem.ShowPopup()
                    Else
                        PopupMensagem.Mensagem = "Ocorreu um erro inesperado.\nTente novamente."
                        PopupMensagem.ShowPopup()
                    End If

                Else 'naoExiste()
                    PopupMensagem.Mensagem = "Usuário já existente"
                    PopupMensagem.ShowPopup()
                End If
            End If 'todos os campos
        Catch ex As Exception
            GravarLog(ex.Message, "btnIncluir_Click de CadastroUsuario.aspx", enLogStatus.enlsError)
            myTrans.FinalizaTransacao(enTransactionStatus.entsFailed)
        Finally
            mbuUsuario = Nothing
            mbuUsLoja = Nothing
            mbuLoja = Nothing
            mbuUsAssunto = Nothing
        End Try
    End Sub

    Function validaEmail(ByVal email As String) As Boolean
        Dim emailRegex As New System.Text.RegularExpressions.Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        Dim emailMatch As System.Text.RegularExpressions.Match = emailRegex.Match(email)
        Return emailMatch.Success
    End Function

    Protected Sub PopupMensagem_OK() Handles PopupMensagem.OK
        Try
            Response.Redirect("ConsultaUsuario.aspx")
        Catch ex As Exception
            GravarLog(ex.Message, "PopupMensagem_OK", enLogStatus.enlsFailed)
        End Try
    End Sub

    Protected Sub cbxTodasLojas_CheckedChanged(sender As Object, e As EventArgs) Handles cbxTodasLojas.CheckedChanged
        Try
            If cbxTodasLojas.Checked Then
                For Each item As ListItem In cblLojas.Items
                    item.Selected = True
                Next
                cbxTodasLojas.Text = "Limpar todas"
            Else
                For Each item As ListItem In cblLojas.Items
                    item.Selected = False
                Next
                cbxTodasLojas.Text = "Selecionar todas"
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "cbxTodasLojas_CheckedChanged de CadastroUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    'Protected Sub ddlRegiao_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRegiao.SelectedIndexChanged
    '    Dim strbAux As New StringBuilder
    '    Dim dsDados As DataSet
    '    Try
    '        If Not ddlRegiao.SelectedValue.Equals("-1") And Not ddlRegiao.SelectedValue = enLojaRegiao.Brasil Then
    '            preencherLoja(strbAux.Append("LOJAREGIAO = ").Append(ddlRegiao.SelectedValue).ToString)
    '        Else
    '            cblLojas.Items.Clear()
    '            dvLoja.Visible = False
    '        End If
    '    Catch ex As Exception
    '        GravarLog(ex.Message, "ddlAssunto_SelectedIndexChanged de CadastroUsuario.aspx", enLogStatus.enlsError)
    '    Finally
    '        dsDados = Nothing
    '    End Try
    'End Sub

    'vê se usuário escolheu pelo menos uma loja
    Protected Function lojaSelecionada() As Boolean
        Try
            If dvLoja.Visible Then
                For Each item As ListItem In cblLojas.Items
                    If item.Selected Then
                        Return True
                    End If
                Next
            Else
                If cbxTodasLojas.Checked Then
                    Return True
                End If
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Function assuntoSelecionado() As Boolean
        Try
            If dvAssunto.Visible Then
                For Each item As ListItem In cblAssunto.Items
                    If item.Selected Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Function subassuntoSelecionado() As Boolean
        Try
            If dvSubassunto.Visible Then
                For Each item As ListItem In cblSubassunto.Items
                    If item.Selected Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub cbxTodosAssuntos_CheckedChanged(sender As Object, e As EventArgs) Handles cbxTodosAssuntos.CheckedChanged
        Dim dsDados As New DataSet
        Dim strbAux As New StringBuilder
        Dim visivel As Boolean = False

        Try
            If cbxTodosAssuntos.Checked Then
                For Each item As ListItem In cblAssunto.Items
                    item.Selected = True
                Next
                cbxTodosAssuntos.Text = "Limpar todos"
                mbuAssuntoSub = New buSolicitacao.buAssuntoSubassunto

                For Each item As ListItem In cblAssunto.Items
                    If item.Selected Then
                        dsDados = mbuAssuntoSub.QueryAssunto(strbAux.Clear.Append("ASSUNTOSUBASSUNTO.ASSUNTOID = ").Append(item.Value).ToString)
                        If dsDados.Tables(0).Rows.Count > 0 Then
                            For Each drRow As DataRow In dsDados.Tables(0).Rows
                                cblSubassunto.Items.Add(New ListItem(strbAux.Clear.Append(drRow("ASSUNTODESCR")).Append(" - ").Append(drRow("SBADESCR")).ToString, drRow("ASSUBID")))
                                visivel = True
                            Next
                        End If
                    End If
                Next
                dvSubassunto.Visible = visivel

            Else
                'Limpa assunto
                For Each item As ListItem In cblAssunto.Items
                    item.Selected = False

                Next
                'Limpa SubAssunto
                For Each item As ListItem In cblSubassunto.Items
                    item.Selected = False

                Next
                dvSubassunto.Visible = False

                cbxTodosAssuntos.Text = "Selecionar todos"
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "cbxTodasLojas_CheckedChanged de CadastroUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Private Function naoExiste(ByVal strEmail As String) As Boolean
        Dim dsDados As New DataSet
        Try
            mbuUsuario = New buLogin.buUsuario
            dsDados = mbuUsuario.Query(New StringBuilder().Append("USEMAIL = '").Append(strEmail).Append("' AND USSTATUS = 1").ToString)
            If dsDados.Tables(0).Rows.Count = 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Protected Sub ddlPerfil_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPerfil.SelectedIndexChanged
        Try
            If ddlPerfil.SelectedValue = enUserPerfil.Master Then
                'ddlRegiao.SelectedValue = enLojaRegiao.Brasil
                'ddlRegiao.Enabled = False
                dvAssunto.Visible = False
                dvLoja.Visible = False
                dvSubassunto.Visible = False
                fsAssunto.Visible = False
                fsSubAssunto.Visible = False
                fsLoja.Visible = False
            Else
                'ddlRegiao.SelectedValue = -1
                'ddlRegiao.Enabled = True
                dvAssunto.Visible = True
                dvLoja.Visible = True
                dvSubassunto.Visible = True
                fsAssunto.Visible = True
                fsSubAssunto.Visible = True
                fsLoja.Visible = True
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "ddlPerfil_SelectedIndexChanged de CadastroUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    Protected Sub cblAssunto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cblAssunto.SelectedIndexChanged
        Dim dsDados As New DataSet
        Dim visivel As Boolean = False
        Dim strbAux As New StringBuilder

        Try
            mbuAssuntoSub = New buSolicitacao.buAssuntoSubassunto

            'cada vez que muda, carrega todos os subassuntos novamente
            cblSubassunto.Items.Clear()

            For Each item As ListItem In cblAssunto.Items
                If item.Selected Then
                    dsDados = mbuAssuntoSub.QueryAssunto(strbAux.Clear.Append("ASSUNTOSUBASSUNTO.ASSUNTOID = ").Append(item.Value).ToString)
                    If dsDados.Tables(0).Rows.Count > 0 Then
                        For Each drRow As DataRow In dsDados.Tables(0).Rows
                            cblSubassunto.Items.Add(New ListItem(strbAux.Clear.Append(drRow("ASSUNTODESCR")).Append(" - ").Append(drRow("SBADESCR")).ToString, drRow("ASSUBID")))
                            visivel = True
                        Next
                    End If
                End If
            Next
            dvSubassunto.Visible = visivel
        Catch ex As Exception
            GravarLog(ex.Message, "cblAssunto_SelectedIndexChanged de CadastroUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub

    ''' <summary>
    ''' Evento de click no botão que seleciona todos os assuntos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Implementado por Eduardo Junque em 16/11/2014</remarks>
    Private Sub cbxTodosSubassuntos_CheckedChanged(sender As Object, e As EventArgs) Handles cbxTodosSubassuntos.CheckedChanged
        Dim dsDados As New DataSet
        Dim strbAux As New StringBuilder
        Dim visivel As Boolean = False

        Try
            If cbxTodosSubassuntos.Checked Then
                For Each item As ListItem In cblSubassunto.Items
                    item.Selected = True
                Next
                cbxTodosSubassuntos.Text = "Limpar todos"
                mbuAssuntoSub = New buSolicitacao.buAssuntoSubassunto

            Else
                'Limpa SubAssunto
                For Each item As ListItem In cblSubassunto.Items
                    item.Selected = False

                Next
            
                cbxTodosSubassuntos.Text = "Selecionar todos"
            End If
        Catch ex As Exception
            GravarLog(ex.Message, "cbxTodosSubAssuntos_CheckedChanged de CadastroUsuario.aspx", enLogStatus.enlsError)
        End Try
    End Sub
End Class