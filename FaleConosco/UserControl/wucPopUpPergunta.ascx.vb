﻿Public Class wucPopUpPergunta
    Inherits System.Web.UI.UserControl

    Public Event Excluir()

    ''' <summary>
    ''' Função que Exibe o Popup na tela.
    ''' </summary>
    Public Sub ShowPopup()
        Try
            popupPergunta.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Excluir
    ''' </summary>
    Protected Sub btnSim_Click(sender As Object, e As System.EventArgs) Handles btnSim.Click
        Try
            RaiseEvent Excluir()
            HidePopup()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Não excluir
    ''' </summary>
    Protected Sub btnNao_Click(sender As Object, e As System.EventArgs) Handles btnNao.Click
        Try
            HidePopup()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    ''' <summary>
    ''' Função que Esconde o Popup.
    ''' </summary>
    Public Sub HidePopup()
        Try
            popupPergunta.Hide()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class