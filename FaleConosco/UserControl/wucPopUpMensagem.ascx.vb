﻿Public Class wucPopUpMensagem
    Inherits System.Web.UI.UserControl

    Private Members As IPostBackEventHandler
    Private mstrMensagem As String
    Private mstrTitulo As String
    Private mblnShowOK As Boolean
    Private mblnValidacao As Boolean
    Public Event OK()

    'Propriedade para exibir o botão OK
    Public Property MensagemValidacao() As Boolean
        Get
            If Not IsNothing(Session("msgValidacao")) Then
                Return Session("msgValidacao")
            Else
                Return mblnValidacao
            End If
        End Get
        Set(ByVal value As Boolean)
            mblnValidacao = value
            Session("msgValidacao") = value
        End Set
    End Property
    'Propriedade para exibir o botão OK
    Public Property ExibirbtnOK() As Boolean
        Get
            Return mblnShowOK
        End Get
        Set(ByVal value As Boolean)
            mblnShowOK = value
        End Set
    End Property

    Public Property Mensagem() As String
        Get
            Return mstrMensagem
        End Get
        Set(ByVal value As String)
            mstrMensagem = value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Função que Exibe o Popup na tela.
    ''' </summary>
    Public Sub ShowPopup(Optional ByVal blnShowMsgErro As Boolean = True)
        Try
            ExibirDados(blnShowMsgErro)
            'pnlPopup.Attributes.Add("display", "block")
            mpeMensagem.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ExibirDados(Optional ByVal blnShowMsgErro As Boolean = True)
        Dim myMSG As Text.StringBuilder
        Try
            'Exibindo a mensagem.
            myMSG = New Text.StringBuilder
            myMSG.Append(mstrMensagem)
            lblMensagem.Text = myMSG.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Função que Esconde o Popup.
    ''' </summary>
    Public Sub HidePopup()
        Try
            'pnlPopup.Attributes.Add("display", "none")
            mpeMensagem.Hide()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Protected Sub btnFechar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFechar.Click
        Try
            RaiseEvent OK()
            HidePopup()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class