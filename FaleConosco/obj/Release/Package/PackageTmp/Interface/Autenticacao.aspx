﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Autenticacao.aspx.vb" Inherits="FaleConosco.Autenticacao" %>

<html>
<head>

    <title>Makro - Fale Conosco</title>

    <!-- Fornece compatilidade para o IE-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- css -->
    <link href="../Style/reset.css" rel="stylesheet" />
    <link href="../Style/style.css" rel="stylesheet" />
    <link href="../Style/login.css" rel="stylesheet" />

    <!-- js -->
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var alturaJanela = $(window).height();
            $('#form1').css('height', alturaJanela);

            // Aplicar altura no form
            $(window).mousemove(function () {
                $('#form1').css('height', alturaJanela);
            });

        });
    </script>

</head>
<body>
    <form runat="server" id="form1" class="form-body">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="wrapper">
            <div class="main">
                <div class="content login">
                    <center>
                    <div class="logo">
                        <img src="../Images/_comum/logo.png" alt="" />
                    </div>
                        </center>
                    <div class="box box-login">
                        <h1 style="width: 63%;">Login efetuado com sucesso!</h1>
                    </div>
                </div>
                <!-- div content -->
            </div>
            <!-- div main -->
        </div>
        <!-- div wrapper -->
    </form>
</body>
</html>