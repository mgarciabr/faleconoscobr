﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Usuario.aspx.vb" Inherits="FaleConosco.Usuario"  MasterPageFile="~/FaleConosco.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">

    <!-- css -->
    <link href="../Style/alteracaoSenha.css" rel="stylesheet" />

    <!-- js -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cph2" runat="Server">

    <div class="baixo">
        <div class="box" style="width: 50%; margin: 0 auto;">
            <h2>Selecione a ação</h2>
            <div class="ir-voltar">
                <asp:LinkButton ID="btnConsulta" runat="server" class="botao btnEsq" Style="width:45%" PostBackUrl="~/Interface/ConsultaUsuario.aspx">Consulta de Usuário</asp:LinkButton>
                <asp:LinkButton ID="btnAlterar" runat="server" class="botao btnDir" Style="width:45%" PostBackUrl="~/Interface/AlteracaoSenha.aspx">Alteração senha</asp:LinkButton>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var tamanhoJanela = $(window).width() - 169;
            $('.content').css('width', tamanhoJanela);
        });
</script>
</asp:Content>