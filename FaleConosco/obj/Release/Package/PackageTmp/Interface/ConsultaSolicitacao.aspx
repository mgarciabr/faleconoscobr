﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ConsultaSolicitacao.aspx.vb" Inherits="FaleConosco.ConsultaSolicitacao" MasterPageFile="../FaleConosco.Master" %>

<%@ Register Src="~/UserControl/wucPopupMensagem.ascx" TagPrefix="uc1" TagName="wucPopupMensagem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">

    <!-- css -->
    <link href="../Style/consultaSolicitacao.css" rel="stylesheet" />

    <!-- js -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="../Scripts/FormataMascara.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph2" runat="Server">
    <div class="baixo" style="width: 90%">
        <div class="box">
            <h2>Consulta de Solicitação</h2>
            <div class="validacao">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
            </div>
            <div runat="server" id="pnlConsulta">
                <asp:Panel ID="Panel1" runat="server" GroupingText="Filtro">
                    <div class="protocolo">
                        <h4>Protocolo</h4>
                        <asp:TextBox ID="txtProtocolo" runat="server" MaxLength="50" TabIndex="1"></asp:TextBox>
                    </div>

                    <div class="passaporte">
                        <h4>Passaporte</h4>
                        <asp:TextBox ID="txtPassaporte" runat="server" MaxLength="13" TabIndex="2"></asp:TextBox>
                    </div>

                    <div class="data">
                        <div class="inicio">
                            <h4>De</h4>
                            <asp:TextBox ID="txtInicio" runat="server" MaxLength="10" TabIndex="1" onkeydown="formataData(this, event)"></asp:TextBox>
                        </div>
                        <div class="fim">
                            <h4>Até</h4>
                            <asp:TextBox ID="txtFim" runat="server" MaxLength="10" TabIndex="2" onkeydown="formataData(this, event)"></asp:TextBox>
                        </div>
                    </div>

                    <div class="situacao">
                        <h4>Situação</h4>
                        <asp:DropDownList ID="ddlSituacao" runat="server" TabIndex="4">
                            <asp:ListItem Value="">Todas</asp:ListItem>
                            <asp:ListItem Value="1">Em aberto</asp:ListItem>
                            <asp:ListItem Value="2">Respondida</asp:ListItem>
                            <asp:ListItem Value="3">Encerrada</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="ir-voltar">
                        <asp:LinkButton ID="btnConsultar" runat="server" class="botao btnConsultar center" Style="width: 120px; margin-bottom: -10px; float: right;">Consultar</asp:LinkButton>
                    </div>
                </asp:Panel>

                <h4 style="margin-top: 15px;">Solicitações<asp:Literal ID="litContador" runat="server"></asp:Literal>:</h4>
                <div>
                </div>
            </div>

            <%--            <asp:UpdatePanel ID="updPanel" runat="server">
                <ContentTemplate>--%>
            <asp:GridView ID="gvSolicitacao" runat="server" AllowPaging="True"
                AutoGenerateColumns="False" EmptyDataText="Nenhuma solicitação cadastrada no momento" DataKeyNames="SOLID" CellPadding="0" class="grid">
                <PagerStyle CssClass="pagination" HorizontalAlign="Center"></PagerStyle>
                <AlternatingRowStyle BackColor="#EEEEEE" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" ButtonType="Image" SelectImageUrl="../Images/_comum/selecionar.png" ControlStyle-CssClass="selecionar" ItemStyle-CssClass="selecionartd" />
                    <asp:BoundField DataField="SOLPROTOCOLO" HeaderText="Protocolo" />
                    <%--<asp:BoundField DataField="SOLNUMEROPASSAPORTE" HeaderText="Passaporte" />--%>
                    <asp:BoundField DataField="SOLDTCADASTRO" HeaderText="Data" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="SOLEMAIL" HeaderText="E-mail" />
                    <asp:BoundField DataField="LOJAID" HeaderText="Loja" />
                    <asp:BoundField DataField="ASSUNTOID" HeaderText="Assunto" />
                    <asp:BoundField DataField="ASSUBID" HeaderText="Subassunto" />
                    <%--<asp:BoundField DataField="SOLMSGDESCR" HeaderText="Mensagem" />--%>
                    <asp:BoundField DataField="STATUSDESCR" HeaderText="Status" />
                    <asp:BoundField DataField="USEMAIL" HeaderText="E-mail Atendente" />
                </Columns>
            </asp:GridView>

            <div runat="server" id="pnlConversa" style="margin-top: 30px;">
            </div>
            <div class="mensagem" runat="server" style="height: auto; margin: 40px 0;" id="dvAuxMsg" visible="false">
                <asp:Label ID="lblAux" runat="server" Style="color: #000; font-size: 16px;"></asp:Label>
            </div>
            <div class="mensagem" runat="server" id="pnlNovaMsg">
                <div class='dir'><span class='remetente' style='color: #B20018'>Resposta</span></div>
                <asp:TextBox class="textoMsg" ID="txtNovaMensagem" runat="server" Style="height: 100px; resize: none;" TextMode="MultiLine"></asp:TextBox>
            </div>
            <asp:Panel ID="pnlCaptura" runat="server" Visible="False">
                <div class="ir-voltar" style="margin: 25px 190px;">
                    <asp:LinkButton ID="btnCapturar" runat="server" class="botao btnDir" Style="margin: 0 4% 0 -1%;">Capturar</asp:LinkButton>
                    <asp:LinkButton ID="btnRetornar" runat="server" class="botao btnEsq">Retornar</asp:LinkButton>
                </div>
            </asp:Panel>
            <div class="mensagem" style="margin: 0; height: 30px">
                <asp:LinkButton ID="btnEnviar" runat="server" class="botao btnEnviar" Style="margin: 0 15px 0 0; width: 90px;" Visible="False">Enviar</asp:LinkButton>
                <asp:LinkButton ID="btnVoltar" runat="server" class="botao btnEnviar" Style="margin: 0 15px 0 0; width: 110px;" Visible="False">Retornar</asp:LinkButton>
                <asp:LinkButton ID="btnEncerrar" runat="server" class="botao btnEnviar" Style="margin: 0 15px 0 0; width: 110px;" Visible="False">Encerrar</asp:LinkButton>
                <asp:LinkButton ID="btnCadastrar" runat="server" class="botao btnCadastrar" Style="margin: 0 15px 0 0; width: 170px;" PostBackUrl="~/Interface/CadastroSolicitacao.aspx" CausesValidation="False" Visible="false">Cadastrar nova</asp:LinkButton>
            </div>
            <%--                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnCapturar" />
                    <asp:PostBackTrigger ControlID="btnEncerrar" />
                    <asp:PostBackTrigger ControlID="gvSolicitacao" />
                </Triggers>
            </asp:UpdatePanel>--%>
        </div>
    </div>
    <uc1:wucPopupMensagem runat="server" ID="PopupMensagem" />

    <script>
        $(document).ready(function () {
            var tamanhoJanela = $(window).width() - 169;
            $('.content').css('width', tamanhoJanela);
        });
    </script>
</asp:Content>
