﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EsquecimentoSenha.aspx.vb" Inherits="FaleConosco.EsquecimentoSenha" MasterPageFile="../FaleConosco.Master" %>

<%@ Register Src="~/UserControl/wucPopupMensagem.ascx" TagPrefix="uc1" TagName="wucPopupMensagem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">

    <!-- css -->
    <link href="../Style/alteracaoSenha.css" rel="stylesheet" />

    <!-- js -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph2" runat="Server">
    <div class="baixo">
        <div class="box" style="width: 60%; margin-left: 20%;">
            <h2>Esqueci a Senha</h2>
            <div class="validacao">
                <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label></div>
            <br />
            <div class="email">
                <h4>E-mail</h4>
                <asp:TextBox ID="txtEmail" runat="server" MaxLength="150"></asp:TextBox>
            </div>

            <div class="ir-voltar">
                <asp:LinkButton ID="btnEnviar" runat="server" class="botao btnEsq"  Style="width: 200px; margin-right: 15px">Enviar token de acesso</asp:LinkButton>
                <asp:LinkButton ID="btnRetornar" runat="server" class="botao btnDir" PostBackUrl="~/Interface/LoginAdmin.aspx"  Style="width: 110px;margin: 0;">Retornar</asp:LinkButton>
            </div>
        </div>
    </div>
    <uc1:wucPopupMensagem runat="server" ID="PopupMensagem" />

    <script>
        $(document).ready(function () {
            var tamanhoJanela = $(window).width() - 169;
            $('.content').css('width', tamanhoJanela);
        });
    </script>
</asp:Content>
