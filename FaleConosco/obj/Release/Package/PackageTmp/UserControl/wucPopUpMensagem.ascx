﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucPopUpMensagem.ascx.vb" Inherits="FaleConosco.wucPopUpMensagem" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

    <!-- css -->
    <link href="../Style/reset.css" rel="stylesheet" />
    <link href="../Style/style.css" rel="stylesheet" />

<style>
    .modalBackground {
        background-color: Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }

    .pnlMensagem 
    {
        border-top: 3px solid #df001d; 
        border-bottom: 3px solid #df001d; 
        overflow: hidden; 
        margin: 0 auto; 
        -moz-box-sizing: border-box; 
        -webkit-box-sizing: border-box;
         box-sizing: border-box; 
         height: auto;
        -webkit-box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.86);
        -moz-box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.86);
        box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.86);
        min-width: 430px;
        background: #fff;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        -ms-border-radius: 5px;
        -o-border-radius: 5px;
        border-radius: 5px;
    }

    .botaoPopup
    { 
        border-radius: 5px;
        padding: 10px 12px;
        border: 0px none;
        font-size: 16px;
        font-weight: normal;
        line-height: 1;
        color: #fff;
        text-decoration: none;
        margin: 7% 0 3%;
        position: relative; 
        background: #df001d;
        cursor: pointer;
    }

    .titulo 
    { 
        text-align: left;
        background: #df001d;
    }

        .titulo h2 
        {
            font-family: Verdana; 
            font-size: 15px;
            font-weight:normal;
            color: #fff;
            padding: 5px 15px;
        }
</style>

<cc1:ModalPopupExtender ID="mpeMensagem" runat="server" TargetControlID="lblPopupFileUpLoad"
    PopupControlID="pnlPopup" BackgroundCssClass="modalBackground">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopup" runat="server" CssClass="pnlMensagem" style="display: none;">
    <div>
        <center>
            <div>
                <div class="titulo">
                    <h2>Makro - Fale Conosco</h2>
                </div>
                <div>
                    <p style="margin-top: 22px">
                        <asp:Label ID="lblMensagem" runat="server" style="font-family: Verdana; font-size: 12px" Text="Operação inválida. Por favor tente novamente."></asp:Label>
                    </p>
                </div>
                <div>
                    <asp:Button ID="btnFechar" runat="server" Text="Fechar" CssClass="botaoPopup"/>
                </div>
            </div>
        </center>
    </div>
</asp:Panel>
<asp:Label ID="lblPopupFileUpLoad" runat="server"></asp:Label>