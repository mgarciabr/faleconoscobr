﻿Imports System
Imports Microsoft.VisualBasic

<Serializable()> _
Public Class PagesCollection

    Private mcolPagesNavigation() As PageNavigation
    'Private mintCount As Integer

    ''' <summary>
    ''' Obtém um Item da coleção de Configurações para Navegação de Página.
    ''' Recebe como parâmetro o nome da Página referida.
    ''' </summary>
    Default Public ReadOnly Property Item(ByVal strName As String) As PageNavigation
        Get
            Dim MyPageNav As PageNavigation
            Dim blnAchou As Boolean

            'Colocar código para pesquisar a página solicitada na coleção
            If Not mcolPagesNavigation Is Nothing Then
                For Idx As Integer = 0 To mcolPagesNavigation.GetUpperBound(0) - 1
                    If mcolPagesNavigation(Idx) Is Nothing Then
                        'Se for Nothing, considera como não encontrado.
                    Else
                        If mcolPagesNavigation(Idx).PageName IsNot Nothing Then
                            If mcolPagesNavigation(Idx).PageName.Trim.ToUpper = strName.Trim.ToUpper Then
                                MyPageNav = mcolPagesNavigation(Idx)
                                blnAchou = True
                                Exit For
                            End If
                        End If
                    End If
                Next
            End If

            'Verifica se o item foi achado ou não.
            If Not blnAchou Then
                MyPageNav = Nothing
            End If

            Return MyPageNav
        End Get
    End Property

    ''' <summary>
    ''' Obtém um Item da coleção de Configurações para Navegação de Página.
    ''' Recebe como parâmetro o nome da Página referida.
    ''' </summary>
    Public ReadOnly Property ItemPosition(ByVal intIndice As Integer) As PageNavigation
        Get
            Dim MyPageNav As PageNavigation
            Dim blnAchou As Boolean
            Dim iMaxBound As Integer

            'Colocar código para pesquisar a página solicitada na coleção
            If Not mcolPagesNavigation Is Nothing Then
                iMaxBound = mcolPagesNavigation.GetUpperBound(0)
                If intIndice <= iMaxBound Then
                    MyPageNav = mcolPagesNavigation(intIndice)
                    blnAchou = True
                End If
            End If

            'Verifica se o item foi achado ou não.
            If Not blnAchou Then
                MyPageNav = Nothing
            End If

            Return MyPageNav
        End Get
    End Property

    ' ''' <summary>
    ' ''' Método para adicionar itens na coleção de Configurações para Navegação de Página.
    ' ''' </summary>
    ' ''' <param name="PageNav"></param>
    'Public Sub AddItem(ByVal PageNav As PageNavigation)
    '    'Acrescenta 1 no contador
    '    mintCount = mintCount + 1

    '    'Redimensiona o Array para comportar o novo item adicionado
    '    If Not mcolPagesNavigation Is Nothing Then
    '        ReDim Preserve mcolPagesNavigation(0 To mcolPagesNavigation.GetUpperBound(0) + 1)
    '    Else
    '        ReDim Preserve mcolPagesNavigation(0 To 0)
    '    End If

    '    'Coloca o novo item no Array
    '    mcolPagesNavigation(mintCount - 1) = PageNav
    'End Sub

    ''' <summary>
    ''' Método para adicionar itens na coleção de Configurações para Navegação de Página.
    ''' </summary>
    ''' <param name="PageNav"></param>
    Public Sub AddItem(ByVal PageNav As PageNavigation)
        'Acrescenta 1 no contador
        'mintCount = mintCount + 1

        'Redimensiona o Array para comportar o novo item adicionado
        If Not mcolPagesNavigation Is Nothing Then
            ReDim Preserve mcolPagesNavigation(0 To mcolPagesNavigation.GetUpperBound(0) + 1)
            'Coloca o novo item no Array
            mcolPagesNavigation(mcolPagesNavigation.GetUpperBound(0) - 1) = PageNav
        Else
            ReDim Preserve mcolPagesNavigation(0 To 0)
            'Coloca o novo item no Array
            mcolPagesNavigation(0) = PageNav
        End If

    End Sub


    ''' <summary>
    ''' Retorna a quantidade de itens na coleção de Configurações para Navegação de Página.
    ''' </summary>
    Public ReadOnly Property Count() As Integer
        Get
            If Not mcolPagesNavigation Is Nothing Then
                Return mcolPagesNavigation.GetUpperBound(0)
            Else
                Return 0
            End If
        End Get
    End Property

    ''' <summary>
    ''' Método para remover um item da coleção de Configurações para Navegação de Página.
    ''' </summary>
    ''' <param name="strPage"></param>
    Public Sub RemoveItem(ByVal strPage As String)
        Dim colTemp() As PageNavigation
        Dim Idx2 As Integer

        'O Array temporário terá uma posição a menos, pq estamos removendo um item.
        ReDim Preserve colTemp(0 To mcolPagesNavigation.GetUpperBound(0) - 1)

        'Acertando o Contador de itens (que deve ter sua soma subtraida em 1 agora)
        'mintCount = mcolPagesNavigation.GetUpperBound(0)

        If Not mcolPagesNavigation Is Nothing Then
            'Código para adicionar os itens em um vetor temporário
            For Idx As Integer = 0 To mcolPagesNavigation.GetUpperBound(0)
                If mcolPagesNavigation(Idx).PageName.Trim.ToUpper <> strPage.Trim.ToUpper Then
                    colTemp(Idx2) = mcolPagesNavigation(Idx)
                    Idx2 += 1
                End If
            Next
        End If

        'Atribuindo o Array temporário ao Modular.
        mcolPagesNavigation = colTemp
        'mintCount = mintCount - 1

    End Sub

    ' ''' <summary>
    ' ''' Método para remover um item da coleção de Configurações para Navegação de Página.
    ' ''' </summary>
    'Public Sub RemoveItem(ByVal strName As Double)
    '    Dim colTemp() As PageNavigation
    '    Dim Idx2 As Integer

    '    'O Array temporário terá uma posição a menos, pq estamos removendo um item.
    '    ReDim Preserve colTemp(0 To mcolPagesNavigation.GetUpperBound(0) - 1)

    '    'Acertando o Contador de itens (que deve ter sua soma subtraida em 1 agora)
    '    mintCount = mcolPagesNavigation.GetUpperBound(0)

    '    If Not mcolPagesNavigation Is Nothing Then
    '        'Código para adicionar os itens em um vetor temporário
    '        For Idx As Integer = 0 To mcolPagesNavigation.GetUpperBound(0)
    '            If mcolPagesNavigation(Idx).PageName <> strName Then
    '                colTemp(Idx2) = mcolPagesNavigation(Idx)
    '                Idx2 += 1
    '            End If
    '        Next
    '    End If

    '    'Atribuindo o Array temporário ao Modular.
    '    mcolPagesNavigation = colTemp
    '    'mintCount = mintCount - 1

    'End Sub

    ''' <summary>
    ''' Método para remover um item da coleção de Configurações para Navegação de Página.
    ''' </summary>
    Public Sub RemoveItem(ByVal strName As Double)
        Dim colTemp() As PageNavigation
        Dim Idx2 As Integer
        Dim intTotalItens As Integer

        'Total de itens na Coleção.
        intTotalItens = mcolPagesNavigation.GetUpperBound(0) - 1

        If Not mcolPagesNavigation Is Nothing Then
            'Código para adicionar os itens em um vetor temporário
            For Idx As Integer = 0 To intTotalItens 'mcolPagesNavigation.GetUpperBound(0)
                If mcolPagesNavigation(Idx).PageName <> strName.ToString Then
                    'O Array temporário terá uma posição a menos, pq estamos removendo um item.
                    ReDim Preserve colTemp(0 To Idx)

                    colTemp(Idx2) = mcolPagesNavigation(Idx)
                    Idx2 += 1
                Else
                    'Achou o item que será excluído.

                    'Acertando o Contador de itens (que deve ter sua soma subtraida em 1 agora)
                    'mintCount = mcolPagesNavigation.GetUpperBound(0)
                End If
            Next
        End If

        'Atribuindo o Array temporário ao Modular.
        mcolPagesNavigation = colTemp
        'mintCount = mintCount - 1

    End Sub


End Class