﻿Public Class ProfileCommonMakro
    'Inherits Profile.DefaultProfile
    Inherits System.Web.Profile.ProfileBase

    ''' <summary>
    ''' Acessa as informações do Profile "PageCollection"
    ''' </summary>
    Public Property PagesNavigation() As PagesCollection
        Get
            Return (HttpContext.Current.Profile.GetPropertyValue("PagesCollection"))
        End Get

        Set(ByVal value As PagesCollection)
            HttpContext.Current.Profile.SetPropertyValue("PagesCollection", value)
        End Set
    End Property

    ''' <summary>
    ''' Acessa as informações do Profile "UserInfo"
    ''' </summary>
    Public Property UserInfo() As UserInfo
        Get
            UserInfo = HttpContext.Current.Profile.GetPropertyValue("UserInfo")
        End Get

        Set(ByVal value As UserInfo)
            HttpContext.Current.Profile.SetPropertyValue("UserInfo", value)
        End Set
    End Property

End Class
