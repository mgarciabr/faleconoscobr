﻿Imports System
Imports Microsoft.VisualBasic

<Serializable()> _
Public Class UserInfo

    Private mintID As Integer
    Private mstrNome As String
    Private mstrLogin As String
    Private mstrEmail As String
    Private mintTecID As String
    Private mintPerfilID As Integer
    Private mintLojaID As Integer
    Private mstrIpAddress As String

    ''' <summary>
    ''' Propriedade que armazena o ID do usuário logado
    ''' </summary>
    ''' <versao ID="1.0" Projeto="Action" RF="" RNF="" UC="" Data="02/09/2011" Desenvolvedor="Felipe Caron">
    ''' </versao>
    Public Property ID() As Integer
        Get
            Return mintID
        End Get

        Set(ByVal value As Integer)
            mintID = value
        End Set
    End Property

    ''' <summary>
    ''' Propriedade que armazena o Nome do usuário logado
    ''' </summary>
    ''' <versao ID="1.0" Projeto="Action" RF="" RNF="" UC="" Data="02/09/2011" Desenvolvedor="Felipe Caron">
    ''' </versao>
    Public Property Nome() As String
        Get
            Return mstrNome
        End Get

        Set(ByVal value As String)
            mstrNome = value
        End Set
    End Property

    ''' <summary>
    ''' Propriedade que armazena o id da loja do usuário logado
    ''' </summary>
    Public Property Loja() As String
        Get
            Return mintLojaID
        End Get

        Set(ByVal value As String)
            mintLojaID = value
        End Set
    End Property

    ''' <summary>
    ''' Propriedade que armazena o Login do usuário logado
    ''' </summary>
    ''' <versao ID="1.0" Projeto="Action" RF="" RNF="" UC="" Data="02/09/2011" Desenvolvedor="Felipe Caron">
    ''' </versao>
    Public Property Login() As String
        Get
            Return mstrLogin
        End Get

        Set(ByVal value As String)
            mstrLogin = value
        End Set
    End Property

    ''' <summary>
    ''' Propriedade que armazena o Email do usuário logado
    ''' </summary>
    ''' <versao ID="1.0" Projeto="Action" RF="" RNF="" UC="" Data="02/09/2011" Desenvolvedor="Felipe Caron">
    ''' </versao>
    Public Property Email() As String
        Get
            Return mstrEmail
        End Get

        Set(ByVal value As String)
            mstrEmail = value
        End Set
    End Property

    ''' <summary>
    ''' Propriedade que armazena o Id de Técnico do Funcionário logado
    ''' </summary>
    ''' <versao ID="1.0" Projeto="Action" RF="" RNF="" UC="" Data="18/06/2012" Desenvolvedor="Juliana Lima">
    ''' </versao>
    Public Property tecID() As Integer
        Get
            Return mintTecID
        End Get

        Set(ByVal value As Integer)
            mintTecID = value
        End Set
    End Property

    ''' <summary>
    ''' Método que limpa as informações do perfil
    ''' </summary>
    ''' <versao ID="1.0" Projeto="Economia ICMS v.1.0" RF="" RNF="" UC="" Data="02/09/2011" Desenvolvedor="Felipe Caron">
    ''' </versao>
    Public Sub LimparPerfil()
        Try
            mstrEmail = Nothing
            mstrNome = Nothing
            mstrLogin = Nothing
            mintID = Nothing
            mintTecID = Nothing
            ID = Nothing
            mintPerfilID = Nothing
            mintLojaID = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Propriedade que armazena o Perfil do Usuario logado
    ''' </summary>
    ''' <versao ID="1.0" Projeto="Action" RF="" RNF="" UC="" Data="18/06/2012" Desenvolvedor="Juliana Lima">
    ''' </versao>
    Public Property PerfilID() As String
        Get
            Return mintPerfilID
        End Get

        Set(ByVal value As String)
            mintPerfilID = value
        End Set
    End Property

    ''' <summary>
    ''' Propriedade que armazena o endereço de IP do usuário logado.
    ''' </summary>
    ''' <versao ID="1.0" Projeto="Action" RF="" RNF="" UC="" Data="18/06/2012" Desenvolvedor="Juliana Lima">
    ''' </versao>
    Public Property IP() As String
        Get
            Return mstrIpAddress
        End Get

        Set(ByVal value As String)
            mstrIpAddress = value
        End Set
    End Property
End Class
