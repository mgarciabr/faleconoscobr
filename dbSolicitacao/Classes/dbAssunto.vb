﻿Imports System
Imports plAppLibrary.clsCommonFunctions

''' <summary>
''' Classe para gerenciamento de Regras de Acesso a Dados das operações na tabela ASSUNTO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 14/04/2014 11:46:07</remarks>
Public Class dbAssunto

    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intASSUNTOSTATUS As Integer, Optional ByVal strASSUNTODESCR As String = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO ASSUNTO (ASSUNTOSTATUS, ASSUNTODESCR) ")
            strAux.Append("VALUES (")
            strAux.Append(intASSUNTOSTATUS).Append(", ").Append("'").Append(TrataTextos(strASSUNTODESCR)).Append("'")
            strAux.Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intASSUNTOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM ASSUNTO WHERE ")
            strAux.Append("ASSUNTOID = ").Append(intASSUNTOID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intASSUNTOID As Integer, ByVal intASSUNTOSTATUS As Integer, Optional ByVal strASSUNTODESCR As String = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE ASSUNTO SET ")
            strAux.Append("ASSUNTOSTATUS = ").Append(intASSUNTOSTATUS).Append(", ").Append("ASSUNTODESCR = ").Append("'").Append(TrataTextos(strASSUNTODESCR)).Append("'")
            strAux.Append("WHERE ")
            strAux.Append("ASSUNTOID = ").Append(intASSUNTOID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            strAux = New Text.StringBuilder
            strAux.Append("SELECT ASSUNTOID, ASSUNTOSTATUS, ASSUNTODESCR FROM ASSUNTO WHERE ")
            strAux.Append("ASSUNTOSTATUS = ").Append("-1").Append(" AND ").Append("ASSUNTODESCR = ").Append("''")

            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.UpdateBatch(dsDados, _
                                         strAux.ToString, _
                                         objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:46:07 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT ASSUNTOID, ASSUNTOSTATUS, ASSUNTODESCR FROM ASSUNTO")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTO com inner join em PERFIL
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryPerfil(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT dbo.ASSUNTO.ASSUNTODESCR, dbo.ASSUNTO.ASSUNTOSTATUS, dbo.PERFIL.PERFID, dbo.ASSUNTOPERFIL.ASSUNTOID")
            strAux.Append(" FROM dbo.ASSUNTO INNER JOIN dbo.ASSUNTOPERFIL ON dbo.ASSUNTO.ASSUNTOID = dbo.ASSUNTOPERFIL.ASSUNTOID INNER JOIN")
            strAux.Append(" dbo.PERFIL ON dbo.ASSUNTOPERFIL.PERFILID = dbo.PERFIL.PERFID ")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

End Class