﻿Imports System
Imports plAppLibrary.clsCommonFunctions

''' <summary>
''' Classe para gerenciamento de Regras de Acesso a Dados das operações na tabela SOLICITACAOMENSAGEM
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 14/04/2014 11:40:05</remarks>
Public Class dbSolicitacaoMensagem

    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intSOLID As Integer, ByVal strSOLMSGDESCR As String, ByVal strDTSOLMSGDTCADASTRO As String, Optional ByVal intUSID As Integer = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO SOLICITACAOMENSAGEM (SOLID, SOLMSGDESCR, SOLMSGDTCADASTRO, USID) ")
            strAux.Append("VALUES (")
            'strAux.Append(intSOLID).Append(", ").Append("'").Append(TrataTextos(strSOLMSGDESCR)).Append("'").Append(", ").Append("'").Append(strDTSOLMSGDTCADASTRO).Append("'").Append(", ").Append(intUSID)
            strAux.Append(intSOLID).Append(", ").Append("'").Append(TrataTextos(strSOLMSGDESCR)).Append("'").Append(", ").Append("'").Append(FormataData_HoraSQL(strDTSOLMSGDTCADASTRO)).Append("'").Append(", ").Append(intUSID)
            strAux.Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intSOLMSGID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM SOLICITACAOMENSAGEM WHERE ")
            strAux.Append("SOLMSGID = ").Append(intSOLMSGID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intSOLMSGID As Integer, ByVal intSOLID As Integer, ByVal strSOLMSGDESCR As String, ByVal strDTSOLMSGDTCADASTRO As String, Optional ByVal intUSID As Integer = Nothing, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE SOLICITACAOMENSAGEM SET ")
            strAux.Append("SOLID = ").Append(intSOLID).Append(", ").Append("SOLMSGDESCR = ").Append("'").Append(TrataTextos(strSOLMSGDESCR)).Append("'").Append(", ").Append("SOLMSGDTCADASTRO = ").Append("'").Append(FormataData_HoraSQL(strDTSOLMSGDTCADASTRO)).Append("'").Append(", ").Append("USID = ").Append(intUSID)
            'strAux.Append("SOLID = ").Append(intSOLID).Append(", ").Append("SOLMSGDESCR = ").Append("'").Append(TrataTextos(strSOLMSGDESCR)).Append("'").Append(", ").Append("SOLMSGDTCADASTRO = ").Append("'").Append(strDTSOLMSGDTCADASTRO).Append("'").Append(", ").Append("USID = ").Append(intUSID)
            strAux.Append("WHERE ")
            strAux.Append("SOLMSGID = ").Append(intSOLMSGID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            strAux = New Text.StringBuilder
            strAux.Append("SELECT SOLMSGID, SOLID, SOLMSGDESCR, SOLMSGDTCADASTRO, USID FROM SOLICITACAOMENSAGEM WHERE ")
            strAux.Append("SOLID = ").Append("-1").Append(" AND ").Append("SOLMSGDESCR = ").Append("''").Append(" AND ").Append("SOLMSGDTCADASTRO = ").Append("''").Append(" AND ").Append("USID = ").Append("-1")

            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.UpdateBatch(dsDados, _
                                         strAux.ToString, _
                                         objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAOMENSAGEM.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:40:05 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT SOLMSGID, SOLID, SOLMSGDESCR, SOLMSGDTCADASTRO, USID FROM SOLICITACAOMENSAGEM")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAOMENSAGEM com inner join em SOLICITACAOMENSAGEM e em USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>08/05/2014 - Implementada por Maria Beatriz</remarks>
    'Public Function QueryUsuario(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
    '                      Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
    '    Dim dsDados As Data.DataSet
    '    Dim strAux As Text.StringBuilder

    '    Try
    '        'Definindo parâmetro de consulta.
    '        strAux = New Text.StringBuilder
    '        strAux.Append("SELECT dbo.SOLICITACAO.SOLNOME, dbo.SOLICITACAO.SOLSTATUS, dbo.SOLICITACAOMENSAGEM.*, dbo.USUARIO.PERFID, dbo.USUARIO.USNOME FROM dbo.SOLICITACAOMENSAGEM")
    '        strAux.Append(" INNER JOIN dbo.SOLICITACAO ON dbo.SOLICITACAOMENSAGEM.SOLID = dbo.SOLICITACAO.SOLID LEFT JOIN dbo.USUARIO")
    '        strAux.Append(" ON dbo.SOLICITACAO.USIDCAPTURA = dbo.USUARIO.USID")

    '        'Verificando se foi definido algum filtro pelo usuário.
    '        If strWhere <> "" Then
    '            strAux.Append(" WHERE ").Append(strWhere)
    '        End If

    '        'Verificando se foi definido alguma ordenação pelo usuário.
    '        If strOrderBy <> "" Then
    '            strAux.Append(" ORDER BY ").Append(strOrderBy)
    '        End If

    '        'Passando parâmetro para daAccess.
    '        mdaAccess = New daAccess.MSSQLServerDataAccess
    '        dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
    '        mdaAccess = Nothing

    '        'Retornando dados da consulta.
    '        Return dsDados

    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        'Limpando variáveis.
    '        mdaAccess = Nothing
    '        dsDados = Nothing
    '        strAux = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAOMENSAGEM com inner join em SOLICITACAO e em USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>08/05/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryUsuario(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT * FROM SOLICITACAOMENSAGEM")
            strAux.Append(" INNER JOIN SOLICITACAO ON SOLICITACAO.SOLID = SOLICITACAOMENSAGEM.SOLID")
            strAux.Append(" LEFT JOIN USUARIO ON USUARIO.USID = SOLICITACAOMENSAGEM.USID")
            strAux.Append(" INNER JOIN LOJA ON LOJA.LOJAID = SOLICITACAO.LOJAID")
            strAux.Append(" INNER JOIN ASSUNTO ON ASSUNTO.ASSUNTOID = SOLICITACAO.ASSUNTOID")
            strAux.Append(" INNER JOIN ASSUNTOSUBASSUNTO ON ASSUNTOSUBASSUNTO.ASSUBID = SOLICITACAO.ASSUBID")
            strAux.Append(" INNER JOIN SUBASSUNTO ON SUBASSUNTO.SBAID = ASSUNTOSUBASSUNTO.SBAID")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAOMENSAGEM com inner join em SOLICITACAO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>08/05/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryPrimeiraMsg(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT dbo.SOLICITACAO.SOLID, dbo.SOLICITACAOMENSAGEM.SOLMSGDTCADASTRO AS primMensagem FROM dbo.SOLICITACAOMENSAGEM")
            strAux.Append(" INNER JOIN dbo.SOLICITACAO ON dbo.SOLICITACAO.SOLID = dbo.SOLICITACAOMENSAGEM.SOLID ")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para consulta de quantidade de mensagens
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>13/06/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryCount(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT COUNT(SOLMSGID) AS qtdMsg FROM SOLICITACAOMENSAGEM")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

End Class