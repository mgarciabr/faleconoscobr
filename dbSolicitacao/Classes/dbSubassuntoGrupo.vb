﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Acesso a Dados das operações na tabela SUBASSUNTOGRUPO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 16/05/2014 15:45:02</remarks>
Public Class dbSubassuntoGrupo

    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:02 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intSBAID As Integer, ByVal intGRUPOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO SUBASSUNTOGRUPO (SBAID, GRUPOID) ")
            strAux.Append("VALUES (")
            strAux.Append(intSBAID).Append(", ").Append(intGRUPOID)
            strAux.Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:02 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intSBAID As Integer, ByVal intGRUPOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM SUBASSUNTOGRUPO WHERE ")
            strAux.Append("SBAID = ").Append(intSBAID).Append(" AND ").Append("GRUPOID = ").Append(intGRUPOID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:02 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intSBAID As Integer, ByVal intGRUPOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE SUBASSUNTOGRUPO SET ")
            strAux.Append("SBAID = ").Append(intSBAID).Append(", ").Append("GRUPOID = ").Append(intGRUPOID)
            strAux.Append("WHERE ")
            strAux.Append("SBAID = ").Append(intSBAID).Append(" AND ").Append("GRUPOID = ").Append(intGRUPOID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:02 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            strAux = New Text.StringBuilder
            strAux.Append("SELECT SBAID, GRUPOID FROM SUBASSUNTOGRUPO WHERE ")
            strAux.Append("SBAID = ").Append("-1").Append(" AND ").Append("GRUPOID = ").Append("-1")

            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.UpdateBatch(dsDados, _
                                         strAux.ToString, _
                                         objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SUBASSUNTOGRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:45:02 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT SBAID, GRUPOID FROM SUBASSUNTOGRUPO")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    Public Function QueryEMAILRespChamado(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT USEMAIL FROM USUARIOSUBASSUNTO INNER JOIN USUARIO ON USUARIO.USID = USUARIOSUBASSUNTO.USID AND USUARIO.USSTATUS = 1 INNER JOIN USUARIOLOJA ON USUARIO.USID = USUARIOLOJA.USID ")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            strAux.Append(" GROUP BY USEMAIL ")

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

End Class