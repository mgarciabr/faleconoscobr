﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Acesso a Dados das operações na tabela ASSUNTOSUBASSUNTO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 19/05/2014 09:38:27</remarks>
Public Class dbASSUNTOSUBASSUNTO

    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intASSUNTOID As Integer, ByVal intSBAID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO ASSUNTOSUBASSUNTO (ASSUNTOID, SBAID) ")
            strAux.Append("VALUES (")
            strAux.Append(intASSUNTOID).Append(", ").Append(intSBAID)
            strAux.Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intASSUBID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM ASSUNTOSUBASSUNTO WHERE ")
            strAux.Append("ASSUBID = ").Append(intASSUBID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intASSUBID As Integer, ByVal intASSUNTOID As Integer, ByVal intSBAID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE ASSUNTOSUBASSUNTO SET ")
            strAux.Append("ASSUNTOID = ").Append(intASSUNTOID).Append(", ").Append("SBAID = ").Append(intSBAID)
            strAux.Append("WHERE ")
            strAux.Append("ASSUBID = ").Append(intASSUBID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            strAux = New Text.StringBuilder
            strAux.Append("SELECT ASSUBID, ASSUNTOID, SBAID FROM ASSUNTOSUBASSUNTO WHERE ")
            strAux.Append("ASSUNTOID = ").Append("-1").Append(" AND ").Append("SBAID = ").Append("-1")

            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.UpdateBatch(dsDados, _
                                         strAux.ToString, _
                                         objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT ASSUBID, ASSUNTOID, SBAID FROM ASSUNTOSUBASSUNTO")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTOSUBASSUNTO com INNER JOIN em SUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>19/05/2014 09:38:27 - Criação desta função pelo it.maker</remarks>
    Public Function QuerySubassunto(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT ASSUBID, ASSUNTOSUBASSUNTO.SBAID, SBADESCR FROM ASSUNTOSUBASSUNTO ")
            strAux.Append("INNER JOIN SUBASSUNTO ON ASSUNTOSUBASSUNTO.SBAID = SUBASSUNTO.SBAID")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela ASSUNTOSUBASSUNTO com INNER JOIN em ASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>27/10/2014 - Implementada por Maria Beatriz</remarks>
    Public Function QueryAssunto(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT dbo.ASSUNTO.ASSUNTODESCR, dbo.ASSUNTOSUBASSUNTO.ASSUBID, dbo.ASSUNTOSUBASSUNTO.ASSUNTOID, dbo.ASSUNTOSUBASSUNTO.SBAID, dbo.SUBASSUNTO.SBADESCR ")
            strAux.Append("FROM dbo.ASSUNTO INNER JOIN ")
            strAux.Append("dbo.ASSUNTOSUBASSUNTO ON dbo.ASSUNTO.ASSUNTOID = dbo.ASSUNTOSUBASSUNTO.ASSUNTOID INNER JOIN ")
            strAux.Append("dbo.SUBASSUNTO ON dbo.ASSUNTOSUBASSUNTO.SBAID = dbo.SUBASSUNTO.SBAID")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

End Class