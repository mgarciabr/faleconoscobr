﻿Imports System
Imports plAppLibrary.clsCommonFunctions

''' <summary>
''' Classe para gerenciamento de Regras de Acesso a Dados das operações na tabela SOLICITACAO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 14/04/2014 11:32:58</remarks>
Public Class dbSolicitacao

    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal strSOLPROTOCOLO As String, ByVal intASSUNTOID As Integer, ByVal intLOJAID As Integer, ByVal strSOLEMAIL As String,
                           ByVal strDTSOLDTCADASTRO As String, ByVal intSOLSTATUS As Integer, ByVal intASSUBID As Integer, Optional ByVal intUSIDCAPTURA As Integer = Nothing,
                           Optional ByVal intUSIDENCERRA As Integer = Nothing, Optional ByVal strSOLTELEFONE As String = Nothing, Optional ByVal strSOLNUMEROPASSAPORTE As String = Nothing,
                           Optional ByVal strSOLDTCAPTURA As String = "", Optional ByVal strSOLDTENCERRA As String = "", Optional ByVal intUSIDULTIMO As Integer = Nothing,
                           Optional ByVal strSOLEMPRESA As String = "", Optional ByVal strSOLNOME As String = "",
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO SOLICITACAO (SOLPROTOCOLO, ASSUNTOID, LOJAID, SOLEMAIL, SOLDTCADASTRO, SOLSTATUS, SOLTELEFONE, SOLNUMEROPASSAPORTE, ASSUBID")
            If intUSIDCAPTURA <> 0 Then
                strAux.Append(", ").Append("USIDCAPTURA")
            End If
            If intUSIDENCERRA <> 0 Then
                strAux.Append(", ").Append("USIDENCERRA")
            End If
            If Not strSOLDTCAPTURA.Equals("") Then
                strAux.Append(", ").Append("SOLDTCAPTURA")
            End If
            If Not strSOLDTENCERRA.Equals("") Then
                strAux.Append(", ").Append("SOLDTENCERRA")
            End If
            If intUSIDULTIMO <> 0 Then
                strAux.Append(", ").Append("USIDULTIMO")
            End If
            If Not strSOLEMPRESA.Equals("") Then
                strAux.Append(", ").Append("SOLEMPRESA")
            End If
            'Campo adicionado por requisição do Makro
            If Not strSOLNOME.Equals("") Then
                strAux.Append(", ").Append("SOLNOME")
            End If

            strAux.Append(") VALUES (")
            'strAux.Append("'").Append(TrataTextos(strSOLPROTOCOLO)).Append("'").Append(", ").Append(intASSUNTOID).Append(", ").Append(intLOJAID).Append(", ").Append("'").Append(TrataTextos(strSOLEMAIL)).Append("'").Append(", ").Append("'").Append(strDTSOLDTCADASTRO).Append("'").Append(", ").Append(intSOLSTATUS).Append(", ").Append("'").Append(TrataTextos(strSOLTELEFONE)).Append("'").Append(", ").Append("'").Append(TrataTextos(strSOLNUMEROPASSAPORTE)).Append("', ").Append(intASSUBID)
            strAux.Append("'").Append(TrataTextos(strSOLPROTOCOLO)).Append("'").Append(", ").Append(intASSUNTOID).Append(", ").Append(intLOJAID).Append(", ").Append("'").Append(TrataTextos(strSOLEMAIL)).Append("'").Append(", ").Append("'").Append(FormataData_HoraSQL(strDTSOLDTCADASTRO)).Append("'").Append(", ").Append(intSOLSTATUS).Append(", ").Append("'").Append(TrataTextos(strSOLTELEFONE)).Append("'").Append(", ").Append("'").Append(TrataTextos(strSOLNUMEROPASSAPORTE)).Append("', ").Append(intASSUBID)
            If intUSIDCAPTURA <> 0 Then
                strAux.Append(", ").Append(intUSIDCAPTURA)
            End If
            If intUSIDENCERRA <> 0 Then
                strAux.Append(", ").Append(intUSIDENCERRA)
            End If
            If Not strSOLDTCAPTURA.Equals("") Then
                strAux.Append(", '").Append(strSOLDTCAPTURA).Append("'")
            End If
            If Not strSOLDTENCERRA.Equals("") Then
                strAux.Append(", '").Append(strSOLDTENCERRA).Append("'")
            End If
            If intUSIDULTIMO <> 0 Then
                strAux.Append(", ").Append(intUSIDULTIMO)
            End If
            If Not strSOLEMPRESA.Equals("") Then
                strAux.Append(", '").Append(TrataTextos(strSOLEMPRESA)).Append("'")
            End If
            If Not strSOLNOME.Equals("") Then
                strAux.Append(", '").Append(TrataTextos(strSOLNOME)).Append("'")
            End If
            strAux.Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intSOLID As Integer,
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM SOLICITACAO WHERE ")
            strAux.Append("SOLID = ").Append(intSOLID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intSOLID As Integer, ByVal strSOLPROTOCOLO As String, ByVal intASSUNTOID As Integer, ByVal intLOJAID As Integer, ByVal strSOLEMAIL As String, ByVal strDTSOLDTCADASTRO As String, ByVal intSOLSTATUS As Integer, ByVal intASSUBID As Integer, Optional ByVal intUSIDCAPTURA As Integer = Nothing, Optional ByVal intUSIDENCERRA As Integer = Nothing, Optional ByVal strSOLTELEFONE As String = Nothing, Optional ByVal strSOLNUMEROPASSAPORTE As String = Nothing, Optional ByVal intUSIDULTIMO As Integer = Nothing,
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE SOLICITACAO SET ")
            strAux.Append("SOLPROTOCOLO = ").Append("'").Append(TrataTextos(strSOLPROTOCOLO)).Append("'").Append(", ").Append("ASSUNTOID = ").Append(intASSUNTOID).Append(", ").Append("LOJAID = ").Append(intLOJAID).Append(", ").Append("SOLEMAIL = ").Append("'").Append(TrataTextos(strSOLEMAIL)).Append("'").Append(", ").Append("SOLDTCADASTRO = ").Append("'").Append(FormataData_HoraSQL(strDTSOLDTCADASTRO)).Append("'").Append(", ").Append("SOLSTATUS = ").Append(intSOLSTATUS).Append(", ").Append("USIDCAPTURA = ").Append(intUSIDCAPTURA).Append(", ").Append("USIDENCERRA = ").Append(intUSIDENCERRA).Append(", ").Append("SOLTELEFONE = ").Append("'").Append(TrataTextos(strSOLTELEFONE)).Append("'").Append(", ").Append("SOLNUMEROPASSAPORTE = ").Append("'").Append(TrataTextos(strSOLNUMEROPASSAPORTE)).Append("'").Append(", ASSUBID = ").Append(intASSUBID).Append(", ").Append("USIDULTIMO = ").Append(intUSIDULTIMO)
            'strAux.Append("SOLPROTOCOLO = ").Append("'").Append(TrataTextos(strSOLPROTOCOLO)).Append("'").Append(", ").Append("ASSUNTOID = ").Append(intASSUNTOID).Append(", ").Append("LOJAID = ").Append(intLOJAID).Append(", ").Append("SOLEMAIL = ").Append("'").Append(TrataTextos(strSOLEMAIL)).Append("'").Append(", ").Append("SOLDTCADASTRO = ").Append("'").Append(strDTSOLDTCADASTRO).Append("'").Append(", ").Append("SOLSTATUS = ").Append(intSOLSTATUS).Append(", ").Append("USIDCAPTURA = ").Append(intUSIDCAPTURA).Append(", ").Append("USIDENCERRA = ").Append(intUSIDENCERRA).Append(", ").Append("SOLTELEFONE = ").Append("'").Append(TrataTextos(strSOLTELEFONE)).Append("'").Append(", ").Append("SOLNUMEROPASSAPORTE = ").Append("'").Append(TrataTextos(strSOLNUMEROPASSAPORTE)).Append("'").Append(", ASSUBID = ").Append(intASSUBID).Append(", ").Append("USIDULTIMO = ").Append(intUSIDULTIMO)
            strAux.Append(" WHERE ")
            strAux.Append("SOLID = ").Append(intSOLID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 11:32:58 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT * FROM SOLICITACAO")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO com TOP 1
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/04/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function QueryTop1(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT TOP 1 * FROM SOLICITACAO")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO, SOLICITACAOMENSAGEM, LOJA, ASSUNTO e USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function QueryUltimo(Optional strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder

            strAux.Append("SELECT dbo.ASSUNTO.ASSUNTODESCR, dbo.ASSUNTOSUBASSUNTO.ASSUBID, dbo.LOJA.LOJADESCR, dbo.LOJA.LOJAREGIAO, dbo.SOLICITACAO.*, dbo.SOLICITACAOMENSAGEM.SOLMSGDESCR")
            strAux.Append(", dbo.SOLICITACAOMENSAGEM.SOLMSGDTCADASTRO, dbo.SOLICITACAOMENSAGEM.USID, dbo.USUARIO.USEMAIL, dbo.SUBASSUNTO.SBASL, dbo.SUBASSUNTO.SBADESCR FROM dbo.ASSUNTO INNER JOIN ")
            strAux.Append("dbo.SOLICITACAO ON dbo.ASSUNTO.ASSUNTOID = dbo.SOLICITACAO.ASSUNTOID ")
            strAux.Append("INNER JOIN dbo.LOJA ON dbo.SOLICITACAO.LOJAID = dbo.LOJA.LOJAID ")
            strAux.Append("INNER JOIN dbo.SOLICITACAOMENSAGEM ON dbo.SOLICITACAO.SOLID = dbo.SOLICITACAOMENSAGEM.SOLID ")
            strAux.Append("INNER JOIN dbo.ASSUNTOSUBASSUNTO ON dbo.SOLICITACAO.ASSUBID = dbo.ASSUNTOSUBASSUNTO.ASSUBID ")
            strAux.Append("INNER JOIN dbo.SUBASSUNTO ON dbo.ASSUNTOSUBASSUNTO.SBAID = dbo.SUBASSUNTO.SBAID ")
            strAux.Append("LEFT JOIN dbo.USUARIO ON dbo.SOLICITACAO.USIDULTIMO = dbo.USUARIO.USID")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO, SOLICITACAOMENSAGEM, LOJA, ASSUNTO e USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function ObterSolicitacoesMaster(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder

            strAux.AppendLine(" SELECT DISTINCT                                                                              ")
            strAux.AppendLine(" 	assunto.ASSUNTODESCR,                                                                    ")
            strAux.AppendLine(" 	assuntoSubAssunto.ASSUBID,                                                               ")
            strAux.AppendLine(" 	subAssunto.SBASL,                                                                        ")
            strAux.AppendLine(" 	subAssunto.SBADESCR,                                                                     ")
            strAux.AppendLine(" 	loja.LOJADESCR,                                                                          ")
            strAux.AppendLine(" 	loja.LOJAREGIAO,                                                                         ")
            strAux.AppendLine(" 	mensagem.SOLMSGDESCR,                                                                    ")
            strAux.AppendLine(" 	mensagem.SOLMSGDTCADASTRO,                                                               ")
            strAux.AppendLine(" 	mensagem.USID,                                                                           ")
            strAux.AppendLine(" 	usuario.USEMAIL,                                                                         ")
            strAux.AppendLine(" 	solicitacao.*                                                                            ")
            strAux.AppendLine(" FROM                                                                                         ")
            strAux.AppendLine(" 	SOLICITACAO solicitacao                                                                  ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	SOLICITACAOMENSAGEM mensagem ON mensagem.SOLID = SOLICITACAO.SOLID                       ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	ASSUNTO assunto ON assunto.ASSUNTOID = solicitacao.ASSUNTOID                             ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	ASSUNTOSUBASSUNTO assuntoSubAssunto on assuntoSubAssunto.ASSUBID = solicitacao.ASSUBID   ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	SUBASSUNTO subAssunto on subAssunto.SBAID = assuntoSubAssunto.SBAID                      ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	LOJA loja ON loja.LOJAID = solicitacao.LOJAID                                            ")
            strAux.AppendLine(" LEFT JOIN                                                                                    ")
            strAux.AppendLine(" 	USUARIO usuario on usuario.USID = SOLICITACAO.USIDULTIMO                                 ")
            strAux.AppendLine(" WHERE                                                                                        ")
            strAux.AppendLine("     1 = 1                                                                                    ")
            strAux.AppendLine("                                                                                              ")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO, SOLICITACAOMENSAGEM, LOJA, ASSUNTO e USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function ObterSolicitacoesInternas(intUsuarioId As Int32, Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder

            strAux.AppendLine(" SELECT DISTINCT                                                                              ")
            strAux.AppendLine(" 	assunto.ASSUNTODESCR,                                                                    ")
            strAux.AppendLine(" 	assuntoSubAssunto.ASSUBID,                                                               ")
            strAux.AppendLine(" 	subAssunto.SBASL,                                                                        ")
            strAux.AppendLine(" 	subAssunto.SBADESCR,                                                                     ")
            strAux.AppendLine(" 	loja.LOJADESCR,                                                                          ")
            strAux.AppendLine(" 	loja.LOJAREGIAO,                                                                         ")
            strAux.AppendLine(" 	mensagem.SOLMSGDESCR,                                                                    ")
            strAux.AppendLine(" 	mensagem.SOLMSGDTCADASTRO,                                                               ")
            strAux.AppendLine(" 	mensagem.USID,                                                                           ")
            strAux.AppendLine(" 	usuario.USEMAIL,                                                                         ")
            strAux.AppendLine(" 	solicitacao.*                                                                            ")
            strAux.AppendLine(" FROM                                                                                         ")
            strAux.AppendLine(" 	SOLICITACAO solicitacao                                                                  ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	SOLICITACAOMENSAGEM mensagem ON mensagem.SOLID = SOLICITACAO.SOLID                       ")
            strAux.AppendLine("                                                                                              ")
            strAux.AppendLine(" /* ASSUNTO + PERMISSÃO EM ASSUNTO */                                                         ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	ASSUNTO assunto ON assunto.ASSUNTOID = solicitacao.ASSUNTOID                             ")
            strAux.AppendLine(" INNER JOIN (                                                                                 ")
            strAux.AppendLine(" 		SELECT usuarioAssunto.ASSUNTOID                                                      ")
            strAux.AppendLine(" 		FROM USUARIOASSUNTO usuarioAssunto                                                   ")
            strAux.AppendLine(" 	    WHERE usuarioAssunto.USID = " & intUsuarioId & "                                     ")
            strAux.AppendLine(" 	) innerUsuarioAssunto ON innerUsuarioAssunto.ASSUNTOID = solicitacao.ASSUNTOID           ")
            strAux.AppendLine("                                                                                              ")
            strAux.AppendLine(" /* SUB-ASSUNTO + PERMISSÃO EM SUB-ASSUNTO */                                                 ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	ASSUNTOSUBASSUNTO assuntoSubAssunto on assuntoSubAssunto.ASSUBID = solicitacao.ASSUBID   ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	SUBASSUNTO subAssunto on subAssunto.SBAID = assuntoSubAssunto.SBAID                      ")
            strAux.AppendLine(" INNER JOIN (                                                                                 ")
            strAux.AppendLine(" 		SELECT usuarioSubAssunto.ASSUBID                                                     ")
            strAux.AppendLine(" 		FROM USUARIOSUBASSUNTO usuarioSubAssunto                                             ")
            strAux.AppendLine(" 	    WHERE usuarioSubAssunto.USID = " & intUsuarioId & "                                  ")
            strAux.AppendLine(" 	) innerUsuarioSubAssunto ON innerUsuarioSubAssunto.ASSUBID = solicitacao.ASSUBID         ")
            strAux.AppendLine("                                                                                              ")
            strAux.AppendLine(" /* LOJA + PERMISSÃO EM LOJA */                                                               ")
            strAux.AppendLine(" INNER JOIN                                                                                   ")
            strAux.AppendLine(" 	LOJA loja ON loja.LOJAID = solicitacao.LOJAID                                            ")
            strAux.AppendLine(" INNER JOIN (                                                                                 ")
            strAux.AppendLine(" 		SELECT subLoja.LOJAID                                                                ")
            strAux.AppendLine(" 		FROM LOJA subLoja                                                                    ")
            strAux.AppendLine(" 		INNER JOIN USUARIOLOJA usuarioLoja ON usuarioLoja.LOJAID = subLoja.LOJAID AND        ")
            strAux.AppendLine(" 			usuarioLoja.USID = " & intUsuarioId & "                                          ")
            strAux.AppendLine(" 	) innerLoja ON innerLoja.LOJAID = solicitacao.LOJAID                                     ")
            strAux.AppendLine(" LEFT JOIN                                                                                    ")
            strAux.AppendLine(" 	USUARIO usuario on usuario.USID = SOLICITACAO.USIDULTIMO                                 ")
            strAux.AppendLine(" WHERE                                                                                        ")
            strAux.AppendLine("     1 = 1                                                                                    ")
            strAux.AppendLine("                                                                                              ")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela SOLICITACAO, SOLICITACAOMENSAGEM, LOJA, ASSUNTO e USUARIO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function ObterSolicitacoesCliente(intUsuarioId As Int32, Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "",
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder

            strAux.Append(" SELECT DISTINCT                                                                              ")
            strAux.Append(" 	assunto.ASSUNTODESCR,                                                                    ")
            strAux.Append(" 	assuntoSubAssunto.ASSUBID,                                                               ")
            strAux.Append(" 	subAssunto.SBASL,                                                                        ")
            strAux.Append(" 	subAssunto.SBADESCR,                                                                     ")
            strAux.Append(" 	loja.LOJADESCR,                                                                          ")
            strAux.Append(" 	loja.LOJAREGIAO,                                                                         ")
            strAux.Append(" 	mensagem.SOLMSGDESCR,                                                                    ")
            strAux.Append(" 	mensagem.SOLMSGDTCADASTRO,                                                               ")
            strAux.Append(" 	mensagem.USID,                                                                           ")
            strAux.Append(" 	usuario.USEMAIL,                                                                         ")
            strAux.Append(" 	solicitacao.*                                                                            ")
            strAux.Append(" FROM                                                                                         ")
            strAux.Append(" 	SOLICITACAO solicitacao                                                                  ")
            strAux.Append(" INNER JOIN                                                                                   ")
            strAux.Append(" 	SOLICITACAOMENSAGEM mensagem ON mensagem.SOLID = SOLICITACAO.SOLID                       ")
            strAux.Append(" INNER JOIN                                                                                   ")
            strAux.Append(" 	ASSUNTO assunto ON assunto.ASSUNTOID = solicitacao.ASSUNTOID                             ")
            strAux.Append(" INNER JOIN                                                                                   ")
            strAux.Append(" 	ASSUNTOSUBASSUNTO assuntoSubAssunto on assuntoSubAssunto.ASSUBID = solicitacao.ASSUBID   ")
            strAux.Append(" INNER JOIN                                                                                   ")
            strAux.Append(" 	SUBASSUNTO subAssunto on subAssunto.SBAID = assuntoSubAssunto.SBAID                      ")
            strAux.Append(" INNER JOIN                                                                                   ")
            strAux.Append(" 	LOJA loja ON loja.LOJAID = solicitacao.LOJAID                                            ")
            strAux.Append(" LEFT JOIN                                                                                    ")
            strAux.Append(" 	USUARIO usuario on usuario.USID = SOLICITACAO.USIDULTIMO                                 ")
            strAux.Append(" WHERE                                                                                        ")
            strAux.Append("         SOLICITACAO.SOLSTATUS <> 0                                                           ")
            strAux.Append("     AND SOLICITACAO.SOLPROTOCOLO = '" & intUsuarioId & "'                                    ")
            strAux.Append("                                                                                              ")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>15/04/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateStatus(ByVal intSOLID As Integer, ByVal intSOLSTATUS As Integer, Optional ByVal intUSIDCAPTURA As Integer = Nothing, Optional ByVal intUSIDENCERRA As Integer = Nothing, Optional ByVal strSOLDTCAPTURA As String = "", Optional ByVal strSOLDTENCERRA As String = "",
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE SOLICITACAO SET ")
            strAux.Append("SOLSTATUS = ").Append(intSOLSTATUS)

            If intUSIDCAPTURA <> 0 Then
                strAux.Append(", ").Append("USIDCAPTURA = ").Append(intUSIDCAPTURA)
            End If

            If intUSIDENCERRA <> 0 Then
                strAux.Append(", ").Append("USIDENCERRA = ").Append(intUSIDENCERRA)
            End If
            If Not strSOLDTCAPTURA.Equals("") Then
                strAux.Append(", ").Append("SOLDTCAPTURA = '").Append(FormataData_HoraSQL(strSOLDTCAPTURA)).Append("'")
            End If
            If Not strSOLDTENCERRA.Equals("") Then
                strAux.Append(", ").Append("SOLDTENCERRA = '").Append(FormataData_HoraSQL(strSOLDTENCERRA)).Append("'")
            End If

            strAux.Append(" WHERE ")
            strAux.Append("SOLID = ").Append(intSOLID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>23/05/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateProtocolo(ByVal intSOLID As Integer, ByVal strSOLPROTOCOLO As Integer,
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE SOLICITACAO SET ")
            strAux.Append("SOLPROTOCOLO = ").Append(strSOLPROTOCOLO)
            strAux.Append(" WHERE ")
            strAux.Append("SOLID = ").Append(intSOLID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela SOLICITACAO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>13/06/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateUltimo(ByVal intSOLID As Integer, ByVal intUSIDULTIMO As Integer,
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE SOLICITACAO SET ")
            strAux.Append("USIDULTIMO = ").Append(intUSIDULTIMO)
            strAux.Append(" WHERE ")
            strAux.Append("SOLID = ").Append(intSOLID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function
End Class