﻿Imports System
Imports plAppLibrary.clsCommonFunctions

Public Class dbUsuarioLoja
    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Insert(ByVal intLOJAID As Integer, ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO USUARIOLOJA (LOJAID, USID) ")
            strAux.Append("VALUES (")
            strAux.Append(intLOJAID).Append(", ").Append(intUSID).Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Delete(ByVal intLOJAID As Integer, ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM USUARIOLOJA WHERE ")
            strAux.Append("USID = ").Append(intUSID).Append(" AND LOJAID = ").Append(intLOJAID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de todos os dados de um usuário.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 09/09/2014</remarks>
    Public Function DeleteUs(ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM USUARIOLOJA WHERE ")
            strAux.Append("USID = ").Append(intUSID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Update(ByVal intLOJAID As Integer, ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE USUARIOLOJA SET ")
            strAux.Append("LOJAID = ").Append(intLOJAID).Append(", USID = ").Append(intUSID)
            strAux.Append("WHERE ")
            strAux.Append("USID = ").Append(intUSID).Append(" AND LOJAID = ").Append(intLOJAID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT * FROM USUARIOLOJA")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            strAux = New Text.StringBuilder
            strAux.Append("SELECT ULID, LOJAID, USID FROM USUARIOLOJA WHERE ")
            strAux.Append("LOJAID = -1 AND USID = -1")

            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.UpdateBatch(dsDados, _
                                         strAux.ToString, _
                                         objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
