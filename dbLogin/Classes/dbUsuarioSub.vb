﻿Imports System
Imports plAppLibrary.clsCommonFunctions

Public Class dbUsuarioSub
    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela USUARIOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 27/10/2014</remarks>
    Public Function Insert(ByVal intUSID As Integer, ByVal intASSUBID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO USUARIOSUBASSUNTO (USID, ASSUBID) ")
            strAux.Append("VALUES (")
            strAux.Append(intUSID).Append(", ").Append(intASSUBID).Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 27/10/2014</remarks>
    Public Function Delete(ByVal intUSSBAID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM USUARIOSUBASSUNTO WHERE ")
            strAux.Append("USSBAID = ").Append(intUSSBAID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de todos os dados de um usuário.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 09/09/2014</remarks>
    Public Function DeleteUs(ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM USUARIOSUBASSUNTO WHERE ")
            strAux.Append("USID = ").Append(intUSID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 27/10/2014</remarks>
    Public Function Update(ByVal intUSSBAID As Integer, ByVal intUSID As Integer, ByVal intASSUBID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE USUARIOSUBASSUNTO SET ")
            strAux.Append("USID = ").Append(intUSID).Append(", ASSUBID = ").Append(intASSUBID)
            strAux.Append("WHERE ")
            strAux.Append("USSBAID = ").Append(intUSSBAID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIOSUBASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 27/10/2014</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT * FROM USUARIOSUBASSUNTO")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            strAux = New Text.StringBuilder
            strAux.Append("SELECT  USID, ASSUBID FROM USUARIOSUBASSUNTO WHERE ")
            strAux.Append("USID = -1 AND ASSUBID = -1")

            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.UpdateBatch(dsDados, _
                                         strAux.ToString, _
                                         objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
