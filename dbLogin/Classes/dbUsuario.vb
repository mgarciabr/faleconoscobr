﻿Imports System
Imports plAppLibrary.clsCommonFunctions

''' <summary>
''' Classe para gerenciamento de Regras de Acesso a Dados das operações na tabela USUARIO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 10/04/2014 14:54:39</remarks>
Public Class dbUsuario

    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intPERFID As Integer, ByVal strUSEMAIL As String, ByVal strUSSENHA As String, ByVal bitUSALTERASENHA As Boolean, ByVal strDTUSDTCADASTRO As String, ByVal intUSSTATUS As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO USUARIO (PERFID, USEMAIL, USSENHA, USALTERASENHA, USDTCADASTRO, USSTATUS) ")
            strAux.Append("VALUES (")
            'strAux.Append(intPERFID).Append(", ").Append("'").Append(TrataTextos(strUSEMAIL)).Append("'").Append(", ").Append("'").Append(TrataTextos(strUSSENHA)).Append("'").Append(", '").Append(bitUSALTERASENHA).Append("', ").Append("'").Append(strDTUSDTCADASTRO).Append("'").Append(", ").Append(intUSSTATUS)
            strAux.Append(intPERFID).Append(", ").Append("'").Append(TrataTextos(strUSEMAIL)).Append("'").Append(", ").Append("'").Append(TrataTextos(strUSSENHA)).Append("'").Append(", '").Append(bitUSALTERASENHA).Append("', ").Append("'").Append(FormataData_HoraSQL(strDTUSDTCADASTRO)).Append("'").Append(", ").Append(intUSSTATUS)
            strAux.Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM USUARIO WHERE ")
            strAux.Append("USID = ").Append(intUSID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intUSID As Integer, ByVal intPERFID As Integer, ByVal strUSEMAIL As String, ByVal strUSSENHA As String, ByVal bitUSALTERASENHA As Boolean, ByVal strDTUSDTCADASTRO As String, ByVal intUSSTATUS As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE USUARIO SET ")
            strAux.Append("PERFID = ").Append(intPERFID).Append(", ").Append("USEMAIL = ").Append("'").Append(TrataTextos(strUSEMAIL)).Append("'").Append(", ").Append("USSENHA = ").Append("'").Append(TrataTextos(strUSSENHA)).Append("'").Append(", ").Append("USALTERASENHA = '").Append(bitUSALTERASENHA).Append("', ").Append("USDTCADASTRO = ").Append("'").Append(FormataData_HoraSQL(strDTUSDTCADASTRO)).Append("'").Append(", ").Append("USSTATUS = ").Append(intUSSTATUS)
            strAux.Append("WHERE ")
            strAux.Append("USID = ").Append(intUSID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração da coluna status na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>12/05/2014 - Implementada por Maria Beatriz</remarks>
    Public Function UpdateStatus(ByVal intUSID As Integer, ByVal intUSSTATUS As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE USUARIO SET ")
            strAux.Append("USSTATUS = ").Append(intUSSTATUS)
            strAux.Append(" WHERE ")
            strAux.Append("USID = ").Append(intUSID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/04/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateSenha(ByVal intUSID As Integer, ByVal strUSSENHA As String, ByVal bitUSALTERASENHA As Boolean, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE USUARIO SET ")
            strAux.Append("USSENHA = ").Append("'").Append(TrataTextos(strUSSENHA)).Append("'").Append(", ").Append("USALTERASENHA = '").Append(bitUSALTERASENHA).Append("'")
            strAux.Append("WHERE ")
            strAux.Append("USID = ").Append(intUSID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>23/06/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateCadastro(ByVal intUSID As Integer, ByVal strUSDTCADASTRO As String, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE USUARIO SET ")
            strAux.Append("USDTCADASTRO = ").Append("'").Append(FormataData_HoraSQL(strUSDTCADASTRO)).Append("'")
            'strAux.Append("USDTCADASTRO = ").Append("'").Append(strUSDTCADASTRO).Append("'")
            strAux.Append("WHERE ")
            strAux.Append("USID = ").Append(intUSID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            strAux = New Text.StringBuilder
            strAux.Append("SELECT USID, PERFID, USEMAIL, USSENHA, USALTERASENHA, USDTCADASTRO, USSTATUS FROM USUARIO WHERE ")
            strAux.Append("PERFID = ").Append("-1").Append(" AND ").Append("USEMAIL = ").Append("''").Append(" AND ").Append("USSENHA = ").Append("''").Append(" AND ").Append("USALTERASENHA = True").Append(" AND ").Append("USDTCADASTRO = ").Append("''").Append(" AND ").Append("USSTATUS = ").Append("-1")

            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.UpdateBatch(dsDados, _
                                         strAux.ToString, _
                                         objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT USID, PERFID, USEMAIL, USSENHA, USALTERASENHA, USDTCADASTRO, USSTATUS FROM USUARIO")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO, INNER JOIN em LOJA
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function QueryRegiao(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT * FROM (")
            strAux.Append("SELECT USUARIO.USID, PERFID, USEMAIL, USSENHA, USALTERASENHA, USDTCADASTRO, USSTATUS, LOJAREGIAO, ROW_NUMBER() OVER(PARTITION BY USUARIO.USID ORDER BY LOJAREGIAO DESC) rn FROM USUARIO ")
            strAux.Append("INNER JOIN USUARIOLOJA ON USUARIOLOJA.USID = USUARIO.USID ")
            strAux.Append("INNER JOIN LOJA ON USUARIOLOJA.LOJAID = LOJA.LOJAID WHERE USSTATUS <> 0 ")
            strAux.Append(") a WHERE rn = 1")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" AND ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO, INNER JOIN em LOJA
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function QueryLoja(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT * FROM (")
            strAux.Append("SELECT USUARIO.USID, PERFID, LOJA.LOJAID, USEMAIL, USSENHA, USALTERASENHA, USDTCADASTRO, USSTATUS, LOJAREGIAO, ROW_NUMBER() OVER(PARTITION BY USUARIO.USID ORDER BY LOJAREGIAO DESC) rn FROM USUARIO ")
            strAux.Append("LEFT JOIN USUARIOLOJA ON USUARIOLOJA.USID = USUARIO.USID ")
            strAux.Append("LEFT JOIN LOJA ON LOJA.LOJAID = USUARIOLOJA.LOJAID WHERE USSTATUS <> 0 ")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" AND ").Append(strWhere)
            End If

            strAux.Append(") a WHERE rn = 1")

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function UltimoId(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Integer
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT TOP 1 USID FROM USUARIO")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados.Tables(0).Rows(0).Item("USID")

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 28/10/2014</remarks>
    Public Function QueryDistinct(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT distinct USUARIO.USID, PERFID, USEMAIL, USSENHA, USALTERASENHA, USDTCADASTRO, USSTATUS FROM USUARIO ")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function
End Class