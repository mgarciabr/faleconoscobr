﻿Imports System
Imports plAppLibrary.clsCommonFunctions

''' <summary>
''' Classe para gerenciamento de Regras de Acesso a Dados das operações na tabela PERFIL
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 14/04/2014 09:37:19</remarks>
Public Class dbPerfil

    Private mdaAccess As daAccess.MSSQLServerDataAccess

    ''' <summary>
    ''' Função para Inclusão de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:19 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intPERFID As Integer, ByVal strPERFDESCR As String, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para inserção dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("INSERT INTO PERFIL (PERFID, PERFDESCR) ")
            strAux.Append("VALUES (")
            strAux.Append(intPERFID).Append(", ").Append("'").Append(TrataTextos(strPERFDESCR)).Append("'")
            strAux.Append(")")

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:19 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intPERFID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para exclusão dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("DELETE FROM PERFIL WHERE ")
            strAux.Append("PERFID = ").Append(intPERFID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:19 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intPERFID As Integer, ByVal strPERFDESCR As String, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            'Definindo os parâmetros para alteração dos dados.
            strAux = New Text.StringBuilder
            strAux.Append("UPDATE PERFIL SET ")
            strAux.Append("PERFDESCR = ").Append("'").Append(TrataTextos(strPERFDESCR)).Append("'")
            strAux.Append("WHERE ")
            strAux.Append("PERFID = ").Append(intPERFID)

            'Passando parametro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.Execute(strAux.ToString, objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:19 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Dim strAux As Text.StringBuilder

        Try
            strAux = New Text.StringBuilder
            strAux.Append("SELECT PERFID, PERFDESCR FROM PERFIL WHERE ")
            strAux.Append("PERFID = ").Append("-1").Append(" AND ").Append("PERFDESCR = ").Append("''")

            mdaAccess = New daAccess.MSSQLServerDataAccess
            Return mdaAccess.UpdateBatch(dsDados, _
                                         strAux.ToString, _
                                         objTransaction)
            mdaAccess = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:19 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Data.DataSet
        Dim dsDados As Data.DataSet
        Dim strAux As Text.StringBuilder

        Try
            'Definindo parâmetro de consulta.
            strAux = New Text.StringBuilder
            strAux.Append("SELECT PERFID, PERFDESCR FROM PERFIL")

            'Verificando se foi definido algum filtro pelo usuário.
            If strWhere <> "" Then
                strAux.Append(" WHERE ").Append(strWhere)
            End If

            'Verificando se foi definido alguma ordenação pelo usuário.
            If strOrderBy <> "" Then
                strAux.Append(" ORDER BY ").Append(strOrderBy)
            End If

            'Passando parâmetro para daAccess.
            mdaAccess = New daAccess.MSSQLServerDataAccess
            dsDados = mdaAccess.ReturnQuery(strAux.ToString, objTransaction)
            mdaAccess = Nothing

            'Retornando dados da consulta.
            Return dsDados

        Catch ex As Exception
            Throw ex
        Finally
            'Limpando variáveis.
            mdaAccess = Nothing
            dsDados = Nothing
            strAux = Nothing
        End Try
    End Function

End Class