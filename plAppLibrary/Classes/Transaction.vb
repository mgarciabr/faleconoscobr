﻿Public Class Transaction

    Private mconConection As SqlClient.SqlConnection
    Private mtrsTransaction As SqlClient.SqlTransaction
    Private mblnThrowException As Boolean = False
    Private mblnTransactionOpened As Boolean = False

    ''' <summary>
    ''' Retorna o objeto SQLTransaction.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SqlTransaction() As SqlClient.SqlTransaction
        Get
            Return mtrsTransaction
        End Get
    End Property

    ''' <summary>
    ''' Retorna o objeto SQLConection.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SqlConection() As SqlClient.SqlConnection
        Get
            Return mconConection
        End Get
    End Property

    ''' <summary>
    ''' Retorna status da Transação.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Opened() As Boolean
        Get
            Return mblnTransactionOpened
        End Get
    End Property

    ''' <summary>
    ''' Inicia a Transação.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub IniciaTransacao()
        Try
            If mblnThrowException Then
                Dim myerr As New Exception("This transaction was closed. Please, create a new instance.")
                Throw myerr
            End If
            If Not mblnTransactionOpened Then
                mconConection = New SqlClient.SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("APPConnectionString").ToString)
                mconConection.Open()

                'iniciando
                mtrsTransaction = mconConection.BeginTransaction
                mblnTransactionOpened = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Finaliza a Transação.
    ''' </summary>
    ''' <param name="Status"></param>
    ''' <remarks></remarks>
    Public Sub FinalizaTransacao(ByVal Status As plAppLibrary.Enumerators.enTransactionStatus)
        Try
            mblnThrowException = True
            If Status = Enumerators.enTransactionStatus.entsSuccess Then
                mtrsTransaction.Commit()
                mblnTransactionOpened = False
            Else
                mtrsTransaction.Rollback()
                mblnTransactionOpened = False
            End If

            If Not mconConection Is Nothing Then
                If mconConection.State <> ConnectionState.Closed Then
                    mconConection.Close()
                End If
            End If
            mconConection = Nothing
            mtrsTransaction = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
