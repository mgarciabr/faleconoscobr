﻿Public Class Enumerators

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '''''
    '''''   ATENÇÃO!!!!
    '''''
    '''''   Sempre verifique se neste módulo já existe o enumerador que você está procurando ANTES de criar um novo enumerador.
    '''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    ''' <summary>
    ''' Enumerador que define o status do usuário.
    ''' </summary>
    Public Enum enStatusUsuario As Integer
        enExcluido = 0
        enAtivo = 1
    End Enum

    ''' <summary>
    ''' Enumerador para Status de Log
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum enLogStatus As Integer
        enlsInformation = 0     'Causado pelo sistema (Validações, Regras, etc)
        enlsError = 1           'Causado pelo Compilador (Erro inesperado).
        enlsSuccessed = 2       'Causado pelo sistema (ao realizar alguma operação, importação, etc).
        enlsFailed = 3          'Causado pelo sistema (ao realizar alguma operação, importação, etc).
        enlsCommandLOG = 4  '***** USADO DIRETO NA CLASSE MSSQLSERVERDATAACCESS.VB do projeto DAACCESS (está como HARDCODE, na função GRAVALOGCOMANDO) ****
    End Enum

    ''' <summary>
    ''' Enumerador para tipos de operação.
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum enTipoOperacao As Integer
        entpINCLUSAO = 1
        entpALTERACAO = 2
        entpEXCLUIR = 3
        entpCONSULTAR = 4
    End Enum

    ''' <summary>
    ''' Enumerador para Status de Transaction
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum enTransactionStatus As Integer
        entsSuccess = 0
        entsFailed = 1
    End Enum

    ''' <summary>
    ''' Enumerador para identificação de módulos do sistema
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum enModulo As Integer
        CadastrarAssociado = 1001
        CadastrarLoja = 1002
        CadastrarMelhorHorario = 1003
        CadastrarConvenio = 1004
        CadastrarBanco = 1005
        AcompanharSolicitacao = 1006
        SimularEmprestimo = 1007
        SolicitarEmprestimo = 1008
        RealizarBusca = 1009
        FaleConosco = 1010
    End Enum

    ''' <summary>
    ''' Enumerador para Definição de Status do Cadastro de Clientes
    ''' </summary>
    ''' <remarks>Criado por André Calza em 2014-01-10</remarks>
    Public Enum enStatusCliente
        Excluido = 0
        Ativo = 1
        Inativo = 2
    End Enum

    ''' <summary>
    ''' Enumerador para definição de estado civil de clientes
    ''' </summary>
    ''' <remarks>Criado por Maria Beatriz em 2014-01-17</remarks>
    Public Enum enEstadoCivil
        Casado = 1
        Divorciado = 2
        Solteiro = 3
        Viuvo = 4
    End Enum

    ''' <summary>
    ''' Enumerador para definição de sexo de clientes
    ''' </summary>
    ''' <remarks>Criado por Maria Beatriz em 2014-01-17</remarks>
    Public Enum enSexo
        Feminino = 1
        Masculino = 2
    End Enum

    ''' <summary>
    ''' Enumerador para definição de status para solicitação
    ''' </summary>
    ''' <remarks>Criado por Maria Beatriz em 2014-04-14</remarks>
    Public Enum enStatusSolicitacao
        Excluida = 0    'não mencionado na documentação        
        Aberta = 1
        Capturada = 2
        Encerrada = 3
    End Enum

    ''' <summary>
    ''' Enumerador para definição de status para assunto
    ''' </summary>
    ''' <remarks>Criado por Maria Beatriz em 2014-04-14</remarks>
    Public Enum enStatusAssunto
        Excluido = 0
        Ativo = 1
        Inativo = 2
    End Enum

    ''' <summary>
    ''' Enumerador para definição de status para assunto
    ''' </summary>
    ''' <remarks>Criado por Maria Beatriz em 2014-05-14</remarks>
    Public Enum enUserPerfil
        Gerente = 1
        Cliente = 2
        Master = 3
    End Enum

    Public Enum enLojaRegiao
        SaoPaulo = 1
        SaoPauloInter = 2
        SaoPauloRM = 3
        RJES = 4
        Centro = 5
        Nordeste = 6
        Norte = 7
        Sul = 8
        Brasil = 9
    End Enum
End Class
