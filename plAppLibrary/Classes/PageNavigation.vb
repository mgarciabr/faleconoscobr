﻿Imports System
Imports Microsoft.VisualBasic

<Serializable()> _
Public Class PageNavigation
    Private mstrPageName As String
    Private mstrConfirmAction As String
    Private mstrReturnAction As String
    Private mstrFilter As String
    Private mobjPageData As Object
    Private mcolPageParameters As Collection
    Private mcolOrder As Collection
    Private mintGridIndexPage As Integer
    Private mintOper As Integer

    ''' <summary>
    ''' Nome da operação controlada.
    ''' </summary>
    Public Property Operacao() As Integer
        Get
            Return mintOper
        End Get
        Set(ByVal value As Integer)
            mintOper = value
        End Set
    End Property

    ''' <summary>
    ''' Nome da página controlada.
    ''' </summary>
    Public Property PageName() As String
        Get
            Return mstrPageName
        End Get
        Set(ByVal value As String)
            mstrPageName = value
        End Set
    End Property

    ''' <summary>
    ''' Ação de Confirmação.
    ''' </summary>
    Public Property ConfirmAction() As String
        Get
            Return mstrConfirmAction
        End Get
        Set(ByVal value As String)
            mstrConfirmAction = value
        End Set
    End Property

    ''' <summary>
    ''' Ação de Retorno da página.
    ''' </summary>
    Public Property ReturnAction() As String
        Get
            Return mstrReturnAction
        End Get
        Set(ByVal value As String)
            mstrReturnAction = value
        End Set
    End Property

    ''' <summary>
    ''' Filtro de dados da página.
    ''' </summary>
    Public Property Filter() As String
        Get
            Return mstrFilter
        End Get
        Set(ByVal value As String)
            mstrFilter = value
        End Set
    End Property

    ''' <summary>
    ''' Ordenação de dados da página.
    ''' </summary>
    Public Property Order() As Collection
        Get
            Return mcolOrder
        End Get
        Set(ByVal value As Collection)
            mcolOrder = value
        End Set
    End Property

    ''' <summary>
    ''' Dados da Página.
    ''' </summary>
    Public Property PageData() As Object
        Get
            Return mobjPageData
        End Get
        Set(ByVal value As Object)
            mobjPageData = value
        End Set
    End Property

    ''' <summary>
    ''' Parâmetros enviados à página.
    ''' </summary>
    Public Property PageParameters() As Collection
        Get
            Return mcolPageParameters
        End Get
        Set(ByVal value As Collection)
            mcolPageParameters = value
        End Set
    End Property

    ''' <summary>
    ''' Índice da página da Grid.
    ''' </summary>
    Public Property GridIndexPage() As Integer
        Get
            Return mintGridIndexPage
        End Get
        Set(ByVal value As Integer)
            mintGridIndexPage = value
        End Set
    End Property

End Class
