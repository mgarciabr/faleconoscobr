﻿Imports System.IO
Imports System.Net.Mail
Imports System.Net
Imports System.Text
Imports System.Security.Cryptography
Imports Mailjet.Client
Imports Mailjet.Client.Resources
Imports System
Imports Newtonsoft.Json.Linq


Public Class clsCommonFunctions

    Private Shared TripleDES As New TripleDESCryptoServiceProvider
    Private Shared MD5 As New MD5CryptoServiceProvider

    ' Definição da chave de encriptação/desencriptação 
    Private Const key As String = "d41d8cd98f00b2PortalCampanhaColgate04e9800998ecf8427e"

    ''' <summary> 
    ''' Calcula o MD5 Hash  
    ''' </summary> 
    ''' <param name="value">Chave</param> 
    Public Shared Function MD5Hash(ByVal value As String) As Byte()

        ' Converte a chave para um array de bytes  
        Dim byteArray() As Byte = ASCIIEncoding.UTF8.GetBytes(value)
        Return MD5.ComputeHash(byteArray)

    End Function

    ''' <summary> 
    ''' Encripta uma string com base em uma chave 
    ''' </summary> 
    ''' <param name="stringToEncrypt">String a encriptar</param> 
    Public Shared Function Encrypt(ByVal stringToEncrypt As String) As String
        Dim strPrepara As String
        Try

            ' Definição da chave e da cifra (que neste caso é Electronic 
            ' Codebook, ou seja, encriptação individual para cada bloco) 
            TripleDES.Key = MD5Hash(key)
            TripleDES.Mode = CipherMode.ECB

            ' Converte a string para bytes e encripta 
            Dim Buffer As Byte() = ASCIIEncoding.ASCII.GetBytes(stringToEncrypt)
            strPrepara = Convert.ToBase64String(TripleDES.CreateEncryptor.TransformFinalBlock(Buffer, 0, Buffer.Length))

            strPrepara = strPrepara.Replace("+", "ZZzZZ")

            Return strPrepara

        Catch ex As Exception

            'MessageBox.Show(ex.Message, My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Error)
            ' Return String.Empty

        End Try

    End Function

    ''' <summary> 
    ''' Desencripta uma string com base em uma chave 
    ''' </summary> 
    ''' <param name="encryptedString">String a decriptar</param> 
    Public Shared Function Decrypt(ByVal encryptedString As String) As String

        Try
            encryptedString = encryptedString.Replace("ZZzZZ", "+")

            ' Definição da chave e da cifra 

            If encryptedString = String.Empty Then Return String.Empty
            TripleDES.Key = MD5Hash(key)
            TripleDES.Mode = CipherMode.ECB

            ' Converte a string encriptada para bytes e decripta 
            Dim Buffer As Byte() = Convert.FromBase64String(encryptedString)
            Return ASCIIEncoding.ASCII.GetString(TripleDES.CreateDecryptor.TransformFinalBlock(Buffer, 0, Buffer.Length))

        Catch ex As Exception

            'MessageBox.Show(ex.Message, My.Application.Info.Title, _
            '               MessageBoxButtons.OK, MessageBoxIcon.Error)
            ' Return String.Empty

        End Try

    End Function

    ''' <summary>
    ''' Função para formatar Data/Hora no padrão do SQL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FormataData_HoraSQLInsert(ByVal strData_Hora As String) As String
        Dim strAux As System.Text.StringBuilder
        Try
            strAux = New System.Text.StringBuilder
            If IsDate(strData_Hora) Then
                'strAux.Append((CDate(strData_Hora)))
                strAux.Append(Format(Year(CDate(strData_Hora)), "00"))
                strAux.Append("-")
                strAux.Append(Format(Day(CDate(strData_Hora)), "00"))
                strAux.Append("-")
                strAux.Append(Format(Month(CDate(strData_Hora)), "00"))
                strAux.Append(" ")
                strAux.Append(Format(Hour(CDate(strData_Hora)), "00"))
                strAux.Append(":")
                strAux.Append(Format(Minute(CDate(strData_Hora)), "00"))
                strAux.Append(":")
                strAux.Append(Format(Second(CDate(strData_Hora)), "00"))
            End If
            Return strAux.ToString

        Catch ex As Exception
            Throw ex
        Finally
            strAux = Nothing
        End Try
    End Function

    Public Shared Function FormataData_HoraSQL(ByVal strData_Hora As String) As String
        Dim strAux As System.Text.StringBuilder
        Try
            strAux = New System.Text.StringBuilder
            If IsDate(strData_Hora) Then
                'strAux.Append((CDate(strData_Hora)))
                strAux.Append(Format(Year(CDate(strData_Hora)), "00"))
                strAux.Append("-")
                strAux.Append(Format(Month(CDate(strData_Hora)), "00"))
                strAux.Append("-")
                strAux.Append(Format(Day(CDate(strData_Hora)), "00"))
                strAux.Append(" ")
                strAux.Append(Format(Hour(CDate(strData_Hora)), "00"))
                strAux.Append(":")
                strAux.Append(Format(Minute(CDate(strData_Hora)), "00"))
                strAux.Append(":")
                strAux.Append(Format(Second(CDate(strData_Hora)), "00"))
            End If
            Return strAux.ToString

        Catch ex As Exception
            Throw ex
        Finally
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para tratar a vírgula (,) quando um número possui casas decimais, trocando-a por ponto (.). 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FormataNumero_SQL(ByVal curTotal As Double, ByVal intCasasDecimais As Integer) As String
        Dim strTotal As String
        Dim i As Integer
        Try

            strTotal = FormatNumber(curTotal, intCasasDecimais)


            '
            'Função com erro!! Descoberto em 27/01/2013 - João Paulo.
            'Tratando o erro: aparece um número "1.235,90" e depois de converter, fica "1.235.90" e dá erro!
            '
            If (InStr(strTotal, ",") > 0) And (InStr(strTotal, ".") > 0) Then
                'Possui as duas pontuações!! Retirar o ponto!
                strTotal = strTotal.Replace(".", "")
            End If



            i = InStr(strTotal, ",")
            If i = 0 Then
                i = InStr(strTotal, ".")
            End If

            If i > 0 Then
                strTotal = Left(strTotal, i - 1) & "." & Right(strTotal, Len(strTotal) - i)
            End If

            Return strTotal

        Catch ex As Exception
            Throw ex
        Finally
            strTotal = Nothing
            i = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Função para formatar Data no padrão do SQL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FormataData_SQL(ByVal strData As String) As String
        Dim strAux As System.Text.StringBuilder
        Try
            strAux = New System.Text.StringBuilder
            If IsDate(strData) Then
                strAux.Append(Format(Year(CDate(strData)), "00"))
                strAux.Append("-")
                strAux.Append(Format(Month(CDate(strData)), "00"))
                strAux.Append("-")
                strAux.Append(Format(Day(CDate(strData)), "00"))
            End If
            Return strAux.ToString

        Catch ex As Exception
            Throw ex
        Finally
            strAux = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para acertar o texto (primeira em maiúsculo, demais em minúsculo).
    ''' </summary>
    ''' <param name="strTexto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AcertaTexto(ByVal strTexto As String) As String
        Dim strAux As Text.StringBuilder

        Try
            If strTexto <> "" Then
                strAux = New Text.StringBuilder
                strAux.Append(strTexto.Substring(0, 1).ToUpper)
                strAux.Append(strTexto.Substring(1, strTexto.Length - 1).ToLower)
                Return strAux.ToString
            Else
                Return ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para colocar todas as iniciais em maiúsculo
    ''' </summary>
    ''' <param name="strTexto"></param>
    ''' <returns></returns>
    ''' <remarks>Implementada por Maria Beatriz em 04/02/2014</remarks>
    Public Shared Function AcertaFrase(ByVal strTexto As String) As String
        Dim palavras() As String = strTexto.ToLower.Split(" ")
        Dim aux As String = ""
        For i As Integer = 0 To palavras.Length - 1
            aux = palavras(i)
            If aux.Equals("de") Or aux.Equals("e") Or aux.Equals("da") Or aux.Equals("do") Or aux.Equals("das") Or aux.Equals("dos") Then
                Continue For
            Else
                palavras(i) = (New StringBuilder().Append(aux.Substring(0, 1).ToUpper).Append(aux.Substring(1, aux.Length - 1))).ToString
            End If
        Next
        Return String.Join(" ", palavras)
    End Function

    ''' <summary>
    ''' Função para obter a descrição de um Mês com base no número do mesmo.
    ''' </summary>
    ''' <param name="intMonth"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getMonthDescr(ByVal intMonth As Integer) As String
        Try
            If intMonth = 1 Then
                Return "Janeiro"
            End If
            If intMonth = 2 Then
                Return "Fevereiro"
            End If

            If intMonth = 3 Then
                Return "Março"
            End If
            If intMonth = 4 Then
                Return "Abril"
            End If
            If intMonth = 5 Then
                Return "Maio"
            End If
            If intMonth = 6 Then
                Return "Junho"
            End If
            If intMonth = 7 Then
                Return "Julho"
            End If
            If intMonth = 8 Then
                Return "Agosto"
            End If
            If intMonth = 9 Then
                Return "Setembro"
            End If
            If intMonth = 10 Then
                Return "Outubro"
            End If
            If intMonth = 11 Then
                Return "Novembro"

            End If
            If intMonth = 12 Then
                Return "Dezembro"
            Else
                Return "Month not found"
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para pegar o inteiro correspondente a um Mês.
    ''' </summary>
    ''' <param name="strMonth"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getMonthInt(ByVal strMonth As String) As Integer
        Try
            If strMonth.ToUpper = "JAN" Then
                Return 1
            End If
            If strMonth.ToUpper = "FEB" Or strMonth.ToUpper = "FEV" Then
                Return 2
            End If
            If strMonth.ToUpper = "MAR" Then
                Return 3
            End If
            If strMonth.ToUpper = "APR" Or strMonth.ToUpper = "ABR" Then
                Return 4
            End If
            If strMonth.ToUpper = "MAY" Or strMonth.ToUpper = "MAI" Then
                Return 5
            End If
            If strMonth.ToUpper = "JUN" Then
                Return 6
            End If
            If strMonth.ToUpper = "JUL" Then
                Return 7
            End If
            If strMonth.ToUpper = "AUG" Or strMonth.ToUpper = "AGO" Then
                Return 8
            End If
            If strMonth.ToUpper = "SEP" Or strMonth.ToUpper = "SET" Then
                Return 9
            End If
            If strMonth.ToUpper = "OCT" Or strMonth.ToUpper = "OUT" Then
                Return 10
            End If
            If strMonth.ToUpper = "NOV" Then
                Return 11
            End If
            If strMonth.ToUpper = "DEC" Or strMonth.ToUpper = "DEZ" Then
                Return 12
            Else
                Return 0
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Função para retirar as aspas simples de um texto.
    ''' </summary>
    ''' <param name="strTexto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function TrataTextos(ByVal strTexto As String) As String
        Return LimpaTexto(Replace(strTexto, "'", "''"))
    End Function

    ''' <summary>
    ''' Função para gerar um arquivo binário
    ''' </summary>
    ''' <param name="strArquivo">diretório + nome do arquivo</param>
    ''' <returns></returns>
    ''' <remarks>Implementada por Juliana Lima em 08/05/2012</remarks>
    Public Shared Function GerarBinario(ByVal strArquivo As String) As Byte()
        Dim FileStream As IO.FileStream
        Dim reader As IO.BinaryReader

        Try
            'Arquivo não encontrado
            If Not IO.File.Exists(strArquivo) Then Return Nothing

            FileStream = New IO.FileStream(strArquivo, IO.FileMode.Open, IO.FileAccess.Read)
            reader = New IO.BinaryReader(FileStream)
            GerarBinario = reader.ReadBytes(CInt(My.Computer.FileSystem.GetFileInfo(strArquivo).Length))
            FileStream.Close()
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            FileStream = Nothing
            reader = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para deletar arquivo temporario.
    ''' </summary>
    ''' <param name="strArquivo">diretório + nome do arquivo</param>
    ''' <remarks>Implementada por Juliana Lima em 08/05/2012</remarks>
    Public Shared Sub DeleteFile(ByVal strArquivo As String)
        Try
            If System.IO.File.Exists(strArquivo) Then
                System.IO.File.Delete(strArquivo)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Deleta um arquivo.
    ''' </summary>
    ''' <remarks>Implementado em 03/01/2013 por João Paulo</remarks>
    Public Shared Sub DeleteFile2(ByVal strArquivo As String)
        Try
            Dim fs As New System.IO.FileInfo(strArquivo)
            fs.Delete()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Método para deletar todos os arquivos de determinado diretório
    ''' </summary>
    ''' <param name="strDiretorio">diretório a ter os arquivos deletados</param>
    ''' <remarks>Implementado por Juliana Lima em 09/05/2012</remarks>
    Public Shared Sub DeleteArquivosDiretorio(ByVal strDiretorio As String)
        'Dim strArquivos As String()
        'Try
        '    If System.IO.Directory.Exists(strDiretorio) Then
        '        strArquivos = System.IO.Directory.GetFiles(strDiretorio, "*.*")

        '        For idx As Integer = 0 To strArquivos.Length - 1
        '            System.IO.File.Delete(strArquivos(idx))
        '        Next
        '    End If

        'Catch ex As Exception
        '    Throw ex
        'Finally
        '    strArquivos = Nothing
        'End Try
    End Sub

    Public Shared Sub DeleteArquivosDiretorioNOVO(ByVal strDiretorio As String)
        Dim strArquivos As String()
        Try
            If System.IO.Directory.Exists(strDiretorio) Then
                strArquivos = System.IO.Directory.GetFiles(strDiretorio, "*.*")

                For idx As Integer = 0 To strArquivos.Length - 1
                    System.IO.File.Delete(strArquivos(idx))
                Next
            End If

        Catch ex As Exception
            Throw ex
        Finally
            strArquivos = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Função para enviar e-mail
    ''' </summary>
    ''' <param name="strMensagem">Mensagem a ser enviada</param>
    ''' <param name="strEmailDestinatario">E-mail do destinatário</param>
    ''' <param name="strNomeDestinatario">Nome do destinatário</param>
    ''' <remarks>Implementado por Juliana Lima em 20/06/2012</remarks>
    Public Shared Sub EnviarEmail(ByVal strMensagem As String, ByVal strEmailDestinatario As String, ByVal strNomeDestinatario As String, ByVal strAssunto As String, Optional ByVal strEmaiLCopia As String = "")
        Dim client As SmtpClient
        Dim remetente As MailAddress
        Dim destinatario, destinatarioCopia As MailAddress
        Dim strAux() As String
        Dim mensagem As MailMessage
        Dim credenciais As NetworkCredential
        Try
            'strEmailDestinatario = "joao.paulo@xy2.com.br"

            'client = New SmtpClient(System.Configuration.ConfigurationSettings.AppSettings("SMTP"), System.Configuration.ConfigurationSettings.AppSettings("Port"))
            'client.EnableSsl = System.Configuration.ConfigurationSettings.AppSettings("Security")

            'remetente = New MailAddress("fcmakrodev@gmail.com", "Fale Conosco - Makro")
            'destinatario = New MailAddress(strEmailDestinatario, strNomeDestinatario)
            'mensagem = New MailMessage(remetente, destinatario)
            'If strEmaiLCopia <> "" Then
            '    destinatarioCopia = New MailAddress(strEmaiLCopia)
            '    strAux = strEmaiLCopia.Split(";")
            '    For idx As Integer = 0 To strAux.Length - 1
            '        mensagem.CC.Add(strAux(idx))
            '    Next
            '    'mensagem.CC.Add(destinatarioCopia)

            'End If

            'mensagem.Body = strMensagem
            'mensagem.Subject = strAssunto
            'client.UseDefaultCredentials = False
            'credenciais = New NetworkCredential(System.Configuration.ConfigurationSettings.AppSettings("Usuario").ToString, System.Configuration.ConfigurationSettings.AppSettings("Senha").ToString, "")

            'client.Credentials = credenciais
            'mensagem.IsBodyHtml = True
            'client.Send(mensagem)




            'Dim APIKey As String = "613d7de1389e3a51ed1e6e55ad7d3eed"
            'Dim SecretKey As String = "93af81ca79d96843ff33bd603409c572"
            'Dim From As String = "FCMakro@makro.com.br"
            ''Dim To As String = "edujunque@hotmail.com"

            'Dim msg As New MailMessage()

            'msg.From = New MailAddress(From)

            'msg.To.Add(New MailAddress("edujunque@hotmail.com"))

            'msg.Subject = "Your mail from Mailjet"
            'msg.Body = "Your mail from Mailjet, sent by VB.NET."

            'Dim client As New SmtpClient("in.mailjet.com", 465)

            'client.EnableSsl = True
            'client.Credentials = New NetworkCredential(APIKey, SecretKey)

            'client.Send(msg)


            Dim Mail As MailMessage = New MailMessage()
            client = New SmtpClient(Configuration.ConfigurationSettings.AppSettings("SMTP"))
            Mail.From = New MailAddress(Configuration.ConfigurationSettings.AppSettings("Usuario"))

            Mail.To.Add(strEmailDestinatario)

            If strEmaiLCopia <> "" Then
                destinatarioCopia = New MailAddress(strEmaiLCopia)
                strAux = strEmaiLCopia.Split(";")
                For idx As Integer = 0 To strAux.Length - 1
                    Mail.CC.Add(strAux(idx))
                Next
                'mensagem.CC.Add(destinatarioCopia)  

            End If

            Mail.Subject = strAssunto
            Mail.Body = strMensagem
            Mail.IsBodyHtml = True

            client.Port = Convert.ToInt16(Configuration.ConfigurationSettings.AppSettings("Port"))
            client.UseDefaultCredentials = False
            client.Credentials = New NetworkCredential(Configuration.ConfigurationSettings.AppSettings("Usuario"), Configuration.ConfigurationSettings.AppSettings("Senha"))
            'SmtpServer.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings["EmailFrom"], ConfigurationSettings.AppSettings["EmailSenha"]);  
            client.EnableSsl = True

            client.Send(Mail)

        Catch ex As Exception
            Throw ex
        Finally
            'client = Nothing
            remetente = Nothing
            destinatario = Nothing
            mensagem = Nothing
            credenciais = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Função onde armazena todas as perguntas desejadas.
    ''' </summary>
    ''' <remarks>Implementado por Marco Antônio em 03/07/2012</remarks>
    Public Function Pergunta() As Collection
        Dim colAux As New Collection
        'Pergunta e ID
        colAux.Add("Qual é cor do cavalo branco de napoleão? ", 1)
        colAux.Add("Qual é cor do camaro amarelo da música?", 2)
        colAux.Add("Você gostou das perguntas anteriores?", 3)
        Return colAux
    End Function

    ''' <summary>
    ''' Função para retornar o Endereço a partir de um CEP
    ''' </summary>
    ''' <param name="cep">cep a ser pesquisado</param>
    ''' <returns></returns>
    ''' <remarks>Implementada por Juliana Lima em 12/07/2012</remarks>
    Public Shared Function BuscaCep(ByVal cep As String) As Hashtable
        Dim ds As DataSet
        Dim _resultado As String
        Dim ht As System.Collections.Hashtable = Nothing
        Dim strbAux As Text.StringBuilder
        Try
            strbAux = New Text.StringBuilder
            strbAux.Append("http://cep.republicavirtual.com.br/web_cep.php?cep=").Append(cep.Trim()).Append("&formato=xml")

            ds = New DataSet()
            ds.ReadXml(strbAux.ToString)

            If Not IsNothing(ds) Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    _resultado = ds.Tables(0).Rows(0).Item("resultado").ToString()
                    ht = New Hashtable
                    Select Case _resultado
                        Case "1"
                            ht.Add("UF", ds.Tables(0).Rows(0).Item("uf").ToString().Trim())
                            ht.Add("cidade", ds.Tables(0).Rows(0).Item("cidade").ToString().Trim())
                            ht.Add("bairro", ds.Tables(0).Rows(0).Item("bairro").ToString().Trim())
                            ht.Add("tipologradouro", ds.Tables(0).Rows(0).Item("tipo_logradouro").ToString().Trim())
                            ht.Add("logradouro", ds.Tables(0).Rows(0).Item("logradouro").ToString().Trim())
                            ht.Add("tipo", 1)

                        Case "2"
                            ht.Add("UF", ds.Tables(0).Rows(0).Item("uf").ToString().Trim())
                            ht.Add("cidade", ds.Tables(0).Rows(0).Item("cidade").ToString().Trim())
                            ht.Add("tipo", 2)
                        Case Else
                            ht.Add("tipo", 0)
                    End Select
                End If
            End If
            Return ht
        Catch ex As Exception
            'Throw New Exception("Falha ao Buscar o Cep" & vbCrLf & ex.ToString)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para verificar se uma String contem apenas números
    ''' </summary>
    ''' <param name="strEntrada">String a ser analisada</param>
    ''' <returns>True caso a String seja apenas de inteiros, false caso contrário</returns>
    ''' <remarks>Implementada por Juliana Lima em 15/03/2012</remarks>
    Public Function isInteger(ByVal strEntrada As String) As Boolean
        Dim isNumber As RegularExpressions.Regex
        Dim match As RegularExpressions.Match
        Try
            isNumber = New RegularExpressions.Regex("^\d+$")
            match = isNumber.Match(strEntrada)

            Return match.Success
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    'Function ValidaDados(input)
    '    lixo = array("select", "insert", "update", "delete", "drop", "--", "'")

    '    ValidaDados = True

    '    For i = lBound(lixo) To ubound(llixo)
    '        If (instr(1, input, lixo(i), vbtextcompare) <> 0) Then
    '            ValidaDados = False
    '              exit function}
    '        End If
    '    Next
    'End Function

    ''' <summary>
    ''' Função para Proteger o conteúdo digitado pelo usuário.
    ''' </summary>
    ''' <param name="strTexto"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function LimpaTexto(ByVal strTexto As String) As String
        Dim strComandosProtegidos() As String
        Dim strTextoLimpinho As String

        '/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/
        strComandosProtegidos = {"select ", "drop ", "create ", ";", "--", "insert ", "update ", "delete ", "xp_", "from ", "where ", " --", "/*", "*/", "exec "}

        strTextoLimpinho = strTexto

        For i = 0 To UBound(strComandosProtegidos)
            'If InStr(strTextoLimpinho, strComandosProtegidos(i), CompareMethod.Text
            strTextoLimpinho = Replace(strTextoLimpinho, strComandosProtegidos(i), "", , , CompareMethod.Text)
        Next

        Return strTextoLimpinho

    End Function

    ''' <summary>
    ''' Gerar uma senha com números e letras
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GerarSenha(ByVal intLength As Integer) As String
        Dim strCaracters As String = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789"
        Dim randomNumber As New Random()
        Dim chars(intLength - 1) As Char
        Dim allowedCharCount As Integer = strCaracters.Length

        Try
            For i As Integer = 0 To intLength - 1
                chars(i) = strCaracters.Chars(CInt(Fix((strCaracters.Length) * randomNumber.NextDouble())))
            Next i
            Return New String(chars)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#Region "Funções que valida CPF e CNPJ. Criado por Marco Antonio em 02-05-2012"
    Public Shared Function ValidaCPF(ByVal CPF As String) As Boolean

        Dim i, x, n1, n2 As Integer

        CPF = CPF.Trim

        If CPF.Length <> 14 Then
            Return False
        End If

        'remove a maskara
        CPF = CPF.Substring(0, 3) + CPF.Substring(4, 3) + CPF.Substring(8, 3) + CPF.Substring(12)

        If ((CPF = "11111111111") Or
           (CPF = "22222222222") Or
           (CPF = "33333333333") Or
           (CPF = "44444444444") Or
           (CPF = "55555555555") Or
           (CPF = "66666666666") Or
           (CPF = "77777777777") Or
           (CPF = "88888888888") Or
           (CPF = "99999999999") Or
           (CPF = "00000000000")) Then
            Return False
        End If

        For x = 0 To 1
            n1 = 0

            For i = 0 To 8 + x
                n1 = n1 + Val(CPF.Substring(i, 1)) * (10 + x - i)
            Next

            n2 = 11 - (n1 - (Int(n1 / 11) * 11))

            If n2 = 10 Or n2 = 11 Then n2 = 0

            If n2 <> Val(CPF.Substring(9 + x, 1)) Then
                Return False
            End If

        Next

        Return True

    End Function

    Public Function ValidaCNPJ(ByVal CNPJ As String) As Boolean
        Dim i As Integer
        Dim valida As Boolean

        CNPJ = CNPJ.Trim

        If CNPJ.Length <> 18 Then
            Return False
        End If

        'remove a maskara
        CNPJ = CNPJ.Substring(0, 2) + CNPJ.Substring(3, 3) + CNPJ.Substring(7, 3) + CNPJ.Substring(11, 4) + CNPJ.Substring(16)
        valida = efetivaValidacao(CNPJ)

        If valida Then
            ValidaCNPJ = True
        Else
            ValidaCNPJ = False
        End If

    End Function

    Private Function efetivaValidacao(ByVal cnpj As String)
        Dim Numero(13) As Integer
        Dim soma As Integer
        Dim i As Integer
        Dim valida As Boolean
        Dim resultado1 As Integer
        Dim resultado2 As Integer

        For i = 0 To Numero.Length - 1
            Numero(i) = CInt(cnpj.Substring(i, 1))
        Next

        soma = Numero(0) * 5 + Numero(1) * 4 + Numero(2) * 3 + Numero(3) * 2 + Numero(4) * 9 + Numero(5) * 8 + Numero(6) * 7 +
                   Numero(7) * 6 + Numero(8) * 5 + Numero(9) * 4 + Numero(10) * 3 + Numero(11) * 2

        soma = soma - (11 * (Int(soma / 11)))
        If soma = 0 Or soma = 1 Then
            resultado1 = 0
        Else
            resultado1 = 11 - soma
        End If

        If resultado1 = Numero(12) Then
            soma = Numero(0) * 6 + Numero(1) * 5 + Numero(2) * 4 + Numero(3) * 3 + Numero(4) * 2 + Numero(5) * 9 + Numero(6) * 8 +
                         Numero(7) * 7 + Numero(8) * 6 + Numero(9) * 5 + Numero(10) * 4 + Numero(11) * 3 + Numero(12) * 2
            soma = soma - (11 * (Int(soma / 11)))
            If soma = 0 Or soma = 1 Then
                resultado2 = 0
            Else
                resultado2 = 11 - soma
            End If
            If resultado2 = Numero(13) Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function
#End Region

End Class
