﻿Public Module GlobalVariables

    Public Const gintIdLojaPandaCampinas As Integer = 7
    
    Private gstrIP As String
    Private gstrPagina As String

    'propriedades
    'Leitura e escrita:
    Public Property pr_strIP() As String
        Get
            'Protegendo a variavel,caso ela seja requisitada,é retornado apenas o valor dela
            Return gstrIP
        End Get
        Set(ByVal value As String)
            'recebe o valor e guarda na variavel nome
            gstrIP = value
        End Set
    End Property


    'propriedades
    'Leitura e escrita:
    Public Property pr_strPagina() As String
        Get
            'Protegendo a variavel,caso ela seja requisitada,é retornado apenas o valor dela
            Return gstrPagina
        End Get
        Set(ByVal value As String)
            'recebe o valor e guarda na variavel nome
            gstrPagina = value
        End Set
    End Property
End Module
