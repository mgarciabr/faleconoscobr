﻿Imports System
Imports plAppLibrary.clsCommonFunctions

Public Class buUsuarioSub

    Private mdbUSUARIOSUB As dbLogin.dbUsuarioSub

    ''' <summary>
    ''' Função para Inclusão de dados na tabela USUARIOSUB.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Insert(ByVal intUSID As Integer, ByVal intASSUBID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOSUB = New dbLogin.dbUsuarioSub
            Return mdbUSUARIOSUB.Insert(intUSID, intASSUBID, objTransaction)
            mdbUSUARIOSUB = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOSUB = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIOSUB.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Delete(ByVal intUSSBAID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOSUB = New dbLogin.dbUsuarioSub
            Return mdbUSUARIOSUB.Delete(intUSSBAID, objTransaction)
            mdbUSUARIOSUB = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOSUB = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIOSUB.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 09/09/2014</remarks>
    Public Function DeleteUs(ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOSUB = New dbLogin.dbUSUARIOSUB
            Return mdbUSUARIOSUB.DeleteUs(intUSID, objTransaction)
            mdbUSUARIOSUB = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOSUB = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIOSUB.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Update(ByVal intUSSBAID As Integer, ByVal intUSID As Integer, ByVal intASSUBID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOSUB = New dbLogin.dbUsuarioSub
            Return mdbUSUARIOSUB.Update(intUSSBAID, intUSID, intASSUBID, objTransaction)
            mdbUSUARIOSUB = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOSUB = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIOSUB.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbUSUARIOSUB = New dbLogin.dbUSUARIOSUB
            Return mdbUSUARIOSUB.Query(strWhere, strOrderBy, objTransaction)
            mdbUSUARIOSUB = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOSUB = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIOSUB.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 10/09/2014</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOSUB = New dbLogin.dbUSUARIOSUB
            Return mdbUSUARIOSUB.UpdateBatch(dsDados, objTransaction)
            mdbUSUARIOSUB = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOSUB = Nothing
        End Try
    End Function

End Class
