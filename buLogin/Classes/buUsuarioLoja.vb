﻿Imports System
Imports plAppLibrary.clsCommonFunctions

Public Class buUsuarioLoja

    Private mdbUSUARIOLOJA As dbLogin.dbUsuarioLoja

    ''' <summary>
    ''' Função para Inclusão de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Insert(ByVal intLOJAID As Integer, ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOLOJA = New dbLogin.dbUsuarioLoja
            Return mdbUSUARIOLOJA.Insert(intLOJAID, intUSID, objTransaction)
            mdbUSUARIOLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOLOJA = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Delete(ByVal intLOJAID As Integer, ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOLOJA = New dbLogin.dbUsuarioLoja
            Return mdbUSUARIOLOJA.Delete(intLOJAID, intUSID, objTransaction)
            mdbUSUARIOLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOLOJA = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 09/09/2014</remarks>
    Public Function DeleteUs(ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOLOJA = New dbLogin.dbUsuarioLoja
            Return mdbUSUARIOLOJA.DeleteUs(intUSID, objTransaction)
            mdbUSUARIOLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOLOJA = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Update(ByVal intLOJAID As Integer, ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOLOJA = New dbLogin.dbUsuarioLoja
            Return mdbUSUARIOLOJA.Update(intLOJAID, intUSID, objTransaction)
            mdbUSUARIOLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOLOJA = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 08/09/2014</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbUSUARIOLOJA = New dbLogin.dbUsuarioLoja
            Return mdbUSUARIOLOJA.Query(strWhere, strOrderBy, objTransaction)
            mdbUSUARIOLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOLOJA = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIOLOJA.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 10/09/2014</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOLOJA = New dbLogin.dbUsuarioLoja
            Return mdbUSUARIOLOJA.UpdateBatch(dsDados, objTransaction)
            mdbUSUARIOLOJA = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOLOJA = Nothing
        End Try
    End Function

End Class
