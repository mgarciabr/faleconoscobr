﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela USUARIO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 10/04/2014 14:54:39</remarks>
Public Class buUsuario

    Private mdbUSUARIO As dbLogin.dbUsuario

    ''' <summary>
    ''' Função para Inclusão de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intPERFID As Integer, ByVal strUSEMAIL As String, ByVal strUSSENHA As String, ByVal bitUSALTERASENHA As Boolean, ByVal strDTUSDTCADASTRO As String, ByVal intUSSTATUS As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.Insert(intPERFID, strUSEMAIL, strUSSENHA, bitUSALTERASENHA, strDTUSDTCADASTRO, intUSSTATUS, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intUSID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.Delete(intUSID, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intUSID As Integer, ByVal intPERFID As Integer, ByVal strUSEMAIL As String, ByVal strUSSENHA As String, ByVal bitUSALTERASENHA As Boolean, ByVal strDTUSDTCADASTRO As String, ByVal intUSSTATUS As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.Update(intUSID, intPERFID, strUSEMAIL, strUSSENHA, bitUSALTERASENHA, strDTUSDTCADASTRO, intUSSTATUS, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/04/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateSenha(ByVal intUSID As Integer, ByVal strUSSENHA As String, ByVal bitUSALTERASENHA As Boolean, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.UpdateSenha(intUSID, strUSSENHA, bitUSALTERASENHA, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração da coluna status na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>12/05/2014 - Implementada por Maria Beatriz</remarks>
    Public Function UpdateStatus(ByVal intUSID As Integer, ByVal intUSSTATUS As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.UpdateStatus(intUSID, intUSSTATUS, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>23/06/2014 - Criação desta função por Maria Beatriz</remarks>
    Public Function UpdateCadastro(ByVal intUSID As Integer, ByVal strUSDTCADASTRO As String, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.UpdateCadastro(intUSID, strUSDTCADASTRO, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.UpdateBatch(dsDados, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.Query(strWhere, strOrderBy, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function QueryRegiao(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.QueryRegiao(strWhere, strOrderBy, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function QueryLoja(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.QueryLoja(strWhere, strOrderBy, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>28/10/2014 - Maria Beatriz</remarks>
    Public Function QueryDistinct(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.QueryDistinct(strWhere, strOrderBy, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>10/04/2014 14:54:39 - Criação desta função pelo it.maker</remarks>
    Public Function UltimoId(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Integer
        Try
            mdbUSUARIO = New dbLogin.dbUsuario
            Return mdbUSUARIO.UltimoId(strWhere, strOrderBy, objTransaction)
            mdbUSUARIO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIO = Nothing
        End Try
    End Function
End Class