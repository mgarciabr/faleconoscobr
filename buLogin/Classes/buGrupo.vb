﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela GRUPO
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 16/05/2014 15:42:08</remarks>
Public Class buGrupo

    Private mdbGRUPO As dbLogin.dbGrupo

    ''' <summary>
    ''' Função para Inclusão de dados na tabela GRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:42:08 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal strGRUPODESCR As String, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbGRUPO = New dbLogin.dbGrupo
            Return mdbGRUPO.Insert(strGRUPODESCR, objTransaction)
            mdbGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbGRUPO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela GRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:42:08 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intGRUPOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbGRUPO = New dbLogin.dbGrupo
            Return mdbGRUPO.Delete(intGRUPOID, objTransaction)
            mdbGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbGRUPO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela GRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:42:08 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intGRUPOID As Integer, ByVal strGRUPODESCR As String, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbGRUPO = New dbLogin.dbGrupo
            Return mdbGRUPO.Update(intGRUPOID, strGRUPODESCR, objTransaction)
            mdbGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbGRUPO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela GRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:42:08 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbGRUPO = New dbLogin.dbGrupo
            Return mdbGRUPO.UpdateBatch(dsDados, objTransaction)
            mdbGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbGRUPO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela GRUPO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>16/05/2014 15:42:08 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbGRUPO = New dbLogin.dbGrupo
            Return mdbGRUPO.Query(strWhere, strOrderBy, objTransaction)
            mdbGRUPO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbGRUPO = Nothing
        End Try
    End Function

End Class