﻿Public Class buUsuarioAssunto

    Private mdbUSUARIOASSUNTO As dbLogin.dbUsuarioAssunto

    ''' <summary>
    ''' Função para Inclusão de dados na tabela USUARIOASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 11/09/2014</remarks>
    Public Function Insert(ByVal intUSID As Integer, ByVal intASSUNTOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOASSUNTO = New dbLogin.dbUSUARIOASSUNTO
            Return mdbUSUARIOASSUNTO.Insert(intUSID, intASSUNTOID, objTransaction)
            mdbUSUARIOASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIOASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 11/09/2014</remarks>
    Public Function Delete(ByVal intUSID As Integer, ByVal intASSUNTOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOASSUNTO = New dbLogin.dbUSUARIOASSUNTO
            Return mdbUSUARIOASSUNTO.Delete(intUSID, intASSUNTOID, objTransaction)
            mdbUSUARIOASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela USUARIOASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 09/09/2014</remarks>
    Public Function DeleteUs(ByVal intASSUNTOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOASSUNTO = New dbLogin.dbUSUARIOASSUNTO
            Return mdbUSUARIOASSUNTO.DeleteUs(intASSUNTOID, objTransaction)
            mdbUSUARIOASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela USUARIOASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 11/09/2014</remarks>
    Public Function Update(ByVal intUSID As Integer, ByVal intASSUNTOID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOASSUNTO = New dbLogin.dbUSUARIOASSUNTO
            Return mdbUSUARIOASSUNTO.Update(intUSID, intASSUNTOID, objTransaction)
            mdbUSUARIOASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOASSUNTO = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIOASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 11/09/2014</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbUSUARIOASSUNTO = New dbLogin.dbUSUARIOASSUNTO
            Return mdbUSUARIOASSUNTO.Query(strWhere, strOrderBy, objTransaction)
            mdbUSUARIOASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOASSUNTO = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela USUARIOASSUNTO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Criado por Maria Beatriz em 10/09/2014</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbUSUARIOASSUNTO = New dbLogin.dbUSUARIOASSUNTO
            Return mdbUSUARIOASSUNTO.UpdateBatch(dsDados, objTransaction)
            mdbUSUARIOASSUNTO = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbUSUARIOASSUNTO = Nothing
        End Try
    End Function
End Class
