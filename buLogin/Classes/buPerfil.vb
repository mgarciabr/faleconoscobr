﻿Imports System

''' <summary>
''' Classe para gerenciamento de Regras de Negócio das operações na tabela PERFIL
''' </summary>
''' <remarks>Esta classe foi gerada pelo it.maker em 14/04/2014 09:37:18</remarks>
Public Class buPerfil

    Private mdbPERFIL As dbLogin.dbPerfil

    ''' <summary>
    ''' Função para Inclusão de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:18 - Criação desta função pelo it.maker</remarks>
    Public Function Insert(ByVal intPERFID As Integer, ByVal strPERFDESCR As String, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbPERFIL = New dbLogin.dbPerfil
            Return mdbPERFIL.Insert(intPERFID, strPERFDESCR, objTransaction)
            mdbPERFIL = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbPERFIL = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Exclusão de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:19 - Criação desta função pelo it.maker</remarks>
    Public Function Delete(ByVal intPERFID As Integer, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbPERFIL = New dbLogin.dbPerfil
            Return mdbPERFIL.Delete(intPERFID, objTransaction)
            mdbPERFIL = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbPERFIL = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:19 - Criação desta função pelo it.maker</remarks>
    Public Function Update(ByVal intPERFID As Integer, ByVal strPERFDESCR As String, _
                           Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbPERFIL = New dbLogin.dbPerfil
            Return mdbPERFIL.Update(intPERFID, strPERFDESCR, objTransaction)
            mdbPERFIL = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbPERFIL = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Alteração em Lote de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:19 - Criação desta função pelo it.maker</remarks>
    Public Function UpdateBatch(ByVal dsDados As DataSet, _
                                Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As Boolean
        Try
            mdbPERFIL = New dbLogin.dbPerfil
            Return mdbPERFIL.UpdateBatch(dsDados, objTransaction)
            mdbPERFIL = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbPERFIL = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Função para Consulta de dados na tabela PERFIL.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>14/04/2014 09:37:19 - Criação desta função pelo it.maker</remarks>
    Public Function Query(Optional ByVal strWhere As String = "", Optional ByVal strOrderBy As String = "", _
                          Optional ByVal objTransaction As plAppLibrary.Transaction = Nothing) As DataSet
        Try
            mdbPERFIL = New dbLogin.dbPerfil
            Return mdbPERFIL.Query(strWhere, strOrderBy, objTransaction)
            mdbPERFIL = Nothing
        Catch ex As Exception
            Throw ex
        Finally
            mdbPERFIL = Nothing
        End Try
    End Function

End Class
